#ifndef _mempool_H__
#define _mempool_H__
#undef __STDC_NO_THREADS__
#include <pthread.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct slice {
  void *p;
  int64_t len;
  int64_t cap;
} slice;

slice slice__New(int64_t len);
void *slice__At(slice s, int64_t offset, int64_t unit);
slice slice__grow(slice s, int64_t cap);
slice slice__append(slice s, void *ptr, int64_t size);

typedef struct mempool__Mempool {
  slice p; // 这个类型相当于 []struct{p void* ; len int ; cap int}
  pthread_mutex_t lock;
} mempool__Mempool;

mempool__Mempool mempool__New(int64_t size);
void *mempool__Mempool_New(mempool__Mempool *ptr, int64_t size);
void mempool_Free(mempool__Mempool *ptr);

#endif