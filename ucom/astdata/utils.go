package astdata

import (
	"strings"

	"gitee.com/u-language/u-language/ucom/enum"
	"gitee.com/u-language/u-language/ucom/internal/utils"
)

func Generate_method_symbol(typ string, funcname string) string {
	typ = utils.Ret_no_lea(typ)
	return strings.Join([]string{enum.Method, typ, funcname}, "_")
}
