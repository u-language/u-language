// Package astdata 提供抽象语法树共用的API
package astdata

// Parame 函数参数
// 缩写：parameter缩写成Parame
type Parame = NameAndType

// RetValue 函数返回值
// 缩写：ReturnValue缩写成RetValue
type RetValue = NameAndType

// NameAndType 名字和类型
type NameAndType struct {
	Type Typ
	Name string
}

// Typ 能表示类型的抽象语法树节点
type Typ interface {
	SetTyp()
	Typ() string
	Copy() Typ
	SetLea(i int)
}

// NewNameAndType 创建一个名字和类型
func NewNameAndType(Name string, Type Typ) NameAndType {
	return NameAndType{Name: Name, Type: Type}
}
