package enum

// Symbol 符号类型枚举
type Symbol int

const (
	NoSymbol Symbol = iota
	SymbolVar
	SymbolFunc
	SymbolConst
	SymbolStruct
	SymbolMethod
	SymbolImport
	SymbolEnum
	SymbolGenInst
	SymbolRemoveGenerics
	SymbolMethodTable
	SymbolNoParameVar //Non parametric variable
)

var (
	SymbolSrtMap = [...]string{
		NoSymbol:             "NoSymbol （未定义的符号枚举）",
		SymbolVar:            "SymbolVar （变量符号）",
		SymbolFunc:           "SymbolFunc （函数符号）",
		SymbolConst:          "SymbolConst （常量符号）",
		SymbolStruct:         "SymbolStruct（结构体符号）",
		SymbolMethod:         "SymbolMethod（方法符号）",
		SymbolImport:         "SymbolImport（导入信息）",
		SymbolEnum:           "SymbolEnum（枚举信息）",
		SymbolGenInst:        "SymbolGenInst（泛型实例化信息）",
		SymbolRemoveGenerics: "SymbolRemoveGenerics（删除泛型信息）",
		SymbolMethodTable:    "SymbolMethodTable（方法表）",
		SymbolNoParameVar:    "SymbolNoParameVar (非参数变量)",
	}
)

func (o Symbol) String() string {
	return SymbolSrtMap[o]
}

const (
	Printf         = "printf"
	Malloc         = "malloc"
	Free           = "free"
	Main           = "main"
	Init           = "init"
	MallocSize     = "mallocSize"
	AutoFree       = "autofree"
	AutoFreeInFunc = "_mempool"
	MemPoolFree    = "mempool_Free"
	MemPool        = "mempool__Mempool"
	MemPoolNew     = "mempool__Mempool_New"
)

const (
	UnsafeAdd     = "unsafe__Add"
	UnsafeConvert = "unsafe__Convert"
	UnsafePointer = "unsafe__Pointer"
	UnsafeSizeof  = "unsafe__Sizeof"
)
