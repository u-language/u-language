// Package enum 提供多个包共用的枚举值
package enum

// 可以在运算中出现的符号枚举
type OPSymbol int

const (
	NoOp OPSymbol = iota
	ADDOP
	SUBOP
	MULOP
	DIVOP
	LessOP
	GreaterOP
	EqualOP
	NoEqualOP
	RemainOP //remainder
	IncOP    //Increment
	DecOP    //Decrement
	AndOP
	OrOP
	XorOp
	LogicAndOP
	LogicOrOP
	LPARENOP //这个是为了写语法解析器方便才列入运算符枚举的
	DerefOP
	CommaOP //这个是为了写语法解析器方便才列入运算符枚举的
)

var (
	OPSymbolSrtMap = [...]string{
		NoOp:       "noop (未定义的op)",
		ADDOP:      "add (加法)",
		SUBOP:      "sub (减法)",
		MULOP:      "mul (乘法)",
		DIVOP:      "div (除法)",
		LessOP:     "less (小于)",
		GreaterOP:  "greater (大于)",
		EqualOP:    "equal (相等)",
		NoEqualOP:  "no equal (不相等)",
		RemainOP:   "remainder (取余数)",
		IncOP:      "Increment (自增)",
		DecOP:      "Decrement (自减)",
		AndOP:      "and (位与)",
		OrOP:       "or (位或)",
		XorOp:      "xor (异或)",
		LogicAndOP: "logic and (逻辑与)",
		LogicOrOP:  "logic or (逻辑或)",
		LPARENOP:   "lparen (左小括号)",
		DerefOP:    "dereference (解引用)",
		CommaOP:    "comma (逗号)",
	}
)

func (n OPSymbol) String() string {
	return OPSymbolSrtMap[n]
}

const (
	Bool     = "bool"
	Int      = "int"
	String   = "string"
	Float    = "float"
	Func     = "func"
	Const    = "const"
	Nil      = "nil"
	Switch   = "switch"
	Case     = "case"
	Default  = "default"
	Method   = "method"
	Enum     = "enum"
	Generate = "generate"
	Tmp      = "tmp"
	StrFalse = "0"
)

// 缩写 Package separator 缩写成 PackageSep
const PackageSep = "__"
