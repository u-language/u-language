package lex_test

import (
	"fmt"
	"testing"

	"gitee.com/u-language/u-language/ucom/internal/teco"
	. "gitee.com/u-language/u-language/ucom/lex"
)

func TestScan_AllScan(t *testing.T) {
	var (
		a1  = "var b float = 4.2"
		a2  = "var	j int = 0"
		a3  = "var s int   = 9"
		a4  = "var g    float = 9"
		a5  = "var g 	   float = 9"
		a6  = "}"
		a7  = "   "
		a8  = "var b float = 4.2        "
		a9  = "b=4.2        "
		a10 = "b=					4.2"
		a11 = "        var b float = 4.2"
		a12 = "func b(a int,b int) {"
		a13 = "b = b + 7"
		a14 = "b=b+7"
		a15 = "\t"
		a16 = "+"
		a17 = "-"
		a18 = "="
		a19 = "var a int = 19 //a=19"
		a20 = "*"
		a21 = "a=a+a*a"
		a22 = "/"
		a23 = "=="
		a24 = "a==b"
		a25 = "!="
		a26 = "a!=b"
		a27 = ">"
		a28 = "<"
		a29 = "true"
		a30 = "false"
		a31 = "if"
		a32 = "else"
		a33 = "for"
		a34 = "break"
		a35 = "continue"
		a36 = "struct"
		a37 = "interface"
		a38 = "&a"
		a39 = "&"
		a40 = "/*"
		a41 = "*/"
		a42 = "\"a\""
		a43 = "\"" + "\\" + "\"" + "a" + "\"" //"\"a"
		a44 = ";"
		a45 = "return"
		a46 = "("
		a47 = ")"
		a48 = "goto"
		a49 = ":"
		a50 = "%"
		a51 = "@a"
		a52 = "@"
		a53 = "const"
		a54 = "++"
		a55 = "--"
		a56 = "nil"
		a57 = "package"
		a58 = "switch"
		a59 = "case"
		a60 = "default"
		a61 = "method"
		a62 = "autofree"
		a63 = "var a int =-1"
		a64 = "a(-1,-1.1)"
		a65 = "1-1"
		a66 = "and |^ &&||"
		a67 = "import enum"
		a68 = "[1+2]int [1+2][1+2]int"
		a69 = "a[1+2] a[1+2][1+2]"
		a70 = "[1][1][1]int"
		a71 = "[1]&int"
		a72 = "&s[int] s[int]"
		a73 = "1<-1 1>-1 1==-1 1!=-1"
	)
	tests := []struct {
		name string
		s    Scan
		want []Token
	}{
		{"正常空格", NewScan(a1, ScanConfig{}), []Token{teco.LexVar, NewToken(NAME, "b"), teco.LexFloat, teco.LexAssign, NewToken(FLOAT, "4.2")}},
		{"中间\\t空格", NewScan(a2, ScanConfig{}), []Token{teco.LexVar, NewToken(NAME, "j"), teco.LexInt, teco.LexAssign, NewToken(NUMBER, "0")}},
		{"后多空格", NewScan(a3, ScanConfig{}), []Token{teco.LexVar, NewToken(NAME, "s"), teco.LexInt, teco.LexAssign, NewToken(NUMBER, "9")}},
		{"前多空格", NewScan(a4, ScanConfig{}), []Token{teco.LexVar, NewToken(NAME, "g"), teco.LexFloat, teco.LexAssign, NewToken(NUMBER, "9")}},
		{"空格加\\t", NewScan(a5, ScanConfig{}), []Token{teco.LexVar, NewToken(NAME, "g"), teco.LexFloat, teco.LexAssign, NewToken(NUMBER, "9")}},
		{"右大括号", NewScan(a6, ScanConfig{}), []Token{NewToken(RBRACE, "}")}},
		{"全空格", NewScan(a7, ScanConfig{}), []Token{}},
		{"尾多空格", NewScan(a8, ScanConfig{}), []Token{teco.LexVar, NewToken(NAME, "b"), teco.LexFloat, teco.LexAssign, NewToken(FLOAT, "4.2")}},
		{"无空格", NewScan(a9, ScanConfig{}), []Token{NewToken(NAME, "b"), teco.LexAssign, NewToken(FLOAT, "4.2")}},
		{"多\\t空格", NewScan(a10, ScanConfig{}), []Token{NewToken(NAME, "b"), teco.LexAssign, NewToken(FLOAT, "4.2")}},
		{"首多空格", NewScan(a11, ScanConfig{}), []Token{teco.LexVar, NewToken(NAME, "b"), teco.LexFloat, teco.LexAssign, NewToken(FLOAT, "4.2")}},
		{"函数声明", NewScan(a12, ScanConfig{}), []Token{NewToken(FUNC, "func"), NewToken(NAME, "b"), NewToken(LPAREN, "("), NewToken(NAME, "a"), teco.LexInt, NewToken(Comma, ","), NewToken(NAME, "b"), teco.LexInt, NewToken(RPAREN, ")"), NewToken(LBRACE, "{")}},
		{"标准赋值", NewScan(a13, ScanConfig{}), []Token{NewToken(NAME, "b"), teco.LexAssign, NewToken(NAME, "b"), teco.LexAdd, NewToken(NUMBER, "7")}},
		{"无空格赋值", NewScan(a14, ScanConfig{}), []Token{NewToken(NAME, "b"), teco.LexAssign, NewToken(NAME, "b"), teco.LexAdd, NewToken(NUMBER, "7")}},
		{"\t空行", NewScan(a15, ScanConfig{}), []Token{}},
		{"+", NewScan(a16, ScanConfig{}), []Token{teco.LexAdd}},
		{"-", NewScan(a17, ScanConfig{}), []Token{NewToken(SUB, "-")}},
		{"=", NewScan(a18, ScanConfig{}), []Token{teco.LexAssign}},
		{"赋值后注释", NewScan(a19, ScanConfig{}), []Token{teco.LexVar, NewToken(NAME, "a"), teco.LexInt, teco.LexAssign, NewToken(NUMBER, "19")}},
		{"*", NewScan(a20, ScanConfig{}), []Token{teco.LexMul}},
		{"无空格多运算符表达式", NewScan(a21, ScanConfig{}), []Token{NewToken(NAME, "a"), teco.LexAssign, NewToken(NAME, "a"), teco.LexAdd, NewToken(NAME, "a"), teco.LexMul, NewToken(NAME, "a")}},
		{"/", NewScan(a22, ScanConfig{}), []Token{NewToken(DIV, "/")}},
		{"==", NewScan(a23, ScanConfig{}), []Token{teco.LexEqual}},
		{"比较是否相等", NewScan(a24, ScanConfig{}), []Token{NewToken(NAME, "a"), teco.LexEqual, NewToken(NAME, "b")}},
		{"!=", NewScan(a25, ScanConfig{}), []Token{teco.LexNoEqual}},
		{"比较是否不相等", NewScan(a26, ScanConfig{}), []Token{NewToken(NAME, "a"), teco.LexNoEqual, NewToken(NAME, "b")}},
		{">", NewScan(a27, ScanConfig{}), []Token{NewToken(Greater, ">")}},
		{"<", NewScan(a28, ScanConfig{}), []Token{teco.LexLess}},
		{"true", NewScan(a29, ScanConfig{}), []Token{NewToken(TRUE, "true")}},
		{"false", NewScan(a30, ScanConfig{}), []Token{NewToken(FALSE, "false")}},
		{"if", NewScan(a31, ScanConfig{}), []Token{NewToken(IF, "if")}},
		{"else", NewScan(a32, ScanConfig{}), []Token{NewToken(ELSE, "else")}},
		{"for", NewScan(a33, ScanConfig{}), []Token{NewToken(FOR, "for")}},
		{"break", NewScan(a34, ScanConfig{}), []Token{NewToken(Break, "break")}},
		{"continue", NewScan(a35, ScanConfig{}), []Token{NewToken(Continue, "continue")}},
		{"struct", NewScan(a36, ScanConfig{}), []Token{NewToken(Struct, "struct")}},
		{"interface", NewScan(a37, ScanConfig{}), []Token{NewToken(Interface, "interface")}},
		{"&a", NewScan(a38, ScanConfig{}), []Token{NewToken(LEA, "&a")}},
		{"&", NewScan(a39, ScanConfig{}), []Token{NewToken(OnlyLEA, "&")}},
		{"/*", NewScan(a40, ScanConfig{IsMLC: new(bool)}), []Token{NewToken(MLCStart, "/*")}},
		{"*/", NewScan(a41, ScanConfig{}), []Token{NewToken(MLCEnd, "*/")}},
		{"\"a\"", NewScan(a42, ScanConfig{}), []Token{NewToken(String, "\"a\"")}},
		{a43, NewScan(a43, ScanConfig{}), []Token{NewToken(String, a43)}},
		{";", NewScan(a44, ScanConfig{}), []Token{NewToken(SEMICOLON, ";")}},
		{"return", NewScan(a45, ScanConfig{}), []Token{NewToken(Return, "return")}},
		{"(", NewScan(a46, ScanConfig{}), []Token{NewToken(LPAREN, "(")}},
		{")", NewScan(a47, ScanConfig{}), []Token{NewToken(RPAREN, ")")}},
		{"goto", NewScan(a48, ScanConfig{}), []Token{NewToken(GOTO, "goto")}},
		{"冒号", NewScan(a49, ScanConfig{}), []Token{NewToken(Colon, ":")}},
		{"取余数", NewScan(a50, ScanConfig{}), []Token{NewToken(Remain, "%")}},
		{"@a", NewScan(a51, ScanConfig{}), []Token{NewToken(Deref, "@a")}},
		{"@", NewScan(a52, ScanConfig{}), []Token{NewToken(OnlyDeref, "@")}},
		{"const", NewScan(a53, ScanConfig{}), []Token{NewToken(Const, "const")}},
		{"自增", NewScan(a54, ScanConfig{}), []Token{NewToken(Inc, "++")}},
		{"自减", NewScan(a55, ScanConfig{}), []Token{NewToken(Dec, "--")}},
		{"nil", NewScan(a56, ScanConfig{}), []Token{NewToken(Nil, "nil")}},
		{"package", NewScan(a57, ScanConfig{}), []Token{NewToken(Package, "package")}},
		{"switch", NewScan(a58, ScanConfig{}), []Token{NewToken(Switch, "switch")}},
		{"case", NewScan(a59, ScanConfig{}), []Token{NewToken(Case, "case")}},
		{"default", NewScan(a60, ScanConfig{}), []Token{NewToken(Default, "default")}},
		{"method", NewScan(a61, ScanConfig{}), []Token{NewToken(Method, "method")}},
		{"autofree", NewScan(a62, ScanConfig{}), []Token{NewToken(AutoFree, "autofree")}},
		{"赋值中的负数", NewScan(a63, ScanConfig{}), []Token{teco.LexVar, NewToken(NAME, "a"), teco.LexInt, teco.LexAssign, NewToken(NUMBER, "-1")}},
		{"函数调用中的负数", NewScan(a64, ScanConfig{}), []Token{NewToken(NAME, "a"), teco.LexLPAREN, NewToken(NUMBER, "-1"), NewToken(Comma, ","), NewToken(FLOAT, "-1.1"), teco.LexRPAREN}},
		{"1-1", NewScan(a65, ScanConfig{}), []Token{NewToken(NUMBER, "1"), NewToken(SUB, "-"), NewToken(NUMBER, "1")}},
		{"位与,位或,异或", NewScan(a66, ScanConfig{}), []Token{NewToken(AND, "and"), NewToken(Or, "|"), NewToken(Xor, "^"), NewToken(LogicAND, "&&"), NewToken(LogicOr, "||")}},
		{"import enum", NewScan(a67, ScanConfig{}), []Token{NewToken(Import, "import"), NewToken(Enum, "enum")}},
		{"数组类型", NewScan(a68, ScanConfig{}), []Token{NewMoreThanJustTokensWithBrackts(NewToken(TokenWithBrackets, "[1+2]"), NewToken(NAME, "int")), NewMoreThanJustTokensWithBrackts(NewToken(TokenWithBrackets, "[1+2]"), NewMoreThanJustTokensWithBrackts(NewToken(TokenWithBrackets, "[1+2]"), NewToken(NAME, "int")))}},
		{"索引表达式", NewScan(a69, ScanConfig{}), []Token{NewMoreThanJustTokensWithBrackts(NewToken(NAME, "a"), NewToken(TokenWithBrackets, "[1+2]")), NewMoreThanJustTokensWithBrackts(NewMoreThanJustTokensWithBrackts(NewToken(NAME, "a"), NewToken(TokenWithBrackets, "[1+2]")), NewToken(TokenWithBrackets, "[1+2]"))}},
		{"三维数组", NewScan(a70, ScanConfig{}), []Token{NewMoreThanJustTokensWithBrackts(NewToken(TokenWithBrackets, "[1]"), NewMoreThanJustTokensWithBrackts(NewToken(TokenWithBrackets, "[1]"), NewMoreThanJustTokensWithBrackts(NewToken(TokenWithBrackets, "[1]"), NewToken(NAME, "int"))))}},
		{"指针数组", NewScan(a71, ScanConfig{}), []Token{NewMoreThanJustTokensWithBrackts(NewToken(TokenWithBrackets, "[1]"), NewToken(LEA, "&int"))}},
		{"指针泛型", NewScan(a72, ScanConfig{}), []Token{NewMoreThanJustTokensWithBrackts(NewToken(LEA, "&s"), NewToken(TokenWithBrackets, "[int]")), NewMoreThanJustTokensWithBrackts(NewToken(NAME, "s"), NewToken(TokenWithBrackets, "[int]"))}},
		{"运算中的负数", NewScan(a73, ScanConfig{}), []Token{NewToken(NUMBER, "1"), NewToken(Less, "<"), NewToken(NUMBER, "-1"), NewToken(NUMBER, "1"), NewToken(Greater, ">"), NewToken(NUMBER, "-1"), NewToken(NUMBER, "1"), NewToken(Equal, "=="), NewToken(NUMBER, "-1"), NewToken(NUMBER, "1"), NewToken(NoEqual, "!="), NewToken(NUMBER, "-1")}},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.s.AllScan(); !equalLineToken(got, tt.want) {
				t.Errorf("Scan.AllScan() \n = %v \n, want %v", got, tt.want)
			}
		})
	}
}

// equlaLineToken 判断一行的Token是不是相同
func equalLineToken(a, b []Token) bool {
	if len(a) != len(b) {
		fmt.Println(len(a), len(b))
		return false
	}
	for i, v := range a {
		if v.TYPE == MoreThanJustTokensWithBrackts {
			if b[i].TYPE == MoreThanJustTokensWithBrackts {
				av := *v.PtrMoreThanJustTokensWithBracktsS().Ptr
				bv := *b[i].PtrMoreThanJustTokensWithBracktsS().Ptr
				if !equalLineToken(av, bv) {
					return false
				}
			} else {
				return false
			}
		}
		if v != b[i] {
			return false
		}
	}
	return true
}
