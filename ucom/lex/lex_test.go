package lex_test

import (
	"context"
	"reflect"
	"strconv"
	"strings"
	"testing"

	"gitee.com/u-language/u-language/ucom/errcode"
	"gitee.com/u-language/u-language/ucom/internal/teco"
	. "gitee.com/u-language/u-language/ucom/lex"
)

func TestLex(t *testing.T) {
	type args struct {
		file   string
		value  []string
		errctx *errcode.ErrCtx
		cancel context.CancelFunc
	}
	a1 := args{file: "1.txt", value: []string{"var s float = 8"}, errctx: errcode.DefaultErrCtx}
	a2 := args{file: "1.txt", value: []string{"var s float = 8"}, errctx: errcode.DefaultErrCtx}
	a1token := []Token{teco.LexVar, NewToken(NAME, "s"), teco.LexFloat, teco.LexAssign, NewToken(NUMBER, "8")}
	newline := func(tk ...[]Token) []Line {
		var ret = make([]Line, len(tk))
		for i := 0; i < len(tk); i++ {
			ret[i] = NewLine(i+1, tk[i])
		}
		return ret
	}
	newargs := func(file string, value []string, errf func(errcode.ErrInfo)) args {
		actx := errcode.NewErrCtx()
		astdctx, acancel := context.WithCancel(context.Background())
		go actx.Handle(astdctx, errf, func() {})
		ret := args{file: file, value: value, errctx: actx, cancel: acancel}
		return ret
	}
	//a3
	a3 := args{file: "1.txt", value: []string{"/*", "}", "*/"}, errctx: errcode.DefaultErrCtx}
	a3want := newline([]Token{NewToken(MLCStart, "/*")}, []Token{NewToken(MLCIn, "}")}, []Token{NewToken(MLCEnd, "*/")})
	//a4
	a4 := newargs("1.txt", []string{"/*", "}"}, func(err errcode.ErrInfo) {
		if !err.IsErr("1.txt", 1, nil, errcode.MLCNoEnd) {
			panic(err.String())
		}
	})
	a4want := newline([]Token{NewToken(MLCStart, "/*")}, []Token{NewToken(MLCIn, "}")})
	tests := []struct {
		name string
		args args
		want []Line
	}{
		{name: "var name float = imm(int)", args: a1, want: []Line{Line{Linenum: 1, Value: a1token}}},
		{name: "var name float = imm(int) p=1", args: a2, want: []Line{Line{Linenum: 1, Value: a1token}}},
		{name: "有右大括号的多行注释", args: a3, want: a3want},
		{name: "没有结束的多行注释", args: a4, want: a4want},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := Lex(tt.args.file, tt.args.value, tt.args.errctx, false); !reflect.DeepEqual(got.Value, tt.want) {
				t.Errorf("Lex() = %v, want %v", got, tt.want)
			}
			if tt.args.cancel != nil {
				tt.args.cancel()
			}
		})
	}
}

const n = 600

func BenchmarkLexP1(b *testing.B) {
	type args struct {
		file   string
		value  []string
		errctx *errcode.ErrCtx
	}
	b.StopTimer()
	a2 := args{file: "1.txt", value: reta2str(), errctx: errcode.DefaultErrCtx}
	b.SetBytes(1)
	b.ReportAllocs()
	b.StartTimer()
	for i := 0; i < b.N; i++ {
		_ = Lex(a2.file, a2.value, a2.errctx, false)
	}
}

func reta2str() []string {
	ret := make([]string, n)
	for i := 0; i < n; i++ {
		var buf strings.Builder
		buf.WriteString("var s")
		buf.WriteString(strconv.Itoa(i))
		buf.WriteString(" float=2.8")
		ret[i] = buf.String()
	}
	return ret
}
