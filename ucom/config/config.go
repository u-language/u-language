// Package config 提供可选配置
//
// 可选配置通过环境变量设置
//
// 被导入时将自动从环境变量获取可选配置，如果没有或为空，将使用默认配置
package config

import (
	"errors"
	"fmt"
	"os"
	"os/exec"
	"path/filepath"
	_ "unsafe"
)

// CC 是可使用的C语言工具链
var CC string = "clang||gcc"

// PrintTime 控制是否打印编译阶段用时，on为启用
var PrintTime string = "on"

// URoot 是U语言编译器和标准库的根目录
// 默认值auto表示自动检测目录
var URoot string = "auto"

var (
	Debug_PrintLex  string = "on"
	Debug_PrintAst  string = "on"
	Debug_PrintCast string = "on"
)

func init() {
	getValue("CC", &CC)
	getValue("PrintTime", &PrintTime)
	getValue("Debug_PrintLex", &Debug_PrintLex)
	getValue("Debug_PrintAst", &Debug_PrintAst)
	getValue("Debug_PrintCast", &Debug_PrintCast)
	getValue("URoot", &URoot)
	if URoot == "auto" { //自动检测URoot目录
		var err error
		URoot, err = os.Executable()
		if err != nil {
			fmt.Println("未知错误", err)
			os.Exit(2)
		}
		if filepath.Dir(filepath.Dir(filepath.Dir(URoot))) == os.TempDir() { //如果是测试中
			fmt.Println("测试推荐设置URoot")
			if !looppath("pkg"+ext) && !looppath("ucom"+ext) {
				fmt.Println("查找程序所在目录失败")
				os.Exit(2)
			}
			URoot, err = filepath.Abs(URoot)
			if err != nil {
				fmt.Println("未知错误", err)
				os.Exit(2)
			}
		}
		URoot = filepath.Dir(URoot)
		URoot = filepath.Dir(URoot)
	}
}

// 为避免循环依赖添加
//
//go:linkname ext gitee.com/u-language/u-language/ucom/internal/utils.Ext
var ext string

func looppath(name string) bool {
	var err error
	URoot, err = exec.LookPath(name)
	if errors.Is(err, exec.ErrDot) || err == nil {
		return true
	}
	if !errors.Is(err, exec.ErrNotFound) {
		fmt.Println("未知错误", err)
		os.Exit(2)
	}
	return false
}

func getValue(name string, ptr *string) {
	if cc := os.Getenv(name); cc != "" {
		*ptr = cc
	}
}
