// Package config 提供可选配置
//
// 可选配置通过环境变量设置
//
// 被导入时将自动从环境变量获取可选配置，如果没有或为空，将使用默认配置
package config

import "testing"

func Test_getValue(t *testing.T) {
	t.Setenv("CC", "f")
	ptr := new(string)
	getValue("CC", ptr)
	if *ptr != "f" {
		t.Fatalf("*ptr != f")
	}
}
