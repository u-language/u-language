package main

import (
	"bufio"
	"context"
	"fmt"
	"os"
	"strings"
	"testing"

	"gitee.com/u-language/u-language/ucom/ast2"
	"gitee.com/u-language/u-language/ucom/cast"
	"gitee.com/u-language/u-language/ucom/check"
	"gitee.com/u-language/u-language/ucom/data"
	"gitee.com/u-language/u-language/ucom/errcode"
	. "gitee.com/u-language/u-language/ucom/internal/parser"
	"gitee.com/u-language/u-language/ucom/internal/utils"
	"gitee.com/u-language/u-language/ucom/lex2"
)

const Thread2 = true

func BenchmarkParserStr(b *testing.B) {
	type args struct {
		str []string
	}
	tests := []struct {
		name string
		args args
	}{
		{"多个变量声明并赋值", args{str: reta1()}},
		{"多个goto", args{str: reta2()}},
		{"多个malloc", args{str: reta3()}},
		{"多个if", args{str: reta4()}},
		{"多个3子句for", args{str: reta5()}},
		{"多个1字段结构体", args{str: reta6()}},
		{"多个选择器x.y", args{str: reta7()}},
		{"多个mallocSize", args{str: reta8()}},
		{"多个枚举", args{str: reta9()}},
		{"多个break和continue", args{str: reta10()}},
		{"多个常量声明并赋值", args{str: reta11()}},
		{"多个多行注释", args{str: reta12()}},
	}
	for _, tt := range tests {
		b.Run(tt.name, func(b *testing.B) {
			b.SetBytes(1)
			b.ReportAllocs()
			for i := 0; i < b.N; i++ {
				tree := ParserStr(tt.args.str)
				check.CheckTree(tree, errcode.DefaultErrCtx, false)
				toc := cast.NewUtoC()
				toc.Parser(tree, false)
				toc.C()
			}
		})
	}
}

const n = 150

func reta1() []string {
	i := 0
	return generateTestDate(&a1once, []string{"s:"}, func() []string {
		i++
		return []string{fmt.Sprint("var ", "a", i, " int=1")}
	})
}

func reta2() []string {
	return generateTestDate(&a2once, []string{"s:"}, func() []string {
		return []string{"goto s"}
	})
}

func reta3() []string {
	return generateTestDate(&a3once, nil, func() []string {
		return []string{"malloc(int)"}
	})
}

func reta4() []string {
	return generateTestDate(&a4once, nil, func() []string {
		return []string{
			"if true==true{",
			"}"}
	})
}

func reta5() []string {
	return generateTestDate(&a5once, nil, func() []string {
		return []string{
			"for var i int;i<100;i++{",
			"}"}
	})
}

func reta6() []string {
	i := 0
	return generateTestDate(&a6once, nil, func() []string {
		i++
		return []string{
			fmt.Sprint("struct a", i, "{"),
			"a int",
			"}"}
	})
}

func reta7() []string {
	return generateTestDate(&a7once, []string{"struct a{", "len int", "}", "var s a"}, func() []string {
		return []string{"s.len=1"}
	})
}

func reta8() []string {
	return generateTestDate(&a8once, nil, func() []string { return []string{"mallocSize(8)"} })
}

func reta9() []string {
	i := 0
	return generateTestDate(&a9once, nil, func() []string {
		i++
		return []string{
			fmt.Sprintf("enum s%d {", i),
			"d",
			"}"}
	})
}

func reta10() []string {
	return generateTestDate(&a10once, []string{"var i int=1"}, func() []string {
		return []string{
			"for ;; {",
			"if i==1{",
			"break",
			"}",
			"else{",
			"continue",
			"}",
			"}"}
	})
}

func reta11() []string {
	i := 0
	return generateTestDate(&a11once, nil, func() []string {
		i++
		return []string{fmt.Sprintf("const s%d int=0", i)}
	})
}

func reta12() []string {
	return generateTestDate(&a12once, nil, func() []string { return []string{"/*", "", "*/"} })
}

var (
	a1once  data.OnceValue[[]string]
	a2once  data.OnceValue[[]string]
	a3once  data.OnceValue[[]string]
	a4once  data.OnceValue[[]string]
	a5once  data.OnceValue[[]string]
	a6once  data.OnceValue[[]string]
	a7once  data.OnceValue[[]string]
	a8once  data.OnceValue[[]string]
	a9once  data.OnceValue[[]string]
	a10once data.OnceValue[[]string]
	a11once data.OnceValue[[]string]
	a12once data.OnceValue[[]string]
)

func TestMain(m *testing.M) {
	go errcode.Handle(context.Background(), func(err errcode.ErrInfo) { panic(err.String()) }, func() {})
	errcode.Debug = true
	utils.Debug = true
	os.Exit(m.Run())
}

// FuzzParserStr 测试寻找导致编译器崩溃的字符串
func FuzzParserStr(f *testing.F) {
	errctx := errcode.NewErrCtx()
	go errctx.Handle(context.Background(), func(err errcode.ErrInfo) {}, func() {})
	f.Fuzz(func(t *testing.T, s string) {
		iobuf := bufio.NewReader(strings.NewReader(s))
		str, err := utils.ReadAllLine(iobuf)
		TryErr(t, err)
		tree := ParserStr(str, errctx)
		check.CheckTree(tree, errctx, false)
		if errctx.Errbol() { //如果有错误
			return
		}
		if t == nil { //如果是空文件
			return
		}
		toc := cast.NewUtoC()
		toc.Parser(tree, false)
	})
}

func generateTestDate(once *data.OnceValue[[]string], start []string, repeat func() []string) []string {
	ret, ok := once.Get()
	if ok {
		return ret
	}
	ret = make([]string, 0, n)
	ret = append(ret, "package main", "func main(){")
	ret = append(ret, start...)
	r := repeat()
	for len(ret)+len(r) <= n-1 {
		ret = append(ret, r...)
		r = repeat()
	}
	ret = append(ret, "}")
	if cap(ret) > n {
		panic("cap(ret) > n")
	}
	once.Set(ret)
	return ret
}

func FuzzParserStr2(f *testing.F) {
	errctx := errcode.NewErrCtx()
	go errctx.Handle(context.Background(), func(err errcode.ErrInfo) {}, func() {})
	f.Fuzz(func(t *testing.T, s string) {
		s = "package main\n" + s
		ft := lex2.NewFileToken("1.txt", strings.NewReader(s), errctx, false)
		_ = ast2.NewTree(ft, "1.txt", errctx, Thread)
	})
}
