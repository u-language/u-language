//go:build enable_amd64

package main

import (
	"fmt"
	"os"

	"gitee.com/u-language/u-language/ucom/ast"
	"gitee.com/u-language/u-language/ucom/fasm/amd64"
	idebug "gitee.com/u-language/u-language/ucom/internal/debug"
	"gitee.com/u-language/u-language/ucom/internal/utils"
)

func CallAmd64(ast *ast.Tree) {
	toasm := amd64.NewAstToAsm(Thread)
	toasm.Parser(ast)
	if idebug.Debug == "true" { //调试时操作
		fmt.Println(toasm.Asm())
	} else {
		fd, err := os.Create("out.asm")
		utils.MustErr(err)
		fd.WriteString(toasm.Asm()) //把汇编代码写到目标文件中
	}
}
