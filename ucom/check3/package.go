package check3

import (
	"gitee.com/u-language/u-language/ucom/ast2"
	"gitee.com/u-language/u-language/ucom/errcode"
)

// CheckPackage 检查一个包的所有抽象语法树
func CheckPackage(p *ast2.Package, errctx *errcode.ErrCtx, Thread bool) {
	for i := range p.Trees.Data {
		CheckTree(p.Trees.Data[i])
	}
}
