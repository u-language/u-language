package check3_test

import (
	"context"
	"fmt"
	"sync"
	"testing"
	"time"

	. "gitee.com/u-language/u-language/ucom/check3"
	"gitee.com/u-language/u-language/ucom/errcode"
	"gitee.com/u-language/u-language/ucom/internal/parser"
	"gitee.com/u-language/u-language/ucom/internal/utils"
)

var run sync.WaitGroup

func TestMain(m *testing.M) {
	errcode.Debug = true
	//这是为了调试时运行时不报告所有goroutine都睡着了
	go func() { select {} }()
	m.Run()
	run.Wait()
	time.Sleep(10 * time.Microsecond)
}

func TestCheckSyntaxErr(t *testing.T) {
	type args struct {
		str    []string
		errctx *errcode.ErrCtx
		cancel context.CancelFunc
		noLF   bool
	}
	newargs := func(str []string, errf func(errcode.ErrInfo), num int, name string) args {
		actx := errcode.NewErrCtx()
		astdctx, acancel := context.WithCancel(context.Background())
		i := 0
		s := make(chan struct{})
		run.Add(1)
		go actx.Handle(astdctx, func(info errcode.ErrInfo) { //每收到一个错误，i++
			i++
			errf(info)
		}, func() { s <- struct{}{} })
		go func() {
			defer run.Done()
			<-s           //等待处理所有错误完毕
			if num != i { //如果预期发生错误数量不等于实际发生错误数量
				panic(fmt.Errorf("%s 应该有%d个错误，实际有%d个错误", name, num, i))
			}
		}()
		ret := args{str: str, errctx: actx, cancel: acancel}
		return ret
	}
	a1 := newargs([]string{"package main", "func main(){", "var a int = 1.2", "}"}, func(err errcode.ErrInfo) {
		if !err.IsErr("1.txt", 3, errcode.NewMsgAssignTypeIsNotEqual("int", "float"), errcode.VARErr, errcode.TypeIsNotEqual) {
			panic("a1\n" + err.String())
		}
	}, 1, "a1")
	a2 := newargs([]string{"package main", "struct a{", "a int", "}", "func main(){", "var s &a = malloc(a)", "s.a=1", "}"}, func(err errcode.ErrInfo) {
		panic("a2\n" + err.String())
	}, 0, "a2")
	a3 := newargs([]string{"package main", "func inc(i int)int{", "return i+1", "}", "func main(){", "inc(1.4)", "}"}, func(err errcode.ErrInfo) {
		if !err.IsErr("1.txt", 6, errcode.NewMsgCallTypeIsNotEqual("int", "float", 1), errcode.CallErr, errcode.TypeIsNotEqual) {
			panic("a3\n" + err.String())
		}
	}, 1, "a3")
	a4 := newargs([]string{"package main", "func inc(i int)int{", "return i+1.9", "}", "func main(){", "}"}, func(err errcode.ErrInfo) {
		if !err.IsErr("1.txt", 3, errcode.NewMsgOpexprTypeIsNotEqual("int", "float"), errcode.TypeIsNotEqual) {
			panic("a4\n" + err.String())
		}
	}, 1, "a4")
	a5 := newargs([]string{"package main", "func inc[T k](i intf)p{", "}", "func main(){", "}"}, func(err errcode.ErrInfo) {
		if !err.IsErr("1.txt", 2, errcode.NewMsgUnknownSymbol("k"), errcode.FUNCErr, errcode.UnknownType) &&
			!err.IsErr("1.txt", 2, errcode.NewMsgUnknownSymbol("intf"), errcode.FUNCErr, errcode.UnknownType) &&
			!err.IsErr("1.txt", 2, errcode.NewMsgUnknownSymbol("p"), errcode.FUNCErr, errcode.UnknownType) {
			panic("a5\n" + err.String())
		}
	}, 3, "a5")
	a6 := newargs([]string{"package main", "func main(){", "var a int = 1", "if a{", "}", "}"}, func(err errcode.ErrInfo) {
		if !err.IsErr("1.txt", 4, nil, errcode.IfErr, errcode.ExprNoBool) {
			panic("a6\n" + err.String())
		}
	}, 1, "a6")
	a7 := newargs([]string{"package main", "func main(){", "var a int = 1", "if true{", "}", "else if a{", "}", "}"}, func(err errcode.ErrInfo) {
		if !err.IsErr("1.txt", 6, nil, errcode.ElseErr, errcode.ExprNoBool) {
			panic("a7\n" + err.String())
		}
	}, 1, "a7")
	a8 := newargs([]string{"package main", "func main(){", "for var a int=1.2;1;a=9.9{", "}", "}"}, func(err errcode.ErrInfo) {
		if !err.IsErr("1.txt", 3, errcode.NewMsgAssignTypeIsNotEqual("int", "float"), errcode.VARErr, errcode.TypeIsNotEqual) &&
			!err.IsErr("1.txt", 3, nil, errcode.ForErr, errcode.ExprNoBool) &&
			!err.IsErr("1.txt", 3, errcode.NewMsgAssignTypeIsNotEqual("int", "float"), errcode.ASSIGNErr, errcode.TypeIsNotEqual) {
			panic("a8\n" + err.String())
		}
	}, 3, "a8")
	a9 := newargs([]string{"package main", "func main(){", "switch a{", "case 1:", "}", "switch 1{", "case 1.2:", "}", "}"}, func(err errcode.ErrInfo) {
		if !err.IsErr("1.txt", 3, errcode.NewMsgUnknownSymbol("a"), errcode.SwitchErr, errcode.UnknownSymbol) &&
			!err.IsErr("1.txt", 7, errcode.NewMsgSwitchAndCaseTypNoEqual("int", "float"), errcode.CaseErr, errcode.TypeIsNotEqual) {
			panic("a9\n" + err.String())
		}
	}, 2, "a9")
	a10 := newargs([]string{"package main", "func main(){", "var a bool", "a++", "var b &int", "b++", "}"}, func(err errcode.ErrInfo) {
		if !err.IsErr("1.txt", 6, nil, errcode.SelfOpStmtErr, errcode.PtrOpOnlyCmp) && !err.IsErr("1.txt", 4, nil, errcode.SelfOpStmtErr, errcode.DestCannotSelfIncOrDec) {
			panic("a10\n" + err.String())
		}
	}, 2, "a10")
	a11 := newargs([]string{"package main", "func main(){", "var a int = 1", "a[0]=1", "var b [2]int", "b[1.2]=0", "}"}, func(err errcode.ErrInfo) {
		if !err.IsErr("1.txt", 4, nil, errcode.IndexExprErr, errcode.IndexExprXErr) && !err.IsErr("1.txt", 6, nil, errcode.IndexExprErr, errcode.IndexLenMustIsInt) {
			panic("a11\n" + err.String())
		}
	}, 2, "a11")
	a12 := newargs([]string{"package main", "func main(){", "s:", "s:", "goto a", "}"}, func(err errcode.ErrInfo) {
		if !err.IsErr("1.txt", 4, errcode.NewMsgSymbolRepeat("s", "1.txt", "1.txt", 4, 3), errcode.LabelErr, errcode.SymbolRepeat) &&
			!err.IsErr("1.txt", 5, errcode.NewMsgLabelNoExist("a"), errcode.GotoErr) {
			panic("a12\n" + err.String())
		}
	}, 2, "a12")
	a13 := newargs([]string{"package main", "func main(){", "const a int = 1.2", "}"}, func(err errcode.ErrInfo) {
		if !err.IsErr("1.txt", 3, errcode.NewMsgAssignTypeIsNotEqual("int", "float"), errcode.ConstErr, errcode.TypeIsNotEqual) {
			panic("a13\n" + err.String())
		}
	}, 1, "a13")
	a14 := newargs([]string{"package main", "func main(){", "const a int = 1", "a=1", "}"}, func(err errcode.ErrInfo) {
		if !err.IsErr("1.txt", 4, errcode.NewMsgAssignToConst("a"), errcode.ASSIGNErr) {
			panic("a14\n" + err.String())
		}
	}, 1, "a14")
	a15 := newargs([]string{"package main", "func main(){", "var b float", "const a int = int(1)", "b=float(1.1)", "printf()", "printf(c)", "}"}, func(err errcode.ErrInfo) {
		if !err.IsErr("1.txt", 4, nil, errcode.CallErr, errcode.CanOnlyConvertFloatToInt) && !err.IsErr("1.txt", 5, nil, errcode.CallErr, errcode.CanOnlyConvertIntToFloat) &&
			!err.IsErr("1.txt", 6, nil, errcode.CallErr, errcode.InsuParame) && !err.IsErr("1.txt", 7, errcode.NewMsgUnknownSymbol("c"), errcode.UnknownSymbol) {
			panic("a15\n" + err.String())
		}
	}, 4, "a15")
	a16 := newargs([]string{"package main", "func f(i int){", "}", "func main(){", "f(p)", "}"}, func(err errcode.ErrInfo) {
		if !err.IsErr("1.txt", 5, errcode.NewMsgUnknownSymbol("p"), errcode.CallErr, errcode.UnknownSymbol) {
			panic("a16\n" + err.String())
		}
	}, 1, "a16")
	a17 := newargs([]string{"package main", "func main(){", "var a int", "a=printf(\"g\")", "}"}, func(err errcode.ErrInfo) {
		if !err.IsErr("1.txt", 4, errcode.NewMsgAssignTypeIsNotEqual("int", ""), errcode.ASSIGNErr, errcode.TypeIsNotEqual) {
			panic("a17\n" + err.String())
		}
	}, 1, "a17")
	a18 := newargs([]string{"package main", "struct s{", "p int", "}", "func main(){", "var v s", "v.f.p()", "}", "method f(v s){", "}"}, func(err errcode.ErrInfo) {
		if !err.IsErr("1.txt", 7, errcode.NewMsgSelectorLNoSelectorR("f", "p"), errcode.CallErr, errcode.SelectorLNoSelectorR) {
			panic("a18\n" + err.String())
		}
	}, 1, "a18")
	a19 := newargs([]string{"package main", "struct s{", "p e", "}", "enum e{", "d", "}", "func main(){", "var v s", "v.p.d()", "}"}, func(err errcode.ErrInfo) {
		if !err.IsErr("1.txt", 10, errcode.NewMsgSelectorLNoSelectorR("p", "d"), errcode.CallErr, errcode.SelectorLNoSelectorR) {
			panic("a19\n" + err.String())
		}
	}, 1, "a19")
	a20 := newargs([]string{"package main", "struct s{", "p e", "}", "enum e{", "d", "}", "func main(){", "var v s", "v.p()", "}"}, func(err errcode.ErrInfo) {
		if !err.IsErr("1.txt", 10, nil, errcode.CallErr, errcode.CallNoFunc) {
			panic("a20\n" + err.String())
		}
	}, 1, "a10")
	tests := []struct {
		name string
		args args
	}{
		{"变量初始化类型不匹配", a1},
		{"正确的给结构体指针字段赋值", a2},
		{"调用类型不相等", a3},
		{"int+float", a4},
		{"函数声明未知的类型", a5},
		{"if表达式表达的不是布尔值", a6},
		{"else if表达式表达的不是布尔值", a7},
		{"for语句三个子句都有错误", a8},
		{"switch和case语句错误", a9},
		{"自操作语句错误", a10},
		{"索引表达式错误", a11},
		{"goto和标签错误", a12},
		{"常量初始化类型不匹配", a13},
		{"给常量赋值", a14},
		{"检查printf,int,float", a15},
		{"参数因为未知的的符号未获得类型", a16},
		{"a=printf()", a17},
		{"v.f.p()左值没有右值", a18},
		{"涉及枚举的多个点选择器", a19},
		{"选择器调用非函数", a20},
	}
	for _, ttf := range tests {
		tt := ttf
		t.Run(tt.name, func(t *testing.T) {
			parser.ParserStr2(tt.args.str, parser.WithErrCtx(tt.args.errctx), parser.WithnoLF(tt.args.noLF), parser.WithCheck(true))
			tt.args.cancel()
		})
	}
}

func init() {
	utils.Debug = true
	errcode.Debug = true
	errcode.Debug2 = true
	Debug = true
}
