package ir2

// OPEnum 指令枚举
type OPEnum int

const (
	NoOp OPEnum = iota //零值表示未定义的指令
	ADDOP
	SUBOP
	MOVOP
	VarOP
	ConstOP
	MULOP
	DIVOP
	FuncOP
	CallOP
	ObjOP
	RbraceOP
	TmpVarOP
	RetOp
	PaPaOp //Passing Parameters
	EndCallOp
	ForOp
	SemicolonOp
	LessOp
	GreaterOp
	EqualOp
	NoEqualOp
	CommaOp
	LbraceOp
	LineFeedOp
	LocalVarOp
	RPARENOp
	IfOp
	RemainOp
	ElseOp
	ElseIfOp
	TypeConvertOp
	Struct1Field
	Struct2Field
	StructDeclStart
	StructDeclEnd
	Field
	Field3
	AssignOp
	LeftSelectRight
	SelectRight
	Malloc
)

var OPEnumStr = [...]string{
	NoOp:            "no_op (未定义的指令)",
	ADDOP:           "add （加法运算）",
	SUBOP:           "sub (减法运算)",
	MOVOP:           "mov （移动操作）",
	VarOP:           "var（变量声明）",
	ConstOP:         "const（常量声明）",
	MULOP:           "mul（乘法运算）",
	DIVOP:           "dlv 除法运算",
	FuncOP:          "func (函数声明)",
	CallOP:          "call（调用）",
	ObjOP:           "obj（对象）",
	RbraceOP:        "右大括号 ",
	TmpVarOP:        "tmp（临时变量）",
	RetOp:           "ret（返回）",
	PaPaOp:          "papa（传递参数）",
	EndCallOp:       "结束调用 ",
	ForOp:           "for ",
	SemicolonOp:     "分号",
	LessOp:          "less（小于）",
	GreaterOp:       "greater（大于）",
	EqualOp:         "equal（等于）",
	NoEqualOp:       "noequal（不等与）",
	CommaOp:         "comma（逗号）",
	LbraceOp:        "左大括号",
	LineFeedOp:      "linefeed（空行）",
	LocalVarOp:      "loacl var（局部变量）",
	RPARENOp:        "rparen（右小括号）",
	IfOp:            "if",
	RemainOp:        "remain（取余数）",
	ElseOp:          "else",
	ElseIfOp:        "else if",
	TypeConvertOp:   "type convert（类型转换）",
	Struct1Field:    "struct 1 field（1字段结构体）",
	Struct2Field:    "struct 2 field（2字段结构体）",
	StructDeclStart: "struct decl start（结构体开始）",
	StructDeclEnd:   "struct decl end（结构体结束）",
	Field:           "field（字段）",
	Field3:          "field3（3个字段）",
	AssignOp:        "assign (=)",
	LeftSelectRight: "left select right（选择器左值和右值）",
	SelectRight:     "select right（选择器右值）",
	Malloc:          "malloc（分配一个类型的内存）",
}

func (o OPEnum) String() string {
	return OPEnumStr[o]
}
