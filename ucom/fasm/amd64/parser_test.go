package amd64

import (
	"context"
	"reflect"
	"testing"

	"gitee.com/u-language/u-language/ucom/ast"
	"gitee.com/u-language/u-language/ucom/errcode"
	"gitee.com/u-language/u-language/ucom/internal/parser"
	"gitee.com/u-language/u-language/ucom/lex"
)

const r1 = 150

func BenchmarkParserNode(b *testing.B) {
	b.StopTimer()
	ast1 := retAstTree()
	if ast1 == nil {
		b.Error("空ast树")
	}
	b.StartTimer()
	b.ReportAllocs()
	b.SetBytes(34)
	for i := 0; i < b.N; i++ {
		to := NewAstToAsm(true)
		to.Parser(ast1)
	}
}

func TestMain(m *testing.M) {
	go errcode.Handle(context.Background(), func(err errcode.ErrInfo) { panic(err.String()) }, func() {})
	m.Run()
}

func retAstTree() *ast.Tree {
	codebuf := make([]string, r1)
	i := 0
	codebuf[i] = "var b int = 4"
	i++
	for ; i < r1/2; i++ {
		codebuf[i] = "b = 5"
	}
	codebuf[i] = "func main() {"
	i++
	codebuf[i] = "var j int = 9"
	i++
	for ; i < r1-1; i++ {
		codebuf[i] = "j = 29"
	}
	codebuf[i] = "}"
	ft := lex.Lex("2", codebuf, errcode.DefaultErrCtx, false)
	ast1 := ast.NewTree(ft, errcode.DefaultErrCtx, ast.NewSbt(true), nil)
	return ast1
}

func TestParserFuncASSIGN(t *testing.T) {
	type args struct {
		to    *AstToAsm
		node  *ast.ASSIGNNode
		vreg  *VReg
		sbt   *ast.Sbt
		stack *Stack
	}
	a1tree := parser.ParserStr([]string{"var b int", "b=b+1+2"})
	a1 := args{NewAstToAsm(false), a1tree.Nodes[1].(*ast.ASSIGNNode), NewVReg(), a1tree.Sbt, NewStack(a1tree.Sbt)}
	w1 := NewAssignKind([]TextNode{NewSrcNumDestMem("b", "3", ADD)})
	//a2
	a2tree := parser.ParserStr([]string{"var b int", "b=b+b"})
	a2 := args{NewAstToAsm(false), a2tree.Nodes[1].(*ast.ASSIGNNode), NewVReg(), a2tree.Sbt, NewStack(a2tree.Sbt)}
	w2node := []TextNode{NewSrcMemDestReg(RAX, "b", MOV), NewSrcRegDestMem("b", RAX, ADD)}
	w2 := NewAssignKind(w2node)
	w2.vreg_use_info.X86 = 1
	//a3
	a3tree := parser.ParserStr([]string{"var b float", "b=b+1.2+3.2"})
	a3 := args{NewAstToAsm(false), a3tree.Nodes[1].(*ast.ASSIGNNode), NewVReg(), a3tree.Sbt, NewStack(a3tree.Sbt)}
	w3node := []TextNode{NewSrcMemDestReg(Xmm0, "auto_1", MOVSS), NewSrcRegDestMem("b", Xmm0, ADDSS)}
	w3 := NewAssignKind(w3node)
	w3.vreg_use_info.SSE = 1
	//a4
	a4tree := parser.ParserStr([]string{"var b float", "b=b+b"})
	a4 := args{NewAstToAsm(false), a4tree.Nodes[1].(*ast.ASSIGNNode), NewVReg(), a4tree.Sbt, NewStack(a4tree.Sbt)}
	w4node := []TextNode{NewSrcMemDestReg(Xmm0, "b", MOVSS), NewSrcRegDestMem("b", Xmm0, ADDSS)}
	w4 := NewAssignKind(w4node)
	w4.vreg_use_info.SSE = 1
	//a5
	a5tree := parser.ParserStr([]string{"var b int", "b=b+b+b"})
	a5 := args{NewAstToAsm(false), a5tree.Nodes[1].(*ast.ASSIGNNode), NewVReg(), a5tree.Sbt, NewStack(a5tree.Sbt)}
	w5node := []TextNode{NewSrcMemDestReg(RAX, "b", MOV), NewSrcMemDestReg(RAX, "b", ADD), NewSrcRegDestMem("b", RAX, ADD)}
	w5 := NewAssignKind(w5node)
	w5.vreg_use_info.X86 = 1
	//a6
	a6tree := parser.ParserStr([]string{"var b float", "b=b+b+b"})
	a6 := args{NewAstToAsm(false), a6tree.Nodes[1].(*ast.ASSIGNNode), NewVReg(), a6tree.Sbt, NewStack(a6tree.Sbt)}
	w6node := []TextNode{NewSrcMemDestReg(Xmm0, "b", MOVSS), NewSrcMemDestReg(Xmm0, "b", ADDSS), NewSrcRegDestMem("b", Xmm0, ADDSS)}
	w6 := NewAssignKind(w6node)
	w6.vreg_use_info.SSE = 1
	//a7
	a7tree := parser.ParserStr([]string{"var b int", "b=2+b+b+b+3"})
	a7 := args{NewAstToAsm(false), a7tree.Nodes[1].(*ast.ASSIGNNode), NewVReg(), a7tree.Sbt, NewStack(a7tree.Sbt)}
	w7node := []TextNode{NewSrcMemDestReg(RAX, "b", MOV), NewSrcNumDestReg(RAX, "2", ADD), NewSrcMemDestReg(RAX, "b", ADD), NewSrcMemDestReg(RAX, "b", ADD), NewSrcNumDestReg(RAX, "3", ADD), NewSrcRegDestMem("b", RAX, MOV)}
	w7 := NewAssignKind(w7node)
	w7.vreg_use_info.X86 = 1
	//a8
	a8tree := parser.ParserStr([]string{"var b float", "b=2.0+b+b+b+3.0"})
	a8 := args{NewAstToAsm(false), a8tree.Nodes[1].(*ast.ASSIGNNode), NewVReg(), a8tree.Sbt, NewStack(a8tree.Sbt)}
	w8node := []TextNode{NewSrcMemDestReg(Xmm0, "auto_1", MOVSS), NewSrcMemDestReg(Xmm0, "b", ADDSS), NewSrcMemDestReg(Xmm0, "b", ADDSS), NewSrcMemDestReg(Xmm0, "b", ADDSS), NewSrcMemDestReg(Xmm0, "auto_2", ADDSS), NewSrcRegDestMem("b", Xmm0, MOVSS)}
	w8 := NewAssignKind(w8node)
	w8.vreg_use_info.SSE = 1
	//a9
	a9tree := parser.ParserStr([]string{"func main(){", "var b int", "b=b+1+2", "}"})
	a9 := args{NewAstToAsm(false), a9tree.Nodes[2].(*ast.ASSIGNNode), NewVReg(), a9tree.Nodes[0].(*ast.FuncNode).Sbt, NewStack(a9tree.Nodes[0].(*ast.FuncNode).Sbt)}
	w9node := []TextNode{NewSrcNumDestRegOffset(RBP, "3", -8, ADD)}
	w9 := NewAssignKind(w9node)
	//a10
	a10tree := parser.ParserStr([]string{"var b int", "b=b+b+c+c", "var c int"})
	a10 := args{NewAstToAsm(false), a10tree.Nodes[1].(*ast.ASSIGNNode), NewVReg(), a10tree.Sbt, NewStack(a10tree.Sbt)}
	w10node := []TextNode{NewSrcMemDestReg(RAX, "b", MOV), NewSrcMemDestReg(RAX, "b", ADD), NewSrcMemDestReg(RAX, "c", ADD), NewSrcMemDestReg(RAX, "c", ADD), NewSrcRegDestMem("b", RAX, MOV)}
	w10 := NewAssignKind(w10node)
	w10.vreg_use_info.X86 = 1
	//a11
	a11tree := parser.ParserStr([]string{"func main(){", "var b int", "b=b+b+b+b", "}"})
	a11 := args{NewAstToAsm(false), a11tree.Nodes[2].(*ast.ASSIGNNode), NewVReg(), a11tree.Nodes[0].(*ast.FuncNode).Sbt, NewStack(a11tree.Nodes[0].(*ast.FuncNode).Sbt)}
	w11node := []TextNode{NewSrcRegOffsetDestReg(RAX, RBP, -8, MOV), NewSrcRegOffsetDestReg(RAX, RBP, -8, ADD), NewSrcRegOffsetDestReg(RAX, RBP, -8, ADD), NewSrcRegDestRegOffset(RBP, RAX, -8, ADD)}
	w11 := NewAssignKind(w11node)
	w11.vreg_use_info.X86 = 1
	//a12
	a12tree := parser.ParserStr([]string{"func main(){", "var b float", "b=b+1.0+2.0", "}"})
	a12 := args{NewAstToAsm(false), a12tree.Nodes[2].(*ast.ASSIGNNode), NewVReg(), a12tree.Nodes[0].(*ast.FuncNode).Sbt, NewStack(a12tree.Nodes[0].(*ast.FuncNode).Sbt)}
	w12node := []TextNode{NewSrcMemDestReg(Xmm0, "auto_1", MOVSS), NewSrcRegDestRegOffset(RBP, Xmm0, -4, ADDSS)}
	w12 := NewAssignKind(w12node)
	w12.vreg_use_info.SSE = 1
	//a13
	a13tree := parser.ParserStr([]string{"func main(){", "var b float", "b=b+b+b", "}"})
	a13 := args{NewAstToAsm(false), a13tree.Nodes[2].(*ast.ASSIGNNode), NewVReg(), a13tree.Nodes[0].(*ast.FuncNode).Sbt, NewStack(a13tree.Nodes[0].(*ast.FuncNode).Sbt)}
	w13node := []TextNode{NewSrcRegOffsetDestReg(Xmm0, RBP, -4, MOVSS), NewSrcRegOffsetDestReg(Xmm0, RBP, -4, ADDSS), NewSrcRegDestRegOffset(RBP, Xmm0, -4, ADDSS)}
	w13 := NewAssignKind(w13node)
	w13.vreg_use_info.SSE = 1
	//a14
	a14tree := parser.ParserStr([]string{"func main(){", "var b float", "b=2.0+b+3.0+b+3.0", "}"})
	a14 := args{NewAstToAsm(false), a14tree.Nodes[2].(*ast.ASSIGNNode), NewVReg(), a14tree.Nodes[0].(*ast.FuncNode).Sbt, NewStack(a14tree.Nodes[0].(*ast.FuncNode).Sbt)}
	w14node := []TextNode{NewSrcMemDestReg(Xmm0, "auto_1", MOVSS), NewSrcRegOffsetDestReg(Xmm0, RBP, -4, ADDSS), NewSrcMemDestReg(Xmm0, "auto_2", ADDSS), NewSrcRegOffsetDestReg(Xmm0, RBP, -4, ADDSS), NewSrcMemDestReg(Xmm0, "auto_3", ADDSS), NewSrcRegDestRegOffset(RBP, Xmm0, -4, MOVSS)}
	w14 := NewAssignKind(w14node)
	w14.vreg_use_info.SSE = 1
	//a15
	a15tree := parser.ParserStr([]string{"var b int", "b=2+b+2+b+3"})
	a15 := args{NewAstToAsm(false), a15tree.Nodes[1].(*ast.ASSIGNNode), NewVReg(), a15tree.Sbt, NewStack(a15tree.Sbt)}
	w15node := []TextNode{NewSrcMemDestReg(RAX, "b", MOV), NewSrcNumDestReg(RAX, "2", ADD), NewSrcNumDestReg(RAX, "2", ADD), NewSrcMemDestReg(RAX, "b", ADD), NewSrcNumDestReg(RAX, "3", ADD), NewSrcRegDestMem("b", RAX, MOV)}
	w15 := NewAssignKind(w15node)
	w15.vreg_use_info.X86 = 1
	//a16
	a16tree := parser.ParserStr([]string{"var b int", "b=b+2+b+3"})
	a16 := args{NewAstToAsm(false), a16tree.Nodes[1].(*ast.ASSIGNNode), NewVReg(), a16tree.Sbt, NewStack(a16tree.Sbt)}
	w16node := []TextNode{NewSrcMemDestReg(RAX, "b", MOV), NewSrcNumDestReg(RAX, "2", ADD), NewSrcMemDestReg(RAX, "b", ADD), NewSrcNumDestReg(RAX, "3", ADD), NewSrcRegDestMem("b", RAX, MOV)}
	w16 := NewAssignKind(w16node)
	w16.vreg_use_info.X86 = 1
	//a17
	a17tree := parser.ParserStr([]string{"var b float", "b=b+2.0+b+3.0"})
	a17 := args{NewAstToAsm(false), a17tree.Nodes[1].(*ast.ASSIGNNode), NewVReg(), a17tree.Sbt, NewStack(a17tree.Sbt)}
	w17node := []TextNode{NewSrcMemDestReg(Xmm0, "auto_1", MOVSS), NewSrcMemDestReg(Xmm0, "b", ADDSS), NewSrcMemDestReg(Xmm0, "b", ADDSS), NewSrcMemDestReg(Xmm0, "auto_2", ADDSS), NewSrcRegDestMem("b", Xmm0, MOVSS)}
	w17 := NewAssignKind(w17node)
	w17.vreg_use_info.SSE = 1
	//a18
	a18tree := parser.ParserStr([]string{"func main(){", "var b int", "b=2+b+b+b+3", "}"})
	a18 := args{NewAstToAsm(false), a18tree.Nodes[2].(*ast.ASSIGNNode), NewVReg(), a18tree.Nodes[0].(*ast.FuncNode).Sbt, NewStack(a18tree.Nodes[0].(*ast.FuncNode).Sbt)}
	w18node := []TextNode{NewSrcRegOffsetDestReg(RAX, RBP, -8, MOV), NewSrcNumDestReg(RAX, "2", ADD), NewSrcRegOffsetDestReg(RAX, RBP, -8, ADD), NewSrcRegOffsetDestReg(RAX, RBP, -8, ADD), NewSrcNumDestReg(RAX, "3", ADD), NewSrcRegDestRegOffset(RBP, RAX, -8, MOV)}
	w18 := NewAssignKind(w18node)
	w18.vreg_use_info.X86 = 1
	//a19
	a19tree := parser.ParserStr([]string{"func main(){", "var b int", "b=b+2+b+3", "}"})
	a19 := args{NewAstToAsm(false), a19tree.Nodes[2].(*ast.ASSIGNNode), NewVReg(), a19tree.Nodes[0].(*ast.FuncNode).Sbt, NewStack(a19tree.Nodes[0].(*ast.FuncNode).Sbt)}
	w19node := []TextNode{NewSrcRegOffsetDestReg(RAX, RBP, -8, MOV), NewSrcNumDestReg(RAX, "2", ADD), NewSrcRegOffsetDestReg(RAX, RBP, -8, ADD), NewSrcNumDestReg(RAX, "3", ADD), NewSrcRegDestRegOffset(RBP, RAX, -8, MOV)}
	w19 := NewAssignKind(w19node)
	w19.vreg_use_info.X86 = 1
	//a20
	a20tree := parser.ParserStr([]string{"var b float", "b=b+b+c+c", "var c float"})
	a20 := args{NewAstToAsm(false), a20tree.Nodes[1].(*ast.ASSIGNNode), NewVReg(), a20tree.Sbt, NewStack(a20tree.Sbt)}
	w20node := []TextNode{NewSrcMemDestReg(Xmm0, "b", MOVSS), NewSrcMemDestReg(Xmm0, "b", ADDSS), NewSrcMemDestReg(Xmm0, "c", ADDSS), NewSrcMemDestReg(Xmm0, "c", ADDSS), NewSrcRegDestMem("b", Xmm0, MOVSS)}
	w20 := NewAssignKind(w20node)
	w20.vreg_use_info.SSE = 1
	//a21
	a21tree := parser.ParserStr([]string{"var b float", "b=2.0+b+2.0+b+3.0"})
	a21 := args{NewAstToAsm(false), a21tree.Nodes[1].(*ast.ASSIGNNode), NewVReg(), a21tree.Sbt, NewStack(a21tree.Sbt)}
	w21node := []TextNode{NewSrcMemDestReg(Xmm0, "auto_1", MOVSS), NewSrcMemDestReg(Xmm0, "b", ADDSS), NewSrcMemDestReg(Xmm0, "auto_2", ADDSS), NewSrcMemDestReg(Xmm0, "b", ADDSS), NewSrcMemDestReg(Xmm0, "auto_3", ADDSS), NewSrcRegDestMem("b", Xmm0, MOVSS)}
	w21 := NewAssignKind(w21node)
	w21.vreg_use_info.SSE = 1
	//a22
	a22tree := parser.ParserStr([]string{"func main(){", "var b int", "b=2+b+2+b+3", "}"})
	a22 := args{NewAstToAsm(false), a22tree.Nodes[2].(*ast.ASSIGNNode), NewVReg(), a22tree.Nodes[0].(*ast.FuncNode).Sbt, NewStack(a22tree.Nodes[0].(*ast.FuncNode).Sbt)}
	w22node := []TextNode{NewSrcRegOffsetDestReg(RAX, RBP, -8, MOV), NewSrcNumDestReg(RAX, "2", ADD), NewSrcNumDestReg(RAX, "2", ADD), NewSrcRegOffsetDestReg(RAX, RBP, -8, ADD), NewSrcNumDestReg(RAX, "3", ADD), NewSrcRegDestRegOffset(RBP, RAX, -8, MOV)}
	w22 := NewAssignKind(w22node)
	w22.vreg_use_info.X86 = 1
	//a23
	a23tree := parser.ParserStr([]string{"func main(){", "var b float", "b=b+2.0+b+3.0", "}"})
	a23 := args{NewAstToAsm(false), a23tree.Nodes[2].(*ast.ASSIGNNode), NewVReg(), a23tree.Nodes[0].(*ast.FuncNode).Sbt, NewStack(a19tree.Nodes[0].(*ast.FuncNode).Sbt)}
	w23node := []TextNode{NewSrcMemDestReg(Xmm0, "auto_1", MOVSS), NewSrcRegOffsetDestReg(Xmm0, RBP, -8, ADDSS), NewSrcRegOffsetDestReg(Xmm0, RBP, -8, ADDSS), NewSrcMemDestReg(Xmm0, "auto_2", ADDSS), NewSrcRegDestRegOffset(RBP, Xmm0, -8, MOVSS)}
	w23 := NewAssignKind(w23node)
	w23.vreg_use_info.SSE = 1
	tests := []struct {
		name string
		args args
		want TextNode
	}{
		//var为int型 testn=7
		{"b(varint)=b+1+2", a1, w1},             //op<var,immint,var>
		{"b(varint)=b+b", a2, w2},               //op<var,var,var> var相同
		{"b(varint)=b+b+b", a5, w5},             //op<var,var,tmp1> op<tmp1,var,var> var相同
		{"b(varint)=2+b+b+b+3", a7, w7},         //op<immint,var,tmp1> op<tmp1,var,tmp2> op<tmp2,var,tmp3> op<tmp3,immint,var>
		{"b(varint)=b+b+c(varint)+c", a10, w10}, //op<var,var,tmp1> op<tmp1,var,tmp2> op<tmp2,var,var>
		{"b(varint)=2+b+2+b+3", a15, w15},       //op<immint,var,tmp1> op<tmp1,immint,tmp2> op<tmp2,var,tmp3> op<tmp3,immint,var>
		{"b(varint)=b+2+b+3", a16, w16},         // op<var,immint,tmp1> op<tmp1,var,tmp2> op<tmp2,immint,var>

		//var为float型 testn =7
		{"b(varfloat)=b+1.2+2.2", a3, w3},           //op<var,immfloat,var>
		{"b(varfloat)=b+b", a4, w4},                 //op<var,var,var> var相同
		{"b(varfloat)=b+b+b", a6, w6},               //op<var,var,tmp1> op<tmp1,var,var> var相同
		{"b(varfloat)=2.0+b+b+b+3.0", a8, w8},       //op<immfloat,var,tmp1> op<tmp1,var,tmp2> op<tmp2,var,tmp3> op<tmp3,immfloat,var>
		{"b(varfloat)=b+b+c(varfloat)+c", a20, w20}, //op<var,var,tmp1> op<tmp1,var,tmp2> op<tmp2,var,var>
		{"b(varfloat)=2.0+b+2.0+b+3.0", a21, w21},   //op<immfloat,var,tmp1> op<tmp1,immfloat,tmp2> op<tmp2,var,tmp3> op<tmp3,immfloat,var>
		{"b(varfloat)=b+2.0+b+3.0", a17, w17},       // op<var,immfloat,tmp1> op<tmp1,var,tmp2> op<tmp2,immfloat,var>

		//var为int型,栈上分配 testn=6
		{"b(stackvarint)=b+1+2", a9, w9},       //op<stackvar,immint,stackvar>
		{"b(stackvarint)=b+b+b+b", a11, w11},   //op<stackvar,stackvar,tmp1> op<tmp1,stackvar,tmp2> op<tmp3,stackvar,stackvar>
		{"b(stackvarint)=2+b+b+b+3", a18, w18}, //op<immint,stackvar,tmp1> op<tmp1,stackvar,tmp2> op<tmp2,stackvar,tmp3> op<tmp3,immint,varstack>
		{"b(stackvarint)=2+b+2+b+3", a22, w22}, //op<immint,stackvar,tmp1> op<tmp1,immint,tmp2> op<tmp2,stackvar,tmp3> op<tmp3,immint,stackvar>
		{"b(stackvarint)=b+2+b+3", a19, w19},   // op<stackvar,immint,tmp1> op<tmp1,stackvar,tmp2> op<tmp2,immint,stackvar>

		//var为float型,栈上分配 testn=4
		{"b(stackvarfloat)=b+1.0+2.0", a12, w12},       //op<stackvar,immfloat,stackvar>
		{"b(stackvarfloat)=b+b+b", a13, w13},           //op<stackvar,stackvar,tmp1> op<tmp1,stackvar,stackvar>
		{"b(stcakvarfloat)=2.0+b+3.0+b+3.0", a14, w14}, //op<immfloat,stackvar,tmp1> op<tmp1,immfloat,tmp2> op<tmp2,stackvar,tmp3> op<tmp3,immfloat,stavkvar>
		{"b(stackvarfloat)=b+2.0+b+3.0", a23, w23},     // op<stackvar,immfloat,tmp1> op<tmp1,stackvar,tmp2> op<tmp2,immfloat,stackvar>

	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := ParserFuncASSIGN(tt.args.to, tt.args.node, tt.args.vreg, tt.args.sbt, tt.args.stack)
			AllocReg([][]TextNode{[]TextNode{got}}, NewRegRun(false), NewSseRegRun(false), _NewvRegToReg(), _NewvregToRegTable(), false)
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("ParserFuncASSIGN() = %v, want %v", got, tt.want)
			}
			autoint = 0
		})
	}
}

func init() {
	ast.NdSotfOn = true
	ast.FirstLineNoPackageDeclOn = true
}
