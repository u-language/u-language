// Deprecated:已弃用
package amd64

import (
	"fmt"
	"strconv"
	"strings"
)

var (
	// dest src
	_ TextNode = (*SrcMemDestReg)(nil)       // reg [mem]
	_ TextNode = (*SrcRegOffsetDestReg)(nil) // reg [reg+offset]
	_ TextNode = (*SrcRegMemDestReg)(nil)    // reg [reg]
	_ TextNode = (*SrcNumDestReg)(nil)       // reg num
	_ TextNode = (*SrcRegDestReg)(nil)       // reg reg
	_ TextNode = (*SrcRegDestMem)(nil)       // [mem] reg
	_ TextNode = (*SrcNumDestMem)(nil)       //[mem] num
	_ TextNode = (*SrcRegDestRegOffset)(nil) // [reg+offset] reg
	_ TextNode = (*SrcNumDestRegOffset)(nil) // [reg+offset] num
)

type SrcMemDestReg struct {
	Mem  string
	Reg  RegEnum
	Kind TextKindEnum
}

func NewSrcMemDestReg(dest RegEnum, src string, Kind TextKindEnum) *SrcMemDestReg {
	return &SrcMemDestReg{
		Reg:  dest,
		Mem:  src,
		Kind: Kind,
	}
}

func (text *SrcMemDestReg) Type() TextTypeEnum {
	return TypeSrcMemDestReg
}

func (text *SrcMemDestReg) String() string {
	var buf strings.Builder
	if text == nil {
		buf.WriteString("&amd64.SrcMemDestReg{ <nil> }")
	} else {
		buf.WriteString("&amd64.SrcMemDestReg")
		buf.WriteString(fmt.Sprintf("%+v", *text))
	}
	return buf.String()
}

func (text *SrcMemDestReg) Asm() string {
	var buf strings.Builder
	buf.WriteString(TextKindEnumStrMap[text.Kind])
	buf.WriteString(" ")
	buf.WriteString(text.Reg.Asm())
	buf.WriteString(",[")
	buf.WriteString(text.Mem)
	buf.WriteString("]")
	return buf.String()
}

func (text *SrcMemDestReg) GetVRegUseInfo() VRegUseInfo {
	return retVRegUseInfo(text.Reg)
}

type SrcRegDestMem struct {
	Mem  string
	Reg  RegEnum
	Kind TextKindEnum
}

func NewSrcRegDestMem(dest string, src RegEnum, Kind TextKindEnum) *SrcRegDestMem {
	return &SrcRegDestMem{
		Reg:  src,
		Mem:  dest,
		Kind: Kind,
	}
}

func (text *SrcRegDestMem) Type() TextTypeEnum {
	return TypeSrcRegDestMem
}

func (text *SrcRegDestMem) String() string {
	var buf strings.Builder
	if text == nil {
		buf.WriteString("&amd64.SrcRegDestMem{ <nil> }")
	} else {
		buf.WriteString("&amd64.SrcRegDestMem")
		buf.WriteString(fmt.Sprintf("%+v", *text))
	}
	return buf.String()
}

func (text *SrcRegDestMem) Asm() string {
	var buf strings.Builder
	buf.WriteString(TextKindEnumStrMap[text.Kind])
	buf.WriteString(" [")
	buf.WriteString(text.Mem)
	buf.WriteString("],")
	buf.WriteString(text.Reg.Asm())
	return buf.String()
}

func (text *SrcRegDestMem) GetVRegUseInfo() VRegUseInfo {
	return retVRegUseInfo(text.Reg)
}

type SrcNumDestReg struct {
	Num  string
	Reg  RegEnum
	Kind TextKindEnum
}

func NewSrcNumDestReg(dest RegEnum, src string, Kind TextKindEnum) *SrcNumDestReg {
	return &SrcNumDestReg{
		Reg:  dest,
		Num:  src,
		Kind: Kind,
	}
}

func (text *SrcNumDestReg) Type() TextTypeEnum {
	return TypeSrcNumDestReg
}

func (text *SrcNumDestReg) String() string {
	var buf strings.Builder
	if text == nil {
		buf.WriteString("&amd64.SrcNumDestReg{ <nil> }")
	} else {
		buf.WriteString("&amd64.SrcNumDestReg")
		buf.WriteString(fmt.Sprintf("%+v", *text))
	}
	return buf.String()
}

func (text *SrcNumDestReg) Asm() string {
	var buf strings.Builder
	buf.WriteString(TextKindEnumStrMap[text.Kind])
	buf.WriteString(" ")
	buf.WriteString(text.Reg.Asm())
	buf.WriteString(",")
	buf.WriteString(text.Num)
	return buf.String()
}

func (text *SrcNumDestReg) GetVRegUseInfo() VRegUseInfo {
	return retVRegUseInfo(text.Reg)
}

type SrcRegDestReg struct {
	Src  RegEnum
	Dest RegEnum
	Kind TextKindEnum
}

func NewSrcRegDestReg(Dest, Src RegEnum, Kind TextKindEnum) *SrcRegDestReg {
	return &SrcRegDestReg{
		Src:  Src,
		Dest: Dest,
		Kind: Kind,
	}
}

func (text *SrcRegDestReg) Type() TextTypeEnum {
	return TypeSrcRegDestReg
}

func (text *SrcRegDestReg) String() string {
	var buf strings.Builder
	if text == nil {
		buf.WriteString("&amd64.SrcRegDestReg{ <nil> }")
	} else {
		buf.WriteString("&amd64.SrcRegDestReg")
		buf.WriteString(fmt.Sprintf("%+v", *text))
	}
	return buf.String()
}

func (text *SrcRegDestReg) Asm() string {
	var buf strings.Builder
	buf.WriteString(TextKindEnumStrMap[text.Kind])
	buf.WriteString(" ")
	buf.WriteString(text.Dest.Asm())
	buf.WriteString(",")
	buf.WriteString(text.Src.Asm())
	return buf.String()
}

func (text *SrcRegDestReg) GetVRegUseInfo() VRegUseInfo {
	return retVRegUseInfo(text.Src, text.Dest)
}

type SrcRegOffsetDestReg struct {
	Src    RegEnum
	Dest   RegEnum
	Offset int64
	Kind   TextKindEnum
}

func NewSrcRegOffsetDestReg(Dest, Src RegEnum, offset int64, Kind TextKindEnum) *SrcRegOffsetDestReg {
	return &SrcRegOffsetDestReg{
		Src:    Src,
		Dest:   Dest,
		Kind:   Kind,
		Offset: offset,
	}
}

func (text *SrcRegOffsetDestReg) Type() TextTypeEnum {
	return TypeSrcRegOffsetDestReg
}

func (text *SrcRegOffsetDestReg) String() string {
	var buf strings.Builder
	if text == nil {
		buf.WriteString("&amd64.SrcRegOffsetDestReg{ <nil> }")
	} else {
		buf.WriteString("&amd64.SrcRegOffsetDestReg")
		buf.WriteString(fmt.Sprintf("%+v", *text))
	}
	return buf.String()
}

func (text *SrcRegOffsetDestReg) Asm() string {
	var buf strings.Builder
	buf.WriteString(TextKindEnumStrMap[text.Kind])
	buf.WriteString(" ")
	buf.WriteString(text.Dest.Asm())
	buf.WriteString(",[")
	buf.WriteString(text.Src.Asm())
	if text.Offset >= 0 {
		buf.WriteString("+")
	}
	buf.WriteString(strconv.FormatInt(text.Offset, 10))
	buf.WriteString("]")
	return buf.String()
}

func (text *SrcRegOffsetDestReg) GetVRegUseInfo() VRegUseInfo {
	return retVRegUseInfo(text.Src, text.Dest)
}

type SrcRegDestRegOffset struct {
	Src    RegEnum
	Dest   RegEnum
	Offset int64
	Kind   TextKindEnum
}

func NewSrcRegDestRegOffset(Dest, Src RegEnum, offset int64, Kind TextKindEnum) *SrcRegDestRegOffset {
	return &SrcRegDestRegOffset{
		Src:    Src,
		Dest:   Dest,
		Kind:   Kind,
		Offset: offset,
	}
}

func (text *SrcRegDestRegOffset) Type() TextTypeEnum {
	return TypeSrcRegDestRegOffset
}

func (text *SrcRegDestRegOffset) String() string {
	var buf strings.Builder
	if text == nil {
		buf.WriteString("&amd64.SrcRegDestRegOffset{ <nil> }")
	} else {
		buf.WriteString("&amd64.SrcRegDestRegOffset")
		buf.WriteString(fmt.Sprintf("%+v", *text))
	}
	return buf.String()
}

func (text *SrcRegDestRegOffset) Asm() string {
	var buf strings.Builder
	buf.WriteString(TextKindEnumStrMap[text.Kind])
	buf.WriteString(" [")
	buf.WriteString(text.Dest.Asm())
	if text.Offset >= 0 {
		buf.WriteString("+")
	}
	buf.WriteString(strconv.FormatInt(text.Offset, 10))
	buf.WriteString("],")
	buf.WriteString(text.Src.Asm())
	return buf.String()
}

func (text *SrcRegDestRegOffset) GetVRegUseInfo() VRegUseInfo {
	return retVRegUseInfo(text.Src, text.Dest)
}

type SrcNumDestRegOffset struct {
	Src    string
	Dest   RegEnum
	Offset int64
	Kind   TextKindEnum
}

func NewSrcNumDestRegOffset(Dest RegEnum, Src string, offset int64, Kind TextKindEnum) *SrcNumDestRegOffset {
	return &SrcNumDestRegOffset{
		Src:    Src,
		Dest:   Dest,
		Kind:   Kind,
		Offset: offset,
	}
}

func (text *SrcNumDestRegOffset) Type() TextTypeEnum {
	return TypeSrcNumDestRegOffset
}

func (text *SrcNumDestRegOffset) String() string {
	var buf strings.Builder
	if text == nil {
		buf.WriteString("&amd64.SrcNumDestRegOffset{ <nil> }")
	} else {
		buf.WriteString("&amd64.SrcNumDestRegOffset")
		buf.WriteString(fmt.Sprintf("%+v", *text))
	}
	return buf.String()
}

func (text *SrcNumDestRegOffset) Asm() string {
	var buf strings.Builder
	buf.WriteString(TextKindEnumStrMap[text.Kind])
	buf.WriteString(" qword [")
	buf.WriteString(text.Dest.Asm())
	if text.Offset >= 0 {
		buf.WriteString("+")
	}
	buf.WriteString(strconv.FormatInt(text.Offset, 10))
	buf.WriteString("], ")
	buf.WriteString(text.Src)
	return buf.String()
}

func (text *SrcNumDestRegOffset) GetVRegUseInfo() VRegUseInfo {
	return retVRegUseInfo(text.Dest)
}

type SrcRegMemDestReg struct {
	Src  RegEnum
	Dest RegEnum
	Kind TextKindEnum
}

func NewSrcRegMemDestReg(Dest, Src RegEnum, Kind TextKindEnum) *SrcRegMemDestReg {
	return &SrcRegMemDestReg{
		Src:  Src,
		Dest: Dest,
		Kind: Kind,
	}
}

func (text *SrcRegMemDestReg) Type() TextTypeEnum {
	return TypeSrcRegMemDestReg
}

func (text *SrcRegMemDestReg) String() string {
	var buf strings.Builder
	if text == nil {
		buf.WriteString("&amd64.SrcRegMemDestReg{ <nil> }")
	} else {
		buf.WriteString("&amd64.SrcRegMemDestReg")
		buf.WriteString(fmt.Sprintf("%+v", *text))
	}
	return buf.String()
}

func (text *SrcRegMemDestReg) Asm() string {
	var buf strings.Builder
	buf.WriteString(TextKindEnumStrMap[text.Kind])
	buf.WriteString(" ")
	buf.WriteString(text.Dest.Asm())
	buf.WriteString(",[")
	buf.WriteString(text.Src.Asm())
	buf.WriteString("]")
	return buf.String()
}

func (text *SrcRegMemDestReg) GetVRegUseInfo() VRegUseInfo {
	return retVRegUseInfo(text.Src, text.Dest)
}

type Label struct {
	Name string
}

func NewLabel(Name string) *Label {
	return &Label{
		Name: Name,
	}
}

func (text *Label) Type() TextTypeEnum {
	return Labels
}

func (text *Label) String() string {
	var buf strings.Builder
	if text == nil {
		buf.WriteString("&amd64.Label{ <nil> }")
	} else {
		buf.WriteString("&amd64.Label")
		buf.WriteString(fmt.Sprintf("%+v", *text))
	}
	return buf.String()
}

func (text *Label) Asm() string {
	var buf strings.Builder
	buf.WriteString(text.Name)
	buf.WriteString(": \n")
	return buf.String()
}

func (text *Label) GetVRegUseInfo() VRegUseInfo {
	return retVRegUseInfo()
}

type SrcNumDestMem struct {
	Mem  string
	Num  string
	Kind TextKindEnum
}

func NewSrcNumDestMem(dest string, src string, Kind TextKindEnum) *SrcNumDestMem {
	return &SrcNumDestMem{
		Mem:  dest,
		Num:  src,
		Kind: Kind,
	}
}

func (text *SrcNumDestMem) Type() TextTypeEnum {
	return TypeSrcNumDestMem
}

func (text *SrcNumDestMem) String() string {
	var buf strings.Builder
	if text == nil {
		buf.WriteString("&amd64.SrcNumDestMem{ <nil> }")
	} else {
		buf.WriteString("&amd64.SrcNumDestMem")
		buf.WriteString(fmt.Sprintf("%+v", *text))
	}
	return buf.String()
}

func (text *SrcNumDestMem) Asm() string {
	var buf strings.Builder
	buf.WriteString(TextKindEnumStrMap[text.Kind])
	buf.WriteString(" [")
	buf.WriteString(text.Mem)
	buf.WriteString("], ")
	buf.WriteString(text.Num)
	return buf.String()
}

func (text *SrcNumDestMem) GetVRegUseInfo() VRegUseInfo {
	return VRegUseInfo{}
}
