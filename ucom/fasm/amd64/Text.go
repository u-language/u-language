package amd64

import (
	"gitee.com/u-language/u-language/ucom/ast"
)

// 设置栈中变量
func StackSetVar(to *AstToAsm, offset int64, name string, value string, sbt *ast.Sbt, vreg *VReg) TextNode {
	var ret = make([]TextNode, 0, 2)
	info := sbt.Have(name)
	switch info.Type() {
	case "int":
		ret = append(ret, NewSrcNumDestRegOffset(RBP, value, -offset, MOV))
	case "float":
		data := NewAutoData(value, 4)
		to.Datas.Add(data)
		xmm1 := vreg.GetSSEReg()
		ret = append(ret, NewSrcMemDestReg(xmm1, data.Name, MOVSS))
		ret = append(ret, NewSrcRegDestRegOffset(RBP, xmm1, -offset, MOVSS))
	}
	return NewStackVarMovKind(ret)
}

func SetVar(to *AstToAsm, name, value string, sbt *ast.Sbt, vreg *VReg) TextNode {
	var ret = make([]TextNode, 0, 2)
	info := sbt.Have(name)
	switch info.Type() {
	case "int":
		ret = append(ret, NewSrcNumDestMem(name, value, MOV))
	case "float":
		data := NewAutoData(value, 4)
		to.Datas.Add(data)
		xmm1 := vreg.GetSSEReg()
		ret = append(ret, NewSrcMemDestReg(xmm1, data.Name, MOVSS))
		ret = append(ret, NewSrcRegDestMem(name, xmm1, MOVSS))
	}
	return NewVarMovKind(ret)
}
