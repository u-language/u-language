package amd64

import (
	"strings"
	"unsafe"
)

// vrmToRegEnum将 内存布局由，虚拟寄存器（vrm），转换为 RegEnum
func vrmToRegEnum(v vrm) RegEnum {
	return *(*RegEnum)(unsafe.Pointer(&v))
}

// _RegEnumTovrm将 内存布局由，RegEnum，转换为 虚拟寄存器（vrm）
func _RegEnumTovrm(reg RegEnum) vrm {
	return *(*vrm)(unsafe.Pointer(&reg))
}

// vrmToString将虚拟寄存器（vrm），转换为字符串
func vrmToString(reg vrm) string {
	var buf strings.Builder
	up := (unsafe.Pointer)(&reg)
	sp := *(*[8]byte)(up)
	for i := 0; i < 8; i++ {
		buf.WriteByte(sp[i])
	}
	return buf.String()
}
