package ast2

import (
	"testing"

	"gitee.com/u-language/u-language/ucom/enum"
)

func Test_sum(t *testing.T) {
	type args struct {
		expr Expr
		sbt  *Sbt
	}
	tests := []struct {
		name  string
		args  args
		want  string
		want1 bool
	}{
		{"1", args{NewObject(INTOBJ, "1"), nil}, "1", true},
		{"1+1", args{NewOpExpr(enum.ADDOP, NewObject(INTOBJ, "1"), NewObject(INTOBJ, "1"), 0), nil}, "2", true},
		{"a=b,b=0", args{NewObject(SymbolObj, "a"), makeTestSbt()}, "0", true},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, got1 := sum(tt.args.expr, tt.args.sbt)
			if got != tt.want {
				t.Errorf("sum() got = %v, want %v", got, tt.want)
			}
			if got1 != tt.want1 {
				t.Errorf("sum() got1 = %v, want %v", got1, tt.want1)
			}
		})
	}
}

func makeTestSbt() *Sbt {
	s := NewSbt(false)
	//创建这种代码对于的测试节点
	// const a int = b
	// const b int = 0
	a := newConstNode("a", 0)
	a.Value = NewObject(SymbolObj, "b")
	s.add("a", a)
	b := newConstNode("b", 0)
	b.Value = NewObject(INTOBJ, "0")
	s.add("b", b)
	return s
}
