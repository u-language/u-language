package ast2

import (
	"strings"
	"testing"
)

func Test_handleNil(t *testing.T) {
	var n *VarNode
	var buf strings.Builder
	handleNil(&buf, n)
	if buf.String() != "*ast2.VarNode{<nil>}" {
		t.Fatal("handleNil output", buf.String())
	}
}
