package ast2_test

import (
	"context"
	"fmt"
	"sync"
	"testing"
	"time"

	. "gitee.com/u-language/u-language/ucom/ast2"
	"gitee.com/u-language/u-language/ucom/errcode"
	"gitee.com/u-language/u-language/ucom/internal/parser"
	"gitee.com/u-language/u-language/ucom/internal/utils"
)

var run sync.WaitGroup

func TestMain(m *testing.M) {
	errcode.Debug = true
	NdSotfOn = true
	//这是为了调试时运行时不报告所有goroutine都睡着了
	go func() { select {} }()
	m.Run()
	run.Wait()
	time.Sleep(10 * time.Microsecond)
}

func TestCheckSyntaxErr(t *testing.T) {
	type args struct {
		str    []string
		errctx *errcode.ErrCtx
		cancel context.CancelFunc
		noLF   bool
	}
	newargs := func(str []string, errf func(errcode.ErrInfo), num int, name string) args {
		actx := errcode.NewErrCtx()
		astdctx, acancel := context.WithCancel(context.Background())
		i := 0
		s := make(chan struct{})
		run.Add(1)
		go actx.Handle(astdctx, func(info errcode.ErrInfo) { //每收到一个错误，i++
			i++
			errf(info)
		}, func() { s <- struct{}{} })
		go func() {
			defer run.Done()
			<-s           //等待处理所有错误完毕
			if num != i { //如果预期发生错误数量不等于实际发生错误数量
				panic(fmt.Errorf("%s 应该有%d个错误，实际有%d个错误", name, num, i))
			}
		}()
		ret := args{str: str, errctx: actx, cancel: acancel}
		return ret
	}
	a1 := newargs([]string{""}, func(err errcode.ErrInfo) {
		if !err.IsErr("1.txt", 1, nil, errcode.PackageDeclMustFirstLine) {
			panic("a1\n" + err.String())
		}
	}, 1, "a1")
	a2 := newargs([]string{"package"}, func(err errcode.ErrInfo) {
		if !err.IsErr("1.txt", 1, errcode.NewMsgExpectedAndFound("package name", "文件结束")) {
			panic("a2\n" + err.String())
		}
	}, 1, "a2")
	a3 := newargs([]string{"package unsafe"}, func(err errcode.ErrInfo) {
		if !err.IsErr("1.txt", 1, nil, errcode.PackageNameCannotBeEqualUnsafe) {
			panic("a3\n" + err.String())
		}
	}, 1, "a3")
	a4 := newargs([]string{"package main a"}, func(err errcode.ErrInfo) {
		if !err.IsErr("1.txt", 1, errcode.NewMsgExpectedAndFound("换行", "a")) {
			panic("a4\n" + err.String())
		}
	}, 1, "a4")
	a4.noLF = true
	a5 := newargs([]string{"package main", "var "}, func(err errcode.ErrInfo) {
		if !err.IsErr("1.txt", 2, errcode.NewMsgExpectedAndFound("变量名", "文件结束"), errcode.VARErr) {
			panic("a5\n" + err.String())
		}
	}, 1, "a5")
	a6 := newargs([]string{"package main", "var a 1"}, func(err errcode.ErrInfo) {
		if !err.IsErr("1.txt", 2, errcode.NewMsgUnexpected("1"), errcode.VARErr) {
			panic("a6\n" + err.String())
		}
	}, 1, "a6")
	a7 := newargs([]string{"package main", "var a int = "}, func(err errcode.ErrInfo) {
		if !err.IsErr("1.txt", 2, errcode.NewMsgUnexpected("文件结束"), errcode.VARErr) {
			panic("a7\n" + err.String())
		}
	}, 1, "a7")
	a8 := newargs([]string{"package main", "func a(j int,p){"}, func(err errcode.ErrInfo) {
		if !err.IsErr("1.txt", 2, errcode.NewMsgUnexpected(")"), errcode.FUNCErr) {
			panic("a8\n" + err.String())
		}
	}, 1, "a8")
	a9 := newargs([]string{"package main", "func (){"}, func(err errcode.ErrInfo) {
		if !err.IsErr("1.txt", 2, errcode.NewMsgExpectedAndFound("func name", "("), errcode.FUNCErr) {
			panic("a9\n" + err.String())
		}
	}, 1, "a9")
	a10 := newargs([]string{"package main", "func a){"}, func(err errcode.ErrInfo) {
		if !err.IsErr("1.txt", 2, errcode.NewMsgExpectedAndFound("(", ")"), errcode.FUNCErr) {
			panic("a10\n" + err.String())
		}
	}, 1, "a10")
	a11 := newargs([]string{"package main", "func a()"}, func(err errcode.ErrInfo) {
		if !err.IsErr("1.txt", 2, errcode.NewMsgUnexpected("文件结束"), errcode.FUNCErr) {
			panic("a11\n" + err.String())
		}
	}, 1, "a11")
	a12 := newargs([]string{"package main", "func a()int"}, func(err errcode.ErrInfo) {
		if !err.IsErr("1.txt", 2, errcode.NewMsgExpectedAndFound("{", "文件结束"), errcode.FUNCErr) {
			panic("a12\n" + err.String())
		}
	}, 1, "a12")
	a13 := newargs([]string{"package main", "var a int 1"}, func(err errcode.ErrInfo) {
		if !err.IsErr("1.txt", 2, errcode.NewMsgUnexpected("1"), errcode.VARErr) {
			panic("a13\n" + err.String())
		}
	}, 1, "a13")
	a14 := newargs([]string{"package main", "package"}, func(err errcode.ErrInfo) {
		if !err.IsErr("1.txt", 2, nil, errcode.PackageDeclMustFirstLine) {
			panic("a14\n" + err.String())
		}
	}, 1, "a14")
	a15 := newargs([]string{"package main", "func a(1){"}, func(err errcode.ErrInfo) {
		if !err.IsErr("1.txt", 2, errcode.NewMsgUnexpected("1"), errcode.FUNCErr) {
			panic("a15\n" + err.String())
		}
	}, 1, "a15")
	a16 := newargs([]string{"package main", "func a(,,){"}, func(err errcode.ErrInfo) {
		if !err.IsErr("1.txt", 2, errcode.NewMsgUnexpected(","), errcode.FUNCErr) {
			panic("a16\n" + err.String())
		}
	}, 1, "a16")
	a17 := newargs([]string{"package main", "a"}, func(err errcode.ErrInfo) {
		if !err.IsErr("1.txt", 2, errcode.NewMsgUnexpected("a")) {
			panic("a17\n" + err.String())
		}
	}, 1, "a17")
	a18 := newargs([]string{"package main\n", "a++"}, func(err errcode.ErrInfo) {
		panic("a18\n" + err.String())
	}, 0, "a18")
	a18.noLF = true
	a19 := newargs([]string{"package main", "a--a"}, func(err errcode.ErrInfo) {
		if !err.IsErr("1.txt", 2, errcode.NewMsgUnexpected("a")) {
			panic("a19\n" + err.String())
		}
	}, 1, "a19")
	a20 := newargs([]string{"package main", "if q==9"}, func(err errcode.ErrInfo) {
		if !err.IsErr("1.txt", 2, errcode.NewMsgUnexpected("文件结束")) {
			panic("a20\n" + err.String())
		}
	}, 1, "a20")
	a21 := newargs([]string{"package main", "if q=9{", "}"}, func(err errcode.ErrInfo) {
		if !err.IsErr("1.txt", 2, nil, errcode.IfErr, errcode.AFIE) {
			panic("a21\n" + err.String())
		}
	}, 1, "a21")
	a22 := newargs([]string{"package main", "if p{", "}", "else q{", "}"}, func(err errcode.ErrInfo) {
		if !err.IsErr("1.txt", 4, errcode.NewMsgUnexpected("q")) {
			panic("a22\n" + err.String())
		}
	}, 1, "a22")
	a23 := newargs([]string{"package main", "if p{", "}", "else if {", "}"}, func(err errcode.ErrInfo) {
		if !err.IsErr("1.txt", 4, nil, errcode.ElseErr, errcode.NoBoolExpr) {
			panic("a23\n" + err.String())
		}
	}, 1, "a23")
	a24 := newargs([]string{"package main", "a("}, func(err errcode.ErrInfo) {
		if !err.IsErr("1.txt", 2, errcode.NewMsgUnexpected("文件结束"), errcode.CallErr) {
			panic("a24\n" + err.String())
		}
	}, 1, "a24")
	a25 := newargs([]string{"package main", "a(,)"}, func(err errcode.ErrInfo) {
		if !err.IsErr("1.txt", 2, errcode.NewMsgUnexpected(","), errcode.CallErr) {
			panic("a25\n" + err.String())
		}
	}, 1, "a25")
	a26 := newargs([]string{"package main", "a(b,,c)"}, func(err errcode.ErrInfo) {
		if !err.IsErr("1.txt", 2, errcode.NewMsgUnexpected(","), errcode.CallErr) {
			panic("a26\n" + err.String())
		}
	}, 1, "a26")
	a27 := newargs([]string{"package main", "a(b+)"}, func(err errcode.ErrInfo) {
		if !err.IsErr("1.txt", 2, errcode.NewMsgUnexpected("+"), errcode.CallErr) {
			panic("a27\n" + err.String())
		}
	}, 1, "a27")
	a28 := newargs([]string{"package main", "for ;;i++1{"}, func(err errcode.ErrInfo) {
		if !err.IsErr("1.txt", 2, errcode.NewMsgUnexpected("1")) {
			panic("a28\n" + err.String())
		}
	}, 1, "a28")
	a29 := newargs([]string{"package main", "for ;i++;{"}, func(err errcode.ErrInfo) {
		if !err.IsErr("1.txt", 2, errcode.NewMsgUnexpected("++"), errcode.ForErr) {
			panic("a29\n" + err.String())
		}
	}, 1, "a29")
	a30 := newargs([]string{"package main", "for var i int =9;;i++{", "}"}, func(err errcode.ErrInfo) {
		panic("a30\n" + err.String())
	}, 0, "a30")
	a31 := newargs([]string{"package main", "for ;;@"}, func(err errcode.ErrInfo) {
		if !err.IsErr("1.txt", 2, errcode.NewMsgUnexpected("文件结束"), errcode.ForErr) {
			panic("a31\n" + err.String())
		}
	}, 1, "a31")
	a32 := newargs([]string{"package main", "&"}, func(err errcode.ErrInfo) {
		if !err.IsErr("1.txt", 2, errcode.NewMsgUnexpected("&")) {
			panic("a32\n" + err.String())
		}
	}, 1, "a32")
	a33 := newargs([]string{"package main", "var a int = &"}, func(err errcode.ErrInfo) {
		if !err.IsErr("1.txt", 2, errcode.NewMsgUnexpected("文件结束"), errcode.VARErr) {
			panic("a33\n" + err.String())
		}
	}, 1, "a33")
	a34 := newargs([]string{"package main", "struct 1"}, func(err errcode.ErrInfo) {
		if !err.IsErr("1.txt", 2, errcode.NewMsgExpectedAndFound("结构体名", "1"), errcode.StructErr) {
			panic("a34\n" + err.String())
		}
	}, 1, "a34")
	a35 := newargs([]string{"package main", "struct p {", ""}, func(err errcode.ErrInfo) {
		if !err.IsErr("1.txt", 3, errcode.NewMsgExpectedAndFound("结构体字段名或}", "文件结束"), errcode.StructErr) {
			panic("a35\n" + err.String())
		}
	}, 1, "a35")
	a36 := newargs([]string{"package main", "struct p {", "a int"}, func(err errcode.ErrInfo) {
		if !err.IsErr("1.txt", 3, errcode.NewMsgUnexpected("文件结束"), errcode.StructErr) {
			panic("a36\n" + err.String())
		}
	}, 1, "a36")
	a37 := newargs([]string{"package main", "var a int =p.", "a"}, func(err errcode.ErrInfo) {
		if !err.IsErr("1.txt", 2, errcode.NewMsgUnexpected("换行"), errcode.VARErr) {
			panic("a37\n" + err.String())
		}
	}, 1, "a37")
	a38 := newargs([]string{"package main", "var a int =&p.", "a"}, func(err errcode.ErrInfo) {
		if !err.IsErr("1.txt", 2, errcode.NewMsgUnexpected("换行"), errcode.VARErr) {
			panic("a38\n" + err.String())
		}
	}, 1, "a38")
	a39 := newargs([]string{"package main", "@a@a@a", ""}, func(err errcode.ErrInfo) {
		if !err.IsErr("1.txt", 2, errcode.NewMsgUnexpected("@")) {
			panic("a39\n" + err.String())
		}
	}, 1, "a39")
	a40 := newargs([]string{"package main", "@a.@a.@a=@a.@a.@a", ""}, func(err errcode.ErrInfo) {
		if !err.IsErr("1.txt", 2, errcode.NewMsgUnexpected("@")) {
			panic("a40\n" + err.String())
		}
	}, 1, "a40")
	a41 := newargs([]string{"package main", `printf("%s","!=3,2,1,0")`}, func(err errcode.ErrInfo) {
		panic("a41\n" + err.String())
	}, 0, "a41")
	a42 := newargs([]string{"package main", `a()`}, func(err errcode.ErrInfo) {
		panic("a42\n" + err.String())
	}, 0, "a42")
	a43 := newargs([]string{"package main", `a(b+-*)`}, func(err errcode.ErrInfo) {
		if !err.IsErr("1.txt", 2, errcode.NewMsgUnexpected("-"), errcode.CallErr) {
			panic("a43\n" + err.String())
		}
	}, 1, "a43")
	a44 := newargs([]string{"package main", `a(a b c)`}, func(err errcode.ErrInfo) {
		if !err.IsErr("1.txt", 2, errcode.NewMsgUnexpected("b"), errcode.CallErr) {
			panic("a44\n" + err.String())
		}
	}, 1, "a44")
	a45 := newargs([]string{"package main", `a.f()`}, func(err errcode.ErrInfo) {
		panic("a45\n" + err.String())
	}, 0, "a45")
	a46 := newargs([]string{"package main", `if(1 + 2 )* 3 == 9{`}, func(err errcode.ErrInfo) {
		if !err.IsErr("1.txt", 2, errcode.NewMsgUnexpected("文件结束")) {
			panic("a44\n" + err.String())
		}
	}, 1, "a46")
	a47 := newargs([]string{"package main", `p=a.b[0]`}, func(err errcode.ErrInfo) {
		panic("a47\n" + err.String())
	}, 0, "a47")
	a48 := newargs([]string{"package main", `p.k=a.b[0]`}, func(err errcode.ErrInfo) {
		panic("a48\n" + err.String())
	}, 0, "a48")
	a49 := newargs([]string{"package main", `a=p.f()`}, func(err errcode.ErrInfo) {
		panic("a49\n" + err.String())
	}, 0, "a49")
	a50 := newargs([]string{"package main", `a.b[0]=a.b[0]`}, func(err errcode.ErrInfo) {
		panic("a50\n" + err.String())
	}, 0, "a50")
	a51 := newargs([]string{"package main", `a=]`}, func(err errcode.ErrInfo) {
		if !err.IsErr("1.txt", 2, errcode.NewMsgUnexpected("]"), errcode.ASSIGNErr) {
			panic("a51\n" + err.String())
		}
	}, 1, "a51")
	a52 := newargs([]string{"package main", `a=func`}, func(err errcode.ErrInfo) {
		if !err.IsErr("1.txt", 2, errcode.NewMsgUnexpected("func"), errcode.ASSIGNErr) {
			panic("a52\n" + err.String())
		}
	}, 1, "a52")
	a53 := newargs([]string{"package main", `a=[`}, func(err errcode.ErrInfo) {
		if !err.IsErr("1.txt", 2, errcode.NewMsgUnexpected("["), errcode.ASSIGNErr) {
			panic("a53\n" + err.String())
		}
	}, 1, "a53")
	a54 := newargs([]string{"package main", `a=1+9)`}, func(err errcode.ErrInfo) {
		if !err.IsErr("1.txt", 2, errcode.NewMsgUnexpected(")"), errcode.ASSIGNErr) {
			panic("a54\n" + err.String())
		}
	}, 1, "a54")
	a55 := newargs([]string{"package main", `a=@(p.p)`}, func(err errcode.ErrInfo) {
		panic("a55\n" + err.String())
	}, 0, "a55")
	a56 := newargs([]string{"package main", `a=@(p.p)+@(p.p)`}, func(err errcode.ErrInfo) {
		panic("a56\n" + err.String())
	}, 0, "a56")
	a57 := newargs([]string{"package main", `a=[`}, func(err errcode.ErrInfo) {
		if !err.IsErr("1.txt", 2, errcode.NewMsgUnexpected("["), errcode.ASSIGNErr) {
			panic("a57\n" + err.String())
		}
	}, 1, "a57")
	a58 := newargs([]string{"package main", `else`}, func(err errcode.ErrInfo) {
		if !err.IsErr("1.txt", 2, errcode.NewMsgUnexpected("else")) {
			panic("a58\n" + err.String())
		}
	}, 1, "a58")
	a59 := newargs([]string{"package main", "if p{", "}else{", "}"}, func(err errcode.ErrInfo) {
		panic("a59\n" + err.String())
	}, 0, "a59")
	// a60 := newargs([]string{"package main", "if p{", "var a int = 9", "var b [a]int", "}"}, func(err errcode.ErrInfo) {
	// 	if !err.IsErr("1.txt", 4, nil, errcode.ArrayLenNotCompileTimeConstExpr) {
	// 		panic("a60\n" + err.String())
	// 	}
	// }, 1, "a60")
	// a61 := newargs([]string{"package main", "if p{", "const b int = 1", "const a [b]int", "}"}, func(err errcode.ErrInfo) {
	// 	panic("a61\n" + err.String())
	// }, 0, "a61")
	// a62 := newargs([]string{"package main", "if p{", "const a [b]int", "const b int = 1", "}"}, func(err errcode.ErrInfo) {
	// 	panic("a62\n" + err.String())
	// }, 0, "a62")
	// a63 := newargs([]string{"package main", "if p{", "if p{", "const a [b]int", "const b int = 1", "}", "}"}, func(err errcode.ErrInfo) {
	// }, 0, "a63")
	a64 := newargs([]string{"package main", "if p{", "const a [b]int", "const b int = 1", "}", "}"}, func(err errcode.ErrInfo) {
		if !err.IsErr("1.txt", 6, errcode.NewMsgUnexpected("}")) {
			panic("a64\n" + err.String())
		}
	}, 1, "a64")
	a65 := newargs([]string{"package main", "case 1:", "default:"}, func(err errcode.ErrInfo) {
		if !err.IsErr("1.txt", 2, nil, errcode.CaseErr, errcode.CaseStmtInSwitch) && !err.IsErr("1.txt", 3, nil, errcode.DefaultErr, errcode.DefaultStmtInSwitch) &&
			!err.IsErr("1.txt", 2, nil, errcode.NdSotf) && !err.IsErr("1.txt", 3, nil, errcode.NdSotf) {
			panic("a65\n" + err.String())
		}
	}, 4, "a65")
	a66 := newargs([]string{"package main", `continue`}, func(err errcode.ErrInfo) {
		if !err.IsErr("1.txt", 2, nil, errcode.ContinueStmtInFor) && !err.IsErr("1.txt", 2, nil, errcode.NdSotf) && !err.IsErr("1.txt", 2, errcode.NewMsgUnexpected("文件结束")) {
			panic("a66\n" + err.String())
		}
	}, 3, "a66")
	a67 := newargs([]string{"package main", "func main(){", "for ;;{", "autofree 0{", "}", "if p{", "}", `continue`, "}", "continue", "}"}, func(err errcode.ErrInfo) {
		if !err.IsErr("1.txt", 10, nil, errcode.ContinueStmtInFor) {
			panic("a67\n" + err.String())
		}
	}, 1, "a67")
	a68 := newargs([]string{"package main", "func main(){", "for ;;{", "}"}, func(err errcode.ErrInfo) {
		if !err.IsErr("1.txt", 4, errcode.NewMsgUnexpected("文件结束")) {
			panic("a68\n" + err.String())
		}
	}, 1, "a68")
	a69 := newargs([]string{"package main", `break`}, func(err errcode.ErrInfo) {
		if !err.IsErr("1.txt", 2, nil, errcode.BreakStmtInForOrSwitch) && !err.IsErr("1.txt", 2, nil, errcode.NdSotf) && !err.IsErr("1.txt", 2, errcode.NewMsgUnexpected("文件结束")) {
			panic("a69\n" + err.String())
		}
	}, 3, "a69")
	a70 := newargs([]string{"package main", "func main(){", "for ;;{", "for;;{", "}", "continue", "}", "}"}, func(err errcode.ErrInfo) {
		panic("a70\n" + err.String())
	}, 0, "a70")
	tests := []struct {
		name string
		args args
	}{
		{"包声明不在第一行", a1},
		{"包声明没有包名", a2},
		{"包名不能为unsafe", a3},
		{"包名后没有换行", a4},
		{"变量没有名字", a5},
		{"变量没有类型", a6},
		{"变量初始化没有值", a7},
		{"函数参数未声明类型", a8},
		{"函数声明没有函数名", a9},
		{"函数声明没有左小括号", a10},
		{"无返回值函数声明没有左大括号", a11},
		{"有返回值函数声明没有左大括号", a12},
		{"变量无赋值却有不是 var name type 中任意一个的", a13},
		{"包声明出现在第二行", a14},
		{"函数括号内出现非逗号或参数声明", a15},
		{"函数声明逗号过多", a16},
		{"未知的语句", a17},
		{"自增语句未跟随换行", a18},
		{"自增语句跟随标识符", a19},
		{"if意外的结束", a20},
		{"if布尔表达式的赋值", a21},
		{"else意外的表达式", a22},
		{"else if没有表达式", a23},
		{"函数调用没有右大括号", a24},
		{"函数调用没参数直接逗号", a25},
		{"函数调用多个逗号相邻", a26},
		{"函数调用意外的加号", a27},
		{"for结束语句意外的1", a28},
		{"for布尔表达式为自操作语句", a29},
		{"for初始化语句变量声明带有赋值", a30},
		{"for结束语句只有一个@", a31},
		{"var &", a32},
		{"变量初始化只有&", a33},
		{"结构体名是1", a34},
		{"结构体名没有结束（跟着文件结束）", a35},
		{"结构体名没有结束（跟着一个字段）", a36},
		{"变量初始化选择器没有右值", a37},
		{"变量初始化取地址选择器没有右值", a38},
		{"@a@a@a", a39},
		{"@a.@a.@a=@a.@a.@", a40},
		{"函数调用有两个参数，第一个是object", a41},
		{"无参数的函数调用", a42},
		{"a(b+-*)", a43},
		{"a(a b c)", a44},
		{"a.f()", a45},
		{"if语句有括号的表达式", a46},
		{"p=a.b[0]", a47},
		{"p.k=a.b[0]", a48},
		{"a=p.f()", a49},
		{"a.b[0]=a.b[0]", a50},
		{"a=]", a51},
		{"a=func", a52},
		{"a=[", a53},
		{"a=1+9)", a54},
		{"a=@(p.p)", a55},
		{"a=@(p.p)+@(p.p)", a56},
		{"a=[", a57},
		{"没有if的else", a58},
		{"if代码块后不换行跟else代码块", a59},
		// {"数组长度不是编译器常量表达式", a60},
		// {"测试计算编译器常量表达式，能否获取局部常量信息", a61},
		// {"测试计算编译器常量表达式，能否获取在数组声明后的局部常量信息", a62},
		// {"测试计算编译器常量表达式，能否获取在数组声明后的更深局部常量信息", a63},
		{"两个}，一个if", a64},
		{"case和default不在switch", a65},
		{"continue不在for", a66},
		{"测试是否错误的报告continue不在for内", a67},
		{"缺少一个}", a68},
		{"break不在for", a69},
		{"在for内嵌套for后跟continue", a70},
	}
	for _, ttf := range tests {
		tt := ttf
		t.Run(tt.name, func(t *testing.T) {
			parser.ParserStr2(tt.args.str, parser.WithErrCtx(tt.args.errctx), parser.WithnoLF(tt.args.noLF))
			tt.args.cancel()
		})
	}
}

func init() {
	utils.Debug = true
	errcode.Debug = true
	errcode.Debug2 = true
	go errcode.Handle(context.Background(), func(err errcode.ErrInfo) { panic(err.String()) }, func() {})
}
