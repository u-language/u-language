package ast2

import "testing"

func Test_stackData_Push(t *testing.T) {
	var s stackData
	s.Push()
	s.Push()
	if !s.Get() {
		t.Fatal("got false")
	}
	if !s.Get() {
		t.Fatal("got false")
	}
	s.Pop()
	s.Pop()
	if s.Get() {
		t.Fatal("got true")
	}
}
