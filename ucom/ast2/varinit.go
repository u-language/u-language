package ast2

import (
	"sort"
	"sync"

	"gitee.com/u-language/u-language/ucom/data"
)

var _ sort.Interface = (*VarInitTable)(nil)

// VarInitTable 变量初始化表
// 实现了 [sort.Interface] 接口
type VarInitTable struct {
	data.Slice[*VarNode]
	once sync.Once
}

// Init 初始化变量初始化表
func (t *VarInitTable) Init(Thread bool) {
	t.Thread = Thread
}

func (t *VarInitTable) Len() int {
	return len(t.Data)
}

func (t *VarInitTable) Less(i, j int) bool {
	return t.Data[i].LineNum < t.Data[j].LineNum
}

func (t *VarInitTable) Swap(i, j int) {
	t.Data[i], t.Data[j] = t.Data[j], t.Data[i]
}

// InitOrder 返回按行升序排列的变量初始化信息
func (t *VarInitTable) InitOrder() []*VarNode {
	t.once.Do(func() {
		sort.Sort(t)
	})
	return t.Data
}
