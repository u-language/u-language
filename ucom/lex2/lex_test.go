package lex2_test

import (
	"context"
	"strconv"
	"strings"
	"testing"

	"gitee.com/u-language/u-language/ucom/errcode"
	. "gitee.com/u-language/u-language/ucom/lex2"
)

const n = 600

func BenchmarkLexP1(b *testing.B) {
	type args struct {
		file   string
		value  string
		errctx *errcode.ErrCtx
	}
	b.StopTimer()
	a2 := args{file: "1.txt", value: reta2str(), errctx: errcode.DefaultErrCtx}
	b.SetBytes(1)
	b.ReportAllocs()
	b.StartTimer()
	for i := 0; i < b.N; i++ {
		f := NewFileToken(a2.file, strings.NewReader(a2.value), errcode.DefaultErrCtx, false)
		for tk := f.Next(); tk.TYPE != EOF; tk = f.Next() {

		}
	}
}

func reta2str() string {
	var buf strings.Builder
	buf.Grow(18 * n)
	for i := 0; i < n; i++ {
		buf.WriteString("var s")
		buf.WriteString(strconv.Itoa(i))
		buf.WriteString(" float=2.8")
		buf.WriteString("\n")
	}
	return buf.String()
}

func init() {
	go errcode.Handle(context.Background(), func(err errcode.ErrInfo) { panic(err.String()) }, func() {})
}
