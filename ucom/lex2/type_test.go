package lex2

import (
	"context"
	"fmt"
	"reflect"
	"strings"
	"testing"

	"gitee.com/u-language/u-language/ucom/errcode"
)

func TestFileToken(t *testing.T) {
	tests := []struct {
		name string
		l    *FileToken
		want []Token
	}{
		{"+", newFileToken("+"), []Token{{TYPE: ADD, Value: "+"}}},
		{"-", newFileToken("-"), []Token{{TYPE: SUB, Value: "-"}}},
		{"*", newFileToken("*"), []Token{{TYPE: MUL, Value: "*"}}},
		{"/", newFileToken("/"), []Token{{TYPE: DIV, Value: "/"}}},
		{"(", newFileToken("("), []Token{{TYPE: LPAREN, Value: "("}}},
		{")", newFileToken(")"), []Token{{TYPE: RPAREN, Value: ")"}}},
		{"{", newFileToken("{"), []Token{{TYPE: LBRACE, Value: "{"}}},
		{"{", newFileToken("}"), []Token{{TYPE: RBRACE, Value: "}"}}},
		{"[", newFileToken("["), []Token{{TYPE: LBRACK, Value: "["}}},
		{"]", newFileToken("]"), []Token{{TYPE: RBRACK, Value: "]"}}},
		{"<", newFileToken("<"), []Token{{TYPE: Less, Value: "<"}}},
		{">", newFileToken(">"), []Token{{TYPE: Greater, Value: ">"}}},
		{"==", newFileToken("=="), []Token{{TYPE: Equal, Value: "=="}}},
		{"!=", newFileToken("!="), []Token{{TYPE: NoEqual, Value: "!="}}},
		{";", newFileToken(";"), []Token{{TYPE: SEMICOLON, Value: ";"}}},
		{"%", newFileToken("%"), []Token{{TYPE: Remain, Value: "%"}}},
		{":", newFileToken(":"), []Token{{TYPE: Colon, Value: ":"}}},
		{",", newFileToken(","), []Token{{TYPE: Comma, Value: ","}}},
		{"&", newFileToken("&"), []Token{{TYPE: LEA, Value: "&"}}},
		{"@", newFileToken("@"), []Token{{TYPE: Deref, Value: "@"}}},
		{"|", newFileToken("|"), []Token{{TYPE: Or, Value: "|"}}},
		{"||", newFileToken("||"), []Token{{TYPE: LogicOr, Value: "||"}}},
		{"&&", newFileToken("&&"), []Token{{TYPE: LogicAND, Value: "&&"}}},
		{"++", newFileToken("++"), []Token{{TYPE: Inc, Value: "++"}}},
		{"--", newFileToken("--"), []Token{{TYPE: Dec, Value: "--"}}},
		{"=", newFileToken("="), []Token{{TYPE: ASSIGN, Value: "="}}},
		{"^", newFileToken("^"), []Token{{TYPE: Xor, Value: "^"}}},
		{".", newFileToken("."), []Token{{TYPE: PERIOD, Value: "."}}},
		{"\r\n", newFileToken("\r\n"), []Token{{TYPE: NewLine, Value: "\n"}}},
		{"\n\n", newFileToken("\n\n"), []Token{{TYPE: NewLine, Value: "\n"}}},
		{"default", newFileToken("default:"), []Token{{TYPE: Default, Value: "default"}, {TYPE: Colon, Value: ":"}}},

		{"var", newFileToken("var"), []Token{{TYPE: VAR, Value: "var"}}},
		{"func", newFileToken("func"), []Token{{TYPE: FUNC, Value: "func"}}},
		{"if", newFileToken("if"), []Token{{TYPE: IF, Value: "if"}}},
		{"else", newFileToken("else"), []Token{{TYPE: ELSE, Value: "else"}}},
		{"for", newFileToken("for"), []Token{{TYPE: FOR, Value: "for"}}},
		{"break", newFileToken("break"), []Token{{TYPE: Break, Value: "break"}}},
		{"continue", newFileToken("continue"), []Token{{TYPE: Continue, Value: "continue"}}},
		{"struct", newFileToken("struct"), []Token{{TYPE: Struct, Value: "struct"}}},
		{"interface", newFileToken("interface"), []Token{{TYPE: Interface, Value: "interface"}}},
		{"return", newFileToken("return"), []Token{{TYPE: Return, Value: "return"}}},
		{"goto", newFileToken("goto"), []Token{{TYPE: GOTO, Value: "goto"}}},
		{"const", newFileToken("const"), []Token{{TYPE: Const, Value: "const"}}},
		{"nil", newFileToken("nil"), []Token{{TYPE: Nil, Value: "nil"}}},
		{"package", newFileToken("package"), []Token{{TYPE: Package, Value: "package"}}},
		{"switch", newFileToken("switch"), []Token{{TYPE: Switch, Value: "switch"}}},
		{"case", newFileToken("case"), []Token{{TYPE: Case, Value: "case"}}},
		{"default", newFileToken("default"), []Token{{TYPE: Default, Value: "default"}}},
		{"method", newFileToken("method"), []Token{{TYPE: Method, Value: "method"}}},
		{"autofree", newFileToken("autofree"), []Token{{TYPE: AutoFree, Value: "autofree"}}},
		{"and", newFileToken("and"), []Token{{TYPE: AND, Value: "and"}}},
		{"import", newFileToken("import"), []Token{{TYPE: Import, Value: "import"}}},
		{"enum", newFileToken("enum"), []Token{{TYPE: Enum, Value: "enum"}}},
		{"true", newFileToken("true"), []Token{{TYPE: TRUE, Value: "true"}}},
		{"false", newFileToken("false"), []Token{{TYPE: FALSE, Value: "false"}}},
		{"bool", newFileToken("bool"), []Token{{TYPE: NAME, Value: "bool"}}},

		{"\"s\"", newFileToken("\"s\""), []Token{{TYPE: String, Value: "\"s\""}}},
		{"1", newFileToken("1"), []Token{{TYPE: Int, Value: "1"}}},
		{"1.1", newFileToken("1.1"), []Token{{TYPE: FLOAT, Value: "1.1"}}},
		{"//1", newFileToken("//1", true), []Token{{TYPE: SinLineComments, Value: "//1"}}},

		{"A 0", newFileToken("A 0"), []Token{{TYPE: NAME, Value: "A"}, {TYPE: Int, Value: "0"}}},
		{"var b int = 9.0", newFileToken("var b int = 9.0"), []Token{{TYPE: VAR, Value: "var"}, {TYPE: NAME, Value: "b"}, {TYPE: NAME, Value: "int"}, {TYPE: ASSIGN, Value: "="}, {TYPE: FLOAT, Value: "9.0"}}},
		{"9.0+8.9", newFileToken("9.0+8.9"), []Token{{TYPE: FLOAT, Value: "9.0"}, {TYPE: ADD, Value: "+"}, {TYPE: FLOAT, Value: "8.9"}}},
		{"//9\\r\\n", newFileToken("//9\\r\\n"), []Token{}},
		{"/*s*/ enableComments", newFileToken("/*s*/", true), []Token{{TYPE: MLC, Value: "/*s*/"}}},
		{"/*s*/", newFileToken("/*s*/"), []Token{}},
		{"//s\n", newFileToken("//s\n"), []Token{{TYPE: NewLine, Value: "\n"}}},
		{"//s\n enableComments", newFileToken("//s\n", true), []Token{{TYPE: SinLineComments, Value: "//s\n"}}},
		{"package main\r\n", newFileToken("package main\r\n"), []Token{{TYPE: Package, Value: "package"}, {TYPE: NAME, Value: "main"}, {TYPE: NewLine, Value: "\n"}}},
		{"package main\r\n j = 1", newFileToken("package main\r\n j = 1"), []Token{{TYPE: Package, Value: "package"}, {TYPE: NAME, Value: "main"}, {TYPE: NewLine, Value: "\n"}, {TYPE: NAME, Value: "j"}, {TYPE: ASSIGN, Value: "="}, {TYPE: Int, Value: "1"}}},
		{"func main()", newFileToken("func main()"), []Token{{TYPE: FUNC, Value: "func"}, {TYPE: NAME, Value: "main"}, {TYPE: LPAREN, Value: "("}, {TYPE: RPAREN, Value: ")"}}},
		{"int,b", newFileToken("int,b"), []Token{{TYPE: NAME, Value: "int"}, {TYPE: Comma, Value: ","}, {TYPE: NAME, Value: "b"}}},
		{"/**/0/**/0", newFileToken("/**/0/**/0"), []Token{{TYPE: Int, Value: "0"}, {TYPE: Int, Value: "0"}}},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			var got = make([]Token, 0, len(tt.want))
			for tk := tt.l.Next(); tk.TYPE != EOF; tk = tt.l.Next() {
				got = append(got, tk)
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Line.NextToken() = %v, want %v", got, tt.want)
			}
		})
	}
}

func newFileToken(str string, enableComments ...bool) *FileToken {
	enableComments_local := false
	if len(enableComments) != 0 {
		enableComments_local = true
	}
	ret := NewFileToken("", strings.NewReader(str), errcode.DefaultErrCtx, enableComments_local)
	return &ret
}

func FuzzLine(f *testing.F) {
	errctx := errcode.NewErrCtx()
	go errctx.Handle(context.Background(), func(err errcode.ErrInfo) {}, func() {})
	f.Fuzz(func(t *testing.T, str string) {
		l := newFileToken(str)
		l.errctx = errctx
		for tk := l.Next(); tk.TYPE != EOF; tk = l.Next() {
		}
	})
}

func TestFileTokenHaveErr(t *testing.T) {
	type args struct {
		l      *FileToken
		cancel func()
	}
	newargs := func(str string, errf func(errcode.ErrInfo), num int, name string, enableComments ...bool) args {
		enableComments_local := false
		if len(enableComments) != 0 {
			enableComments_local = true
		}
		errctx := errcode.NewErrCtx()
		ctx, cancel := context.WithCancel(context.Background())
		i := 0
		s := make(chan struct{})
		go errctx.Handle(ctx, func(info errcode.ErrInfo) { //每收到一个错误，i++
			i++
			errf(info)
		}, func() { s <- struct{}{} })
		go func() {
			<-ctx.Done()
			if num != 0 { //如果预期发生错误数量大于0，等待处理所有错误完毕
				<-s
			}
			if num != i { //如果预期发生错误数量不等于实际发生错误数量
				panic(fmt.Errorf("%s 应该有%d个错误，实际有%d个错误", name, num, i))
			}
		}()
		ret := NewFileToken("", strings.NewReader(str), errctx, enableComments_local)
		return args{&ret, cancel}
	}
	a1 := newargs("1.2.", func(info errcode.ErrInfo) {
		if !info.IsErr("", 1, errcode.NewMsgUnexpected(".")) {
			panic("a1\n" + info.String())
		}
	}, 1, "a1")
	a2 := newargs("/*\n", func(info errcode.ErrInfo) {
		if !info.IsErr("", 2, nil, errcode.MLCNoEnd) {
			panic("a2\n" + info.String())
		}
	}, 1, "a2", true)
	a3 := newargs("!", func(info errcode.ErrInfo) {
		if !info.IsErr("", 1, errcode.NewMsgUnexpected("!")) {
			panic("a3\n" + info.String())
		}
	}, 1, "a3", true)
	a4 := newargs("! ", func(info errcode.ErrInfo) {
		if !info.IsErr("", 1, errcode.NewMsgUnexpected("!")) {
			panic("a4\n" + info.String())
		}
	}, 1, "a4", true)
	a5 := newargs("\"s", func(info errcode.ErrInfo) {
		if !info.IsErr("", 1, nil, errcode.StringNoEnd) {
			panic("a5\n" + info.String())
		}
	}, 1, "a5", true)
	a6 := newargs("\"s\n", func(info errcode.ErrInfo) {
		if !info.IsErr("", 1, nil, errcode.StringNoEnd) {
			panic("a6\n" + info.String())
		}
	}, 1, "a6", true)
	tests := []struct {
		name string
		args args
		want []Token
	}{
		{"意外的.", a1, []Token{}},
		{"多行注释在跨行后未结束", a2, []Token{}},
		{"最后意外的!", a3, []Token{}},
		{"意外的!", a4, []Token{}},
		{"字符串未结束", a5, []Token{}},
		{"字符串换行后", a6, []Token{}},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			var got = make([]Token, 0, len(tt.want))
			for tk := tt.args.l.Next(); tk.TYPE != EOF; tk = tt.args.l.Next() {
				got = append(got, tk)
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Line.NextToken() = %v, want %v", got, tt.want)
			}
			tt.args.cancel()
		})
	}
}

func FuzzLineNum(f *testing.F) {
	f.Fuzz(func(t *testing.T, str string) {
		linenum := strings.Count(str, "\n")
		l := newFileToken(str)
		errctx := errcode.NewErrCtx()
		ctx, cancel := context.WithCancel(context.Background())
		defer cancel()
		go errctx.Handle(ctx, func(err errcode.ErrInfo) {}, func() {})
		l.errctx = errctx
		for tk := l.Next(); tk.TYPE != EOF; tk = l.Next() {
		}
		if l.Line != linenum+1 && !errctx.Errbol() { //有错误时的行数是不对的，不检查
			t.Fatalf("%s 的行数不正确，应为%d，得到%d", str, linenum+1, l.Line)
		}
	})
}

func FuzzLineMatchWithComment(f *testing.F) {
	errctx := errcode.NewErrCtx()
	go errctx.Handle(context.Background(), func(err errcode.ErrInfo) {}, func() {})
	f.Fuzz(func(t *testing.T, str string) {
		l := newFileToken(str, true)
		l.errctx = errctx
		result := &strings.Builder{}
		for tk := l.Next(); tk.TYPE != EOF; tk = l.Next() {
			result.WriteString(tk.Value)
		}
		if !errctx.Errbol() {
			resultStr := result.String()
			if !codeEqual(str, resultStr, true) {
				t.Fatalf("%s 词法分析后得到 %s", str, result.String())
			}
		}
	})
}

func FuzzLineMatch(f *testing.F) {
	errctx := errcode.NewErrCtx()
	go errctx.Handle(context.Background(), func(err errcode.ErrInfo) {}, func() {})
	f.Fuzz(func(t *testing.T, str string) {
		l := newFileToken(str)
		l.errctx = errctx
		result := &strings.Builder{}
		for tk := l.Next(); tk.TYPE != EOF; tk = l.Next() {
			result.WriteString(tk.Value)
		}
		if !errctx.Errbol() {
			resultStr := result.String()
			if !codeEqual(str, resultStr, false) {
				t.Fatalf("%s 词法分析后得到 %s", str, resultStr)
			}
		}
	})
}

// code 比较str词法分析后的结果和原文是否具有一样的内容
func codeEqual(str, result string, withComment bool) bool {
	si, ri := 0, 0
	for ; si < len(str) && ri < len(result); si, ri = si+1, ri+1 {
		for {
			if !(si < len(str)) {
				return si == len(result)
			}

			if !withComment && str[si] == '/' && si+1 < len(str) { // 在不启用带有注释时跳过注释,因为词法分析不会把这些作为token的一部分返回
				if str[si+1] == '*' { //跳过多行注释
					for str[si] == '/' && si+1 < len(str) && str[si+1] == '*' {
						for {
							si++
							if str[si] == '*' && si+1 < len(str) && str[si+1] == '/' { //发现多行注释结束
								si += 2
								break
							}
						}
					}
				} else { //跳过单行注释
					for ; si < len(str); si++ {
						if str[si] == '\n' {
							break
						}
					}
					if !(si < len(str)) {
						return si == len(result)
					}
				}

			}
			if str[si] == ' ' || str[si] == '\r' || str[si] == '\t' { //跳过空格\r\t,因为词法分析不会把这些作为token的一部分返回
				si++
				continue
			}
			break
		}
		if str[si] != result[ri] {
			return false
		}
	}
	return true
}
