//go:build !Debug
// +build !Debug

package debug

import (
	"gitee.com/u-language/u-language/ucom/errcode"
	"gitee.com/u-language/u-language/ucom/internal/utils"
	"gitee.com/u-language/u-language/ucom/parser"
)

func TraceStart() {}

func PprofStart() {}

func TraceClose() {}

func PprofClose() {}

var Debug bool = false

func init() {
	parser.Debug = Debug
	errcode.Debug = Debug
	utils.Debug = Debug
}
