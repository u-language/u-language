//go:build Debug
// +build Debug

package debug

import (
	"os"
	"runtime/pprof"
	"runtime/trace"
	"time"

	"gitee.com/u-language/u-language/ucom/errcode"
	"gitee.com/u-language/u-language/ucom/internal/utils"
	"gitee.com/u-language/u-language/ucom/parser"
)

var cpuout, traceout *os.File

func TraceStart() {
	var err error
	traceout, err = os.Create("./trace.out")
	utils.MustErr(err)
	err = trace.Start(traceout)
	utils.MustErr(err)
}

func TraceClose() {
	trace.Stop()
	traceout.Close()
}

func PprofStart() {
	var err error
	cpuout, err = os.Create("./cpu.out")
	utils.MustErr(err)
	err = pprof.StartCPUProfile(cpuout)
	utils.MustErr(err)
}

func PprofClose() {
	time.Sleep(100 * time.Millisecond)
	pprof.StopCPUProfile()
	cpuout.Close()
}

var Debug bool = true

func init() {
	parser.Debug = Debug
	errcode.Debug = Debug
	utils.Debug = Debug
}
