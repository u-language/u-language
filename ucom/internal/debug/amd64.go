//go:build enable_amd64

package debug

import "gitee.com/u-language/u-language/ucom/fasm/amd64"

func init() {
	amd64.Debug = Debug
}
