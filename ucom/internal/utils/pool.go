package utils

import (
	"sync"
)

var MapSet = sync.Pool{
	New: func() interface{} {
		return make(map[string]struct{})
	},
}

func PutMapSet(m map[string]struct{}) {
	for k := range m {
		delete(m, k)
	}
	MapSet.Put(m)
}
