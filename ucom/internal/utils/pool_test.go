package utils_test

import (
	"testing"

	. "gitee.com/u-language/u-language/ucom/internal/utils"
)

func TestMapSet(t *testing.T) {
	for i := 0; i < 100; i++ {
		set := MapSet.Get().(map[string]struct{})
		if len(set) != 0 {
			t.Fatal("len(set) != 0")
		}
		set["h"] = struct{}{}
		PutMapSet(set)
	}
}
