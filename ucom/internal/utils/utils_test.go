package utils

import "testing"

func TestTyp_is_equal(t *testing.T) {
	type args struct {
		src1 string
		src2 string
	}
	tests := []struct {
		name string
		args args
		want bool
	}{
		{"两个int", args{"int", "int"}, true},
		{"两个指针", args{"&T", "&T"}, true},
		{"不同的类型", args{"int", "float"}, false},
		{"不同的指针类型", args{"&int", "&float"}, false},
		{"指针与nil", args{"&int", "nil"}, true},
		{"无类型指针与nil", args{"nil", "unsafe__Pointer"}, true},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := Typ_is_equal(tt.args.src1, tt.args.src2); got != tt.want {
				t.Errorf("Typ_is_equal() = %v, want %v", got, tt.want)
			}
		})
	}
}
