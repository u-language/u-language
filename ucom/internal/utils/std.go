package utils

import (
	"path/filepath"

	"gitee.com/u-language/u-language/ucom/config"
)

type StdInfo struct {
	CFile []string
	HDir  []string
}

func newStdInfo(CFile []string, HDir []string) StdInfo {
	return StdInfo{CFile: CFile, HDir: HDir}
}

var StdOne = make(map[string]StdInfo, 0)

func initStd() {
	StdOne["mempool"] = newStdInfo([]string{"std/mempool/mempool.c"}, []string{"std/mempool/"})
	for _, std := range StdOne {
		joinlist(std.CFile)
		joinlist(std.HDir)
	}
}

func joinlist(file []string) {
	for i := 0; i < len(file); i++ {
		file[i] = filepath.Join(config.URoot, file[i])
	}
}
