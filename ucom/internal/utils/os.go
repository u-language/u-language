//go:build !windows

package utils

import "os/exec"

func moveFile(oldname, newname string) error {
	cmd := exec.Command("mv", oldname, newname)
	return cmd.Run()
}

var Ext = ""
