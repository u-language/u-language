package utils_test

import (
	"bufio"
	"reflect"
	"strings"
	"testing"

	. "gitee.com/u-language/u-language/ucom/internal/utils"
)

func TestRet_type_no_lea(t *testing.T) {
	type args struct {
		s string
	}
	tests := []struct {
		name  string
		args  args
		want  string
		want1 int
	}{
		{"a", args{"a"}, "a", 0},
		{"&a", args{"&a"}, "a", 1},
		{"&&a", args{"&&a"}, "a", 2},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, got1 := Ret_type_no_lea(tt.args.s)
			if got != tt.want {
				t.Errorf("Ret_type_no_lea() got = %v, want %v", got, tt.want)
			}
			if got1 != tt.want1 {
				t.Errorf("Ret_type_no_lea() got1 = %v, want %v", got1, tt.want1)
			}
		})
	}
}

func TestRet_no_lea(t *testing.T) {
	type args struct {
		s string
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		{"a", args{"a"}, "a"},
		{"&a", args{"&a"}, "a"},
		{"&&a", args{"&&a"}, "a"},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := Ret_no_lea(tt.args.s); got != tt.want {
				t.Errorf("Ret_no_lea() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestNoExtBase(t *testing.T) {
	type args struct {
		f string
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		{"/e/f.txt", args{"/e/f.txt"}, "f"},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := NoExtBase(tt.args.f); got != tt.want {
				t.Errorf("NoExtBase() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestReadAllLine(t *testing.T) {
	type args struct {
		r *bufio.Reader
	}
	a1b := bufio.NewReader(strings.NewReader("1\n2\n3"))
	a1 := args{a1b}
	tests := []struct {
		name    string
		args    args
		want    []string
		wantErr bool
	}{
		{"123", a1, []string{"1", "2", "3"}, false},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := ReadAllLine(tt.args.r)
			if (err != nil) != tt.wantErr {
				t.Errorf("ReadAllLine() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("ReadAllLine() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestElem(t *testing.T) {
	type args struct {
		T string
	}
	tests := []struct {
		name  string
		args  args
		want  string
		want1 string
		want2 bool
	}{
		{"[2]int", args{"[2]int"}, "int", "[2]", true},
		{"[2][2]int", args{"[2][2]int"}, "[2]int", "[2]", true},
		{"[2][2][2]int", args{"[2][2][2]int"}, "[2][2]int", "[2]", true},
		{"int", args{"int"}, "", "", false},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, got1, got2 := Elem(tt.args.T)
			if got != tt.want {
				t.Errorf("Elem() got = %v, want %v", got, tt.want)
			}
			if got1 != tt.want1 {
				t.Errorf("Elem() got1 = %v, want %v", got1, tt.want1)
			}
			if got2 != tt.want2 {
				t.Errorf("Elem() got2 = %v, want %v", got2, tt.want2)
			}
		})
	}
}

func TestSplit_package_name(t *testing.T) {
	type args struct {
		s string
	}
	tests := []struct {
		name  string
		args  args
		want  string
		want1 string
	}{
		{"内置函数", args{"malloc"}, "", "malloc"},
		{"包函数", args{"main__str"}, "main", "str"},
		{"指针类型", args{"&main__str"}, "main", "&str"},
		{"二级指针", args{"&&main__str"}, "main", "&&str"},
		{"不安全函数", args{"unsafe__Sizeof"}, "", "unsafe__Sizeof"},
		{"包名为空函数", args{"__Sizeof"}, "", "Sizeof"},
		{"有包分隔符", args{"main__str__Sizeof"}, "main", "str__Sizeof"},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, got1 := Split_package_name(tt.args.s)
			if got != tt.want {
				t.Errorf("Split_package_name() got = %v, want %v", got, tt.want)
			}
			if got1 != tt.want1 {
				t.Errorf("Split_package_name() got1 = %v, want %v", got1, tt.want1)
			}
		})
	}
}
