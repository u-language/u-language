package utils

import (
	"strconv"
	"strings"

	"gitee.com/u-language/u-language/ucom/enum"
)

// GenerateGenericTypes 生成已经实际实例化的泛型类型名
func GenerateGenericTypes(BaseName string, ActualType []string) string {
	var buf strings.Builder
	l := len(BaseName) + 1
	for i := range ActualType {
		l += len(ActualType[i]) + 1
	}
	buf.Grow(l)
	buf.WriteString(BaseName)
	buf.WriteString("_")
	for _, v := range ActualType[:len(ActualType)-1] {
		buf.WriteString(v)
		buf.WriteString("_")
	}
	buf.WriteString(ActualType[len(ActualType)-1])
	return buf.String()
}

// Generate_mempool_Name 生成 自动释放快 的实现 内存池变量名
// - linenum是行号
// - filename是文件名
func Generate_mempool_Name(linenum int, filename string) string {
	var buf strings.Builder
	filename = NoExtBase(filename)
	line := strconv.Itoa(linenum)
	buf.Grow(len(enum.Generate) + len(filename) + len(line))
	buf.WriteString(enum.Generate)
	buf.WriteString(filename)
	buf.WriteString(line)
	return buf.String()
}

// GeneratePackageSymbol 生成包含包名的符号
func GeneratePackageSymbol(packageName string, Name string) string {
	var buf strings.Builder
	buf.Grow(len(packageName) + len(enum.PackageSep) + len(Name))
	buf.WriteString(packageName)
	buf.WriteString(enum.PackageSep)
	buf.WriteString(Name)
	return buf.String()
}
