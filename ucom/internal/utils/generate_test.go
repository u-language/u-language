package utils_test

import (
	"testing"

	. "gitee.com/u-language/u-language/ucom/internal/utils"
)

func TestGenerateGenericTypes(t *testing.T) {
	type args struct {
		BaseName   string
		ActualType []string
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		{"a[int]", args{"a", []string{"int"}}, "a_int"},
		{"a[int,int]", args{"a", []string{"int", "int"}}, "a_int_int"},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := GenerateGenericTypes(tt.args.BaseName, tt.args.ActualType); got != tt.want {
				t.Errorf("GenerateGenericTypes() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestGenerate_mempool_Name(t *testing.T) {
	type args struct {
		linenum  int
		filename string
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		{"a.u:1", args{1, "a.u"}, "generatea1"},
		{"a.u:2", args{2, "a.u"}, "generatea2"},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := Generate_mempool_Name(tt.args.linenum, tt.args.filename); got != tt.want {
				t.Errorf("Generate_mempool_Name() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestGeneratePackageSymbol(t *testing.T) {
	type args struct {
		packageName string
		Name        string
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		{"main.a", args{"main", "a"}, "main__a"},
		{"dep.S", args{"dep", "S"}, "dep__S"},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := GeneratePackageSymbol(tt.args.packageName, tt.args.Name); got != tt.want {
				t.Errorf("GeneratePackageSymbol() = %v, want %v", got, tt.want)
			}
		})
	}
}
