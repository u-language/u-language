package utils

import (
	"syscall"

	"golang.org/x/sys/windows"
)

func moveFile(oldname, newname string) error {
	old, err := syscall.UTF16PtrFromString(oldname)
	if err != nil {
		return err
	}
	n, err := syscall.UTF16PtrFromString(newname)
	if err != nil {
		return err
	}
	return windows.MoveFileEx(old, n, windows.MOVEFILE_COPY_ALLOWED|windows.MOVEFILE_REPLACE_EXISTING)
}

var Ext = ".exe"
