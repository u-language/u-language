// Packags errutil 提供错误的增强功能
package errutil

// UnknownSymbol 表示未知的符号错误
var UnknownSymbol = "未知的符号"

// UnknownType 表示未知的类型错误
var UnknownType = "未知的类型"

// CompileErr 表示编译器错误
var CompileErr = "编译器错误："

// Err2 是由错误类型和描述信息组成的错误
type Err2 struct {
	typ string
	msg string
}

func (err Err2) Error() string {
	return err.typ + err.msg
}

func NewErr2(typ string, msg string) Err2 {
	return Err2{typ: typ, msg: msg}
}

// IsUnknownSymbol 返回是否是一个变量不存在错误，以及变量名（如果有）
// 实现假设 参数err如果类型是Err2,msg字段是变量名
func IsUnknownSymbol(err interface{}) (string, bool) {
	err2, ok := err.(Err2)
	if ok && err2.typ == UnknownSymbol {
		return err2.msg, true
	}
	return "", false
}

// IsUnknownType 返回是否是一个类型不存在错误，以及变量名（如果有）
// 实现假设 参数err如果类型是Err2,msg字段是类型名
func IsUnknownType(err interface{}) (string, bool) {
	err2, ok := err.(Err2)
	if ok && err2.typ == UnknownType {
		return err2.msg, true
	}
	return "", false
}

// IsCompileErr 返回是否是一个编译器错误
func IsCompileErr(err error) bool {
	err2, ok := err.(Err2)
	if ok && err2.typ == CompileErr {
		return true
	}
	return false
}
