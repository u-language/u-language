// Package teco 提供测试时需要的常量
package teco

import (
	"gitee.com/u-language/u-language/ucom/lex"
)

var (
	LexVar       = lex.NewToken(lex.VAR, "var")
	LexAssign    = lex.NewToken(lex.ASSIGN, "=")
	LexLess      = lex.NewToken(lex.Less, "<")
	LexAdd       = lex.NewToken(lex.ADD, "+")
	LexEmptyLine = lex.NewLine(1, []lex.Token{})
	LexInt       = lex.NewToken(lex.NAME, "int")
	LexFloat     = lex.NewToken(lex.NAME, "float")
	LexEqual     = lex.NewToken(lex.Equal, "==")
	LexNoEqual   = lex.NewToken(lex.NoEqual, "!=")
	LexMul       = lex.NewToken(lex.MUL, "*")
	LexLPAREN    = lex.NewToken(lex.LPAREN, "(")
	LexRPAREN    = lex.NewToken(lex.RPAREN, ")")
)
