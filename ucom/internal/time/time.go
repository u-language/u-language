package time

import (
	"fmt"
	stdtime "time"
)

// Time 保存当前时间和一段信息
type Time struct {
	now     stdtime.Time
	message string
}

// NewTime 将当前时间和一段信息保存到 [Time]
func NewTime(message string) Time {
	return Time{message: message, now: stdtime.Now()}
}

// PrintlnSubNow 打印保存的信息和从 [NewTime] 到现在经过的时间
func (t Time) PrintlnSubNow() {
	fmt.Println(t.message, stdtime.Since(t.now))
}
