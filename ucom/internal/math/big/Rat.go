package big

import (
	"strconv"
	_ "unsafe"
)

type 分数 struct { //分数类型
	分子, 分母 int
}

type RationalNumber = 分数

type Rat = RationalNumber

// 返回字符串表示
func (r Rat) String() string {
	return strconv.Itoa(r.分子) + "/" + strconv.Itoa(r.分母)
}

func NewRat(a, b int) Rat {
	return Rat{
		分子: a,
		分母: b,
	}
}

//go:linkname AddRat 分数相加
func AddRat(a, b Rat) Rat

//go:linkname SubRat 分数相减
func SubRat(a, b Rat) Rat

//go:linkname MulRat 分数相乘
func MulRat(a, b Rat) Rat

//go:linkname DivRat 分数相除
func DivRat(a, b Rat) Rat
