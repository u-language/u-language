package big

import (
	"testing"
)

func BenchmarkNewFloat(b *testing.B) {
	b.SetBytes(1)
	b.ReportAllocs()
	for i := 0; i < b.N; i++ {
		_ = NewFloat("1", "2", true)
	}
}

func BenchmarkAddFloat(b *testing.B) {
	b.SetBytes(1)
	b.ReportAllocs()
	a := NewFloat("1", "2", true)
	c := NewFloat("2", "2", true)
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		_ = AddFloat(a, c)
	}
}

func BenchmarkSubFloat(b *testing.B) {
	b.SetBytes(1)
	b.ReportAllocs()
	a := NewFloat("1", "2", true)
	c := NewFloat("2", "2", true)
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		_ = SubFloat(a, c)
	}
}

func BenchmarkMulFloat(b *testing.B) {
	b.SetBytes(1)
	b.ReportAllocs()
	a := NewFloat("1", "2", true)
	c := NewFloat("2", "2", true)
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		_ = MulFloat(a, c)
	}
}

func BenchmarkDivFloat(b *testing.B) {
	b.SetBytes(1)
	b.ReportAllocs()
	a := NewFloat("1", "2", true)
	c := NewFloat("2", "2", true)
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		_ = DivFloat(a, c)
	}
}
