package big

import (
	_ "unsafe"
)

var (
	FloatAdd = 小数相加
	FloatSub = 小数相减
	FloatMul = 小数相乘
	FloatDiv = 小数相除
)

type Float = 小数

func (f Float) String() string {
	if f.是否为负 == false {
		return f.整数部分 + "." + f.小数部分
	} else {
		return "-" + f.整数部分 + "." + f.小数部分
	}
}

func NewFloat(a, b string, c bool) Float {
	return Float{
		整数部分: a,
		小数部分: b,
		是否为负: c,
	}
}

func AddFloat(a, b Float) Float {
	return FloatAdd(a, b)
}

func SubFloat(a, b Float) Float {
	return FloatSub(a, b)
}

func MulFloat(a, b Float) Float {
	return FloatMul(a, b)
}

func DivFloat(a, b Float) Float {
	return FloatDiv(a, b)
}
