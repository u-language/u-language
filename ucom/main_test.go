package main

import (
	"os"
	"os/exec"
	"path/filepath"
	stddebug "runtime/debug"
	"strings"
	"testing"
	"time"
	_ "unsafe"

	"gitee.com/u-language/u-language/ucom/ast"
	"gitee.com/u-language/u-language/ucom/ast2"
	"gitee.com/u-language/u-language/ucom/cast"
	"gitee.com/u-language/u-language/ucom/cast2"
	"gitee.com/u-language/u-language/ucom/check3"
	"gitee.com/u-language/u-language/ucom/config"
	"gitee.com/u-language/u-language/ucom/errcode"
	"gitee.com/u-language/u-language/ucom/internal/utils"
	"gitee.com/u-language/u-language/ucom/parser"
)

var tests = []struct {
	name      string
	args      args
	want      string
	wantInEnd bool
	notTest   bool
}{
	{name: "输出456", args: a1, want: "456"},
	{name: "for加1，1万次，输出10000", args: a2, want: "10000"},
	{name: "for代码块和main代码块都有变量i，输出10000", args: a3, want: "10000"},
	{name: "输出1-10000中偶数的数量,比较\"s\"==\"a\"输出false", args: a4, want: "5000\tfalse"},
	{name: "使用指针循环加一个int变量自增10000次", args: a5, want: "10000"},
	{name: "输出结构体字段，结果字符串11", args: a6, want: "11"},
	{name: "整数和浮点数互相转换", args: a7, want: "1 1.000000"},
	{name: "调用malloc和free", args: a8, want: "1"},
	{name: "使用自操作语句", args: a9, want: "10", wantInEnd: true},
	{name: "使用nil", args: a10, want: "1nilnil"},
	{name: "使用选择器取地址与解引用", args: a11, want: "1"},
	{name: "使用init函数", args: a12, want: "1"},
	{name: "多文件", args: a13, want: "1101nilnil11"},
	{name: "switch", args: a14, want: ">=3>=2>=1!=3,2,1,0"},
	{name: "使用方法", args: a15, want: "1"},
	{name: "使用指针运算和指针类型转换", args: a16, want: "1"},
	{name: "使用自动释放块", args: a17, want: "1", notTest: true},
	{name: "多文件使用自动释放块", args: a18, want: "0", notTest: true},
	{name: "位与，位或，异或，逻辑运算,括号表达式", args: a19, want: "011truetruetrue"},
	{name: "包导入包", args: a20, want: "42"},
	{name: "包导入包(文件模式)", args: a21, want: "42"},
	{name: "枚举", args: a22, want: "o1"},
	{name: "数组类型与索引表达式", args: a23, want: "222"},
	{name: "泛型", args: a24, want: "1 1.2", notTest: true},
	{name: "泛型(目录模式)", args: a25, want: "1 1.2", notTest: true},
	{name: "多重导入", args: a26, want: "44", notTest: true},
	{name: "多重导入(文件模式)", args: a27, want: "44", notTest: true},
	{name: "跨包使用自动释放块(文件模式)", args: a28, want: strings.Repeat("9", 10), notTest: true},
	{name: "跨包使用自动释放块", args: a29, want: strings.Repeat("9", 10), notTest: true},
	{name: "goto语句", args: a30, want: "2"},
}

type args struct {
	path      string
	cfilename string
	outname   string
}

var (
	a1  = args{path: "../testdata/5.txt", cfilename: "5.c", outname: "5" + utils.Ext}
	a2  = args{path: "../testdata/6.txt", cfilename: "6.c", outname: "6" + utils.Ext}
	a3  = args{path: "../testdata/7.txt", cfilename: "7.c", outname: "7" + utils.Ext}
	a4  = args{path: "../testdata/8.txt", cfilename: "8.c", outname: "8" + utils.Ext}
	a5  = args{path: "../testdata/9.txt", cfilename: "9.c", outname: "9" + utils.Ext}
	a6  = args{path: "../testdata/10.txt", cfilename: "10.c", outname: "10" + utils.Ext}
	a7  = args{path: "../testdata/11.txt", cfilename: "11.c", outname: "11" + utils.Ext}
	a8  = args{path: "../testdata/12.txt", cfilename: "12.c", outname: "12" + utils.Ext}
	a9  = args{path: "../testdata/13.txt", cfilename: "13.c", outname: "13" + utils.Ext}
	a10 = args{path: "../testdata/14.txt", cfilename: "14.c", outname: "14" + utils.Ext}
	a11 = args{path: "../testdata/15.txt", cfilename: "15.c", outname: "15" + utils.Ext}
	a12 = args{path: "../testdata/16.txt", cfilename: "16.c", outname: "16" + utils.Ext}
	a13 = args{path: "../testdata/u17", cfilename: "./u17", outname: "17" + utils.Ext}
	a14 = args{path: "../testdata/18.txt", cfilename: "18.c", outname: "18" + utils.Ext}
	a15 = args{path: "../testdata/19.txt", cfilename: "19.c", outname: "19" + utils.Ext}
	a16 = args{path: "../testdata/20.txt", cfilename: "20.c", outname: "20" + utils.Ext}
	a17 = args{path: "../testdata/21.txt", cfilename: "21.c", outname: "21" + utils.Ext}
	a18 = args{path: "../testdata/u22", cfilename: "./u22", outname: "22" + utils.Ext}
	a19 = args{path: "../testdata/23.txt", cfilename: "u23.c", outname: "23" + utils.Ext}
	a20 = args{path: "../testdata/u24", cfilename: "./u24", outname: "24" + utils.Ext}
	a21 = args{path: "../testdata/u24/main.u", cfilename: "./u24u.c", outname: "24u" + utils.Ext}
	a22 = args{path: "../testdata/25.u", cfilename: "25.c", outname: "25" + utils.Ext}
	a23 = args{path: "../testdata/26.u", cfilename: "26.c", outname: "26" + utils.Ext}
	a24 = args{path: "../testdata/u27/main.u", cfilename: "27.c", outname: "27" + utils.Ext}
	a25 = args{path: "../testdata/u27", cfilename: "./u27u.c", outname: "27u" + utils.Ext}
	a26 = args{path: "../testdata/u28", cfilename: "./u28u.c", outname: "28u" + utils.Ext}
	a27 = args{path: "../testdata/u28/main.u", cfilename: "./27.c", outname: "28" + utils.Ext}
	a28 = args{path: "../testdata/u29/main.u", cfilename: "./29.c", outname: "29" + utils.Ext}
	a29 = args{path: "../testdata/u29", cfilename: "./29u.c", outname: "29u" + utils.Ext}
	a30 = args{path: "../testdata/30.u", cfilename: "./30u.c", outname: "30u" + utils.Ext}
)

func TestComplierOut(t *testing.T) {
	tmp, err := os.MkdirTemp("", "c*")
	TryErr(t, err)
	//替换相对路径为绝对路径
	for i := 0; i < len(tests); i++ {
		tests[i].args.path, err = filepath.Abs(tests[i].args.path)
		TryErr(t, err)
	}
	//获取当前工作目录
	dir, err := os.Getwd()
	TryErr(t, err)
	t.Cleanup(func() {
		//测试结束后切换工作目录到原本的目录
		err = os.Chdir(dir)
		TryErr(t, err)
		//删除临时目录
		err = os.RemoveAll(tmp)
		TryErr(t, err)
	})
	err = os.Chdir(tmp)
	TryErr(t, err)
	for _, ttf := range tests {
		tt := ttf
		t.Run(tt.name, func(tr *testing.T) {
			tr.Parallel()
			defer utils.Deferfunc()
			out := tmp + "\\" + tt.args.outname
			Build(tt.args.path, out, 1)
			cmd := exec.Command(out)
			buf, err := cmd.CombinedOutput()
			if tt.wantInEnd {
				if !strings.HasSuffix(string(buf), tt.want) {
					tr.Errorf("got=%s \n want=%s", string(buf), tt.want)
				}
			} else if string(buf) != tt.want {
				tr.Errorf("got=%s \n want=%s", string(buf), tt.want)
			}

			TryErr(tr, err)
		})
	}
}

func init() {
	config.PrintTime = "off"
	utils.Debug = true
	errcode.Debug = true
	test = true
	outputPrint = true
}

//go:linkname test gitee.com/u-language/u-language/ucom/parser.test
var test bool

func BenchmarkComplierToC(b *testing.B) {
	for _, ttf := range tests {
		tt := ttf
		b.Run(tt.name, func(b *testing.B) {
			b.SetBytes(1)
			b.ReportAllocs()
			for i := 0; i < b.N; i++ {
				//解析获得抽象语法树，并进行语义检查
				iface, err := parser.ParserAuto(tt.args.path, Thread, errcode.DefaultErrCtx)
				utils.MustErr(err)
				if errcode.Errbol() { //代码有错误
					time.Sleep(1000 * time.Second) //代码有错误不应继续执行
				}
				switch iface.(type) {
				case *ast.Tree:
					toc := cast.NewUtoC()
					ast := iface.(*ast.Tree)
					toc.Parser(ast, false) //转换为C抽象语法树
					toc.C()
				case *ast.Package:
					p := cast.NewPackage(Thread)
					astp := iface.(*ast.Package)
					p.AddUastSlice(astp, nil)
					p.String()
				}
			}
		})
	}
}

func TryErr(tb testing.TB, err error) {
	if err != nil {
		stddebug.PrintStack()
		tb.Fatal(err)
	}
}

func TestBuildReproducible(t *testing.T) {
	for _, ttf := range tests {
		tt := ttf
		t.Run(tt.name, func(t *testing.T) {
			t.Parallel()
			var result string
			for i := 0; i < 100; i++ {
				//解析获得抽象语法树，并进行语义检查
				iface, err := parser.ParserAuto(tt.args.path, Thread, errcode.DefaultErrCtx)
				utils.MustErr(err)
				if errcode.Errbol() { //代码有错误
					time.Sleep(1000 * time.Second) //代码有错误不应继续执行
				}
				switch ast := iface.(type) {
				case *ast.Tree:
					toc := cast.NewUtoC()
					toc.Parser(ast, false) //转换为C抽象语法树
					checkResult(&result, toc.C(), t)
				case *ast.Package:
					p := cast.NewPackage(Thread)
					p.AddUastSlice(ast, nil)
					var buf strings.Builder
					p.Oupput(&buf, p.GenerateheaderFile())
					checkResult(&result, buf.String(), t)
				}
			}
		})
	}
}

func checkResult(save *string, new string, t *testing.T) {
	if *save == "" {
		*save = new
		return
	}
	if *save != new {
		t.Fatalf("\n不一致的构建，save=%s,\n,new=%s", *save, new)
		//t.Fatalf("\n不一致的构建，%s", cmp.Diff(*save, new))
	}
}

func TestComplierOutBuildMode2(t *testing.T) {
	tmp, err := os.MkdirTemp("", "c*")
	TryErr(t, err)
	//替换相对路径为绝对路径
	for i := 0; i < len(tests); i++ {
		tests[i].args.path, err = filepath.Abs(tests[i].args.path)
		TryErr(t, err)
	}
	//获取当前工作目录
	dir, err := os.Getwd()
	TryErr(t, err)
	t.Cleanup(func() {
		//测试结束后切换工作目录到原本的目录
		err = os.Chdir(dir)
		TryErr(t, err)
		//删除临时目录
		err = os.RemoveAll(tmp)
		TryErr(t, err)
	})
	err = os.Chdir(tmp)
	TryErr(t, err)
	for _, ttf := range tests {
		tt := ttf
		if tt.notTest {
			continue
		}
		t.Run(tt.name, func(tr *testing.T) {
			tr.Parallel()
			defer utils.Deferfunc()
			out := tmp + "\\" + tt.args.outname
			Build(tt.args.path, out, 2)
			cmd := exec.Command(out)
			buf, err := cmd.CombinedOutput()
			if tt.wantInEnd {
				if !strings.HasSuffix(string(buf), tt.want) {
					tr.Errorf("got=%s \n want=%s", string(buf), tt.want)
				}
			} else if string(buf) != tt.want {
				tr.Errorf("got=%s \n want=%s", string(buf), tt.want)
			}

			TryErr(tr, err)
		})
	}
}

func BenchmarkComplierToCBuildMode2(b *testing.B) {
	for _, ttf := range tests {
		tt := ttf
		if tt.notTest {
			continue
		}
		b.Run(tt.name, func(b *testing.B) {
			b.SetBytes(1)
			b.ReportAllocs()
			for i := 0; i < b.N; i++ {
				//解析获得抽象语法树，并进行语义检查
				tree, err := parser.ParserAutoBuildMode2(tt.args.path, Thread, errcode.DefaultErrCtx)
				utils.MustErr(err)
				if errcode.Errbol() { //代码有错误
					time.Sleep(1000 * time.Second) //代码有错误不应继续执行
				}
				switch tree := tree.(type) {
				case *ast2.Tree:
					toc := &cast2.UtoC{}
					toc.Parser(tree) //转换为C抽象语法树
					_ = toc.C(true)
				case *ast2.Package:
					p := cast2.NewPackage(Thread, tree, true)
					_ = p.String()
				}
			}
		})
	}
}

func init() {
	check3.Test = true
}
