package ast

import (
	"reflect"
	"testing"

	"gitee.com/u-language/u-language/ucom/enum"
	"gitee.com/u-language/u-language/ucom/errcode"
	"gitee.com/u-language/u-language/ucom/lex"
)

func Test_stack_AutoExpr(t *testing.T) {
	type args struct {
		t    *Tree
		line *lex.Line
	}
	a1lex := lex.Lex("1.txt", []string{"package main", "var b int", "b=1+2+3"}, errcode.DefaultErrCtx, false)
	a1 := args{NewTree(a1lex, errcode.DefaultErrCtx, NewSbt(true), nil ), &a1lex.Value[2]}
	w1 := NewASSIGNNode(NewObject(SymbolObj, "b"), NewOpExpr(enum.ADDOP, NewOpExpr(enum.ADDOP, NewObject(INTOBJ, "1"), NewObject(INTOBJ, "2")), NewObject(INTOBJ, "3")))
	//a2
	a2lex := lex.Lex("1.txt", []string{"package main", "var b int", "b=1+2-3"}, errcode.DefaultErrCtx, false)
	a2 := args{NewTree(a1lex, errcode.DefaultErrCtx, NewSbt(true), nil), &a2lex.Value[2]}
	w2 := NewASSIGNNode(NewObject(SymbolObj, "b"), NewOpExpr(enum.SUBOP, NewOpExpr(enum.ADDOP, NewObject(INTOBJ, "1"), NewObject(INTOBJ, "2")), NewObject(INTOBJ, "3")))
	//a3
	a3lex := lex.Lex("1.txt", []string{"package main", "var b int", "b=1*2*3"}, errcode.DefaultErrCtx, false)
	a3 := args{NewTree(a1lex, errcode.DefaultErrCtx, NewSbt(true), nil), &a3lex.Value[2]}
	w3 := NewASSIGNNode(NewObject(SymbolObj, "b"), NewOpExpr(enum.MULOP, NewOpExpr(enum.MULOP, NewObject(INTOBJ, "1"), NewObject(INTOBJ, "2")), NewObject(INTOBJ, "3")))
	//a4
	a4lex := lex.Lex("1.txt", []string{"package main", "var b int", "b=1+2/3"}, errcode.DefaultErrCtx, false)
	a4 := args{NewTree(a1lex, errcode.DefaultErrCtx, NewSbt(true), nil), &a4lex.Value[2]}
	w4 := NewASSIGNNode(NewObject(SymbolObj, "b"), NewOpExpr(enum.ADDOP, NewObject(INTOBJ, "1"), NewOpExpr(enum.DIVOP, NewObject(INTOBJ, "2"), NewObject(INTOBJ, "3"))))
	tests := []struct {
		name string
		s    *stack
		args args
		want Node
	}{
		{"3个数相加", newstack(nil, false), a1, w1},
		{"1+2-3", newstack(nil, false), a2, w2},
		{"三个数相乘", newstack(nil, false), a3, w3},
		{"1+2*3", newstack(nil, false), a4, w4},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.s.AutoExpr(tt.args.t, tt.args.line); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("stack.AutoExpr() = %v, want %v", got, tt.want)
			}
		})
	}
}

func BenchmarkStack_AutoExpr(b *testing.B) {
	type args struct {
		t    *Tree
		line *lex.Line
	}
	a1lex := lex.Lex("1.txt", []string{"package main", "var b int", "b=1+2+3"}, errcode.DefaultErrCtx, false)
	a1 := args{NewTree(a1lex, errcode.DefaultErrCtx, NewSbt(true), nil), &a1lex.Value[2]}
	w1 := NewASSIGNNode(NewObject(SymbolObj, "b"), NewOpExpr(enum.ADDOP, NewOpExpr(enum.ADDOP, NewObject(INTOBJ, "1"), NewObject(INTOBJ, "2")), NewObject(INTOBJ, "3")))
	//a2
	a2lex := lex.Lex("1.txt", []string{"package main", "var b int", "b=1+2-3"}, errcode.DefaultErrCtx, false)
	a2 := args{NewTree(a1lex, errcode.DefaultErrCtx, NewSbt(true), nil), &a2lex.Value[2]}
	w2 := NewASSIGNNode(NewObject(SymbolObj, "b"), NewOpExpr(enum.SUBOP, NewOpExpr(enum.ADDOP, NewObject(INTOBJ, "1"), NewObject(INTOBJ, "2")), NewObject(INTOBJ, "3")))
	//a3
	a3lex := lex.Lex("1.txt", []string{"package main", "var b int", "b=1*2*3"}, errcode.DefaultErrCtx, false)
	a3 := args{NewTree(a1lex, errcode.DefaultErrCtx, NewSbt(true), nil), &a3lex.Value[2]}
	w3 := NewASSIGNNode(NewObject(SymbolObj, "b"), NewOpExpr(enum.MULOP, NewOpExpr(enum.MULOP, NewObject(INTOBJ, "1"), NewObject(INTOBJ, "2")), NewObject(INTOBJ, "3")))
	//a4
	a4lex := lex.Lex("1.txt", []string{"package main", "var b int", "b=1+2/3"}, errcode.DefaultErrCtx, false)
	a4 := args{NewTree(a1lex, errcode.DefaultErrCtx, NewSbt(true), nil), &a4lex.Value[2]}
	w4 := NewASSIGNNode(NewObject(SymbolObj, "b"), NewOpExpr(enum.ADDOP, NewObject(INTOBJ, "1"), NewOpExpr(enum.DIVOP, NewObject(INTOBJ, "2"), NewObject(INTOBJ, "3"))))
	tests := []struct {
		name string
		s    *stack
		args args
		want Node
	}{
		{"3个数相加", newstack(nil, false), a1, w1},
		{"1+2-3", newstack(nil, false), a2, w2},
		{"三个数相乘", newstack(nil, false), a3, w3},
		{"1+2*3", newstack(nil, false), a4, w4},
	}
	for _, tt := range tests {
		b.Run(tt.name, func(b *testing.B) {
			b.SetBytes(1)
			b.ReportAllocs()
			for i := 0; i < b.N; i++ {
				tt.s.AutoExpr(tt.args.t, tt.args.line)
			}
		})
	}
}

func Test_stack_AutoParameList(t *testing.T) {
	type args struct {
		t    *Tree
		line *lex.Line
	}
	a1lex := lex.Lex("1.txt", []string{"b()"}, errcode.DefaultErrCtx, false)
	a1 := args{nil, &a1lex.Value[0]}
	//a2
	a2lex := lex.Lex("1.txt", []string{"b(1)"}, errcode.DefaultErrCtx, false)
	a2 := args{nil, &a2lex.Value[0]}
	//a3
	a3lex := lex.Lex("1.txt", []string{"b(1,1)"}, errcode.DefaultErrCtx, false)
	a3 := args{nil, &a3lex.Value[0]}
	//a4
	a4lex := lex.Lex("1.txt", []string{"b(a())"}, errcode.DefaultErrCtx, false)
	a4 := args{nil, &a4lex.Value[0]}
	//a5
	a5lex := lex.Lex("1.txt", []string{"b(a(),a())"}, errcode.DefaultErrCtx, false)
	a5 := args{nil, &a5lex.Value[0]}
	//a6
	a6lex := lex.Lex("1.txt", []string{"b(a()+a())"}, errcode.DefaultErrCtx, false)
	a6 := args{nil, &a6lex.Value[0]}
	//a7
	a7lex := lex.Lex("1.txt", []string{"b(2,a()+a())"}, errcode.DefaultErrCtx, false)
	a7 := args{nil, &a7lex.Value[0]}
	tests := []struct {
		name string
		s    *stack
		args args
		want []Expr
	}{
		{"无参函数调用", newstack(nil, false), a1, nil},
		{"整数参数", newstack(nil, false), a2, []Expr{NewObject(INTOBJ, "1")}},
		{"两个整数参数", newstack(nil, false), a3, []Expr{NewObject(INTOBJ, "1"), NewObject(INTOBJ, "1")}},
		{"函数调用表达式做参数", newstack(nil, false), a4, []Expr{NewCallExpr(NewObject(SymbolObj, "a"), false)}},
		{"两个函数调用表达式做参数", newstack(nil, false), a5, []Expr{NewCallExpr(NewObject(SymbolObj, "a"), false), NewCallExpr(NewObject(SymbolObj, "a"), false)}},
		{"运算表达式两个函数调用表达式做参数", newstack(nil, false), a6, []Expr{NewOpExpr(enum.ADDOP, NewCallExpr(NewObject(SymbolObj, "a"), false), NewCallExpr(NewObject(SymbolObj, "a"), false))}},
		{"数字和运算表达式两个函数调用表达式做参数", newstack(nil, false), a7, []Expr{NewObject(INTOBJ, "2"), NewOpExpr(enum.ADDOP, NewCallExpr(NewObject(SymbolObj, "a"), false), NewCallExpr(NewObject(SymbolObj, "a"), false))}},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.s.AutoParameList(tt.args.t, tt.args.line); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("stack.AutoParameList() = %s, want %s", got, tt.want)
			}
		})
	}
}

func BenchmarkStack_AutoParameList(b *testing.B) {
	type args struct {
		t    *Tree
		line *lex.Line
	}
	a1lex := lex.Lex("1.txt", []string{"b()"}, errcode.DefaultErrCtx, false)
	a1 := args{nil, &a1lex.Value[0]}
	//a2
	a2lex := lex.Lex("1.txt", []string{"b(1)"}, errcode.DefaultErrCtx, false)
	a2 := args{nil, &a2lex.Value[0]}
	//a3
	a3lex := lex.Lex("1.txt", []string{"b(1,1)"}, errcode.DefaultErrCtx, false)
	a3 := args{nil, &a3lex.Value[0]}
	//a4
	a4lex := lex.Lex("1.txt", []string{"b(a())"}, errcode.DefaultErrCtx, false)
	a4 := args{nil, &a4lex.Value[0]}
	//a5
	a5lex := lex.Lex("1.txt", []string{"b(a(),a())"}, errcode.DefaultErrCtx, false)
	a5 := args{nil, &a5lex.Value[0]}
	//a6
	a6lex := lex.Lex("1.txt", []string{"b(a()+a())"}, errcode.DefaultErrCtx, false)
	a6 := args{nil, &a6lex.Value[0]}
	//a7
	a7lex := lex.Lex("1.txt", []string{"b(2,a()+a())"}, errcode.DefaultErrCtx, false)
	a7 := args{nil, &a7lex.Value[0]}
	tests := []struct {
		name string
		s    *stack
		args args
		want []Expr
	}{
		{"无参函数调用", newstack(nil, false), a1, nil},
		{"整数参数", newstack(nil, false), a2, []Expr{NewObject(INTOBJ, "1")}},
		{"两个整数参数", newstack(nil, false), a3, []Expr{NewObject(INTOBJ, "1"), NewObject(INTOBJ, "1")}},
		{"函数调用表达式做参数", newstack(nil, false), a4, []Expr{NewCallExpr(NewObject(SymbolObj, "a"), false)}},
		{"两个函数调用表达式做参数", newstack(nil, false), a5, []Expr{NewCallExpr(NewObject(SymbolObj, "a"), false), NewCallExpr(NewObject(SymbolObj, "a"), false)}},
		{"运算表达式两个函数调用表达式做参数", newstack(nil, false), a6, []Expr{NewOpExpr(enum.ADDOP, NewCallExpr(NewObject(SymbolObj, "a"), false), NewCallExpr(NewObject(SymbolObj, "a"), false))}},
		{"数字和运算表达式两个函数调用表达式做参数", newstack(nil, false), a7, []Expr{NewObject(INTOBJ, "2"), NewOpExpr(enum.ADDOP, NewCallExpr(NewObject(SymbolObj, "a"), false), NewCallExpr(NewObject(SymbolObj, "a"), false))}},
	}
	for _, tt := range tests {
		b.Run(tt.name, func(b *testing.B) {
			b.ReportAllocs()
			b.SetBytes(2)
			for i := 0; i < b.N; i++ {
				tt.s.AutoParameList(tt.args.t, tt.args.line)
				tt.s = newstack(nil, false)
			}
		})
	}
}
