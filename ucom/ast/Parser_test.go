package ast

import (
	"reflect"
	"testing"

	"gitee.com/u-language/u-language/ucom/internal/teco"
	"gitee.com/u-language/u-language/ucom/lex"
)

func Test_splitFor(t *testing.T) {
	type args struct {
		line *lex.Line
	}
	newargs := func(lines []string) args {
		ft := lex.Lex("1.txt", lines, nil, false)
		return args{line: &ft.Value[0]}
	}
	a1 := newargs([]string{"for var i int =0;i<100;i=i+1{"})
	a1want1 := lex.NewLine(1, []lex.Token{teco.LexVar, lex.NewToken(lex.NAME, "i"), teco.LexInt, teco.LexAssign, lex.NewToken(lex.NUMBER, "0")})
	a1want2 := lex.NewLine(1, []lex.Token{lex.NewToken(lex.NAME, "i"), teco.LexLess, lex.NewToken(lex.NUMBER, "100")})
	a1want3 := lex.NewLine(1, []lex.Token{lex.NewToken(lex.NAME, "i"), teco.LexAssign, lex.NewToken(lex.NAME, "i"), teco.LexAdd, lex.NewToken(lex.NUMBER, "1")})
	a2 := newargs([]string{"for ;;{"})
	a3 := newargs([]string{"for ;i<100;{"})
	a3want := lex.NewLine(1, []lex.Token{lex.NewToken(lex.NAME, "i"), teco.LexLess, lex.NewToken(lex.NUMBER, "100")})
	tests := []struct {
		name  string
		args  args
		want  []lex.Line
		want1 int
	}{
		{"正常3子句", a1, []lex.Line{a1want1, a1want2, a1want3}, 2},
		{"全空子句", a2, []lex.Line{teco.LexEmptyLine, teco.LexEmptyLine, teco.LexEmptyLine}, 2},
		{"布尔表达式非空", a3, []lex.Line{teco.LexEmptyLine, a3want, teco.LexEmptyLine}, 2},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, got1 := splitFor(tt.args.line)
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("splitFor() got = %v, want %v", got, tt.want)
			}
			if got1 != tt.want1 {
				t.Errorf("splitFor() got1 = %v, want %v", got1, tt.want1)
			}
		})
	}
}
