package ast

import (
	"strings"
	"testing"
)

func Test_splitElemAndLen(t *testing.T) {
	type args struct {
		elem *strings.Builder
		Len  *strings.Builder
		typ  string
	}
	a1 := args{new(strings.Builder), new(strings.Builder), "[2]int"}
	a2 := args{new(strings.Builder), new(strings.Builder), "[2][2]int"}
	a3 := args{new(strings.Builder), new(strings.Builder), "[2][2][2]int"}
	tests := []struct {
		name string
		args args
		Elem string
		Len  string
	}{
		{"[2]int", a1, "int", "[2]"},
		{"[2][2]int", a2, "int", "[2][2]"},
		{"[2][2][2]int", a3, "int", "[2][2][2]"},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			splitElemAndLen(tt.args.elem, tt.args.Len, tt.args.typ)
			if tt.args.elem.String() != tt.Elem {
				t.Fatalf("elem = %s \n want = %s", tt.args.elem.String(), tt.Elem)
			}
			if tt.args.Len.String() != tt.Len {
				t.Fatalf("len = %s \n want = %s", tt.args.Len.String(), tt.Len)
			}
		})
	}
}
