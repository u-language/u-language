package ast

import (
	"context"
	"reflect"
	"testing"

	"gitee.com/u-language/u-language/ucom/astdata"
	"gitee.com/u-language/u-language/ucom/errcode"
	"gitee.com/u-language/u-language/ucom/lex"
)

func Test_check_func_Parame(t *testing.T) {
	type args struct {
		line *lex.Line
	}
	a1 := lex.Lex("1.txt", []string{"package main", "func a (a int){"}, nil, false).Value[1]
	a2 := lex.Lex("1.txt", []string{"package main", "func a (a int){"}, nil, false).Value[1]
	a3 := lex.Lex("1.txt", []string{"package main", "func a (a int,b int){"}, nil, false).Value[1]
	a4 := lex.Lex("1.txt", []string{"package main", "func a (a int,b int,c int){"}, nil, false).Value[1]
	a5 := lex.Lex("1.txt", []string{"package main", "func a (a int)int{"}, nil, false).Value[1]
	a6 := lex.Lex("1.txt", []string{"package main", "func a (a &int){"}, nil, false).Value[1]
	a7 := lex.Lex("1.txt", []string{"package main", "func a (a){"}, nil, false).Value[1]
	a8 := lex.Lex("1.txt", []string{"package main", "func a (a int,,){"}, nil, false).Value[1]
	a9 := lex.Lex("1.txt", []string{"package main", "func a (a ,b int){"}, nil, false).Value[1]
	tests := []struct {
		name  string
		args  args
		want  []astdata.Parame
		want1 []astdata.RetValue
		want2 errcode.ErrCode
	}{
		{"单个参数", args{line: &a1}, []astdata.Parame{astdata.NewNameAndType("a", TypeInt)}, []astdata.RetValue{}, errcode.NoErr},
		{"单个参数", args{line: &a2}, []astdata.Parame{astdata.NewNameAndType("a", TypeInt)}, []astdata.RetValue{}, errcode.NoErr},
		{"两个参数", args{line: &a3}, []astdata.Parame{astdata.NewNameAndType("a", TypeInt), astdata.NewNameAndType("b", TypeInt)}, []astdata.RetValue{}, errcode.NoErr},
		{"三个参数", args{line: &a4}, []astdata.Parame{astdata.NewNameAndType("a", TypeInt), astdata.NewNameAndType("b", TypeInt), astdata.NewNameAndType("c", TypeInt)}, []astdata.RetValue{}, errcode.NoErr},
		{"单个参数和返回值", args{line: &a5}, []astdata.Parame{astdata.NewNameAndType("a", TypeInt)}, []astdata.RetValue{astdata.NewNameAndType("", TypeInt)}, errcode.NoErr},
		{"指针参数", args{line: &a6}, []astdata.Parame{astdata.NewNameAndType("a", NewObject(LeaObj|TypeObj, "&int"))}, []astdata.RetValue{}, errcode.NoErr},
		{"没有类型", args{line: &a7}, nil, nil, errcode.ParameDeclHasNoType},
		{"逗号过多", args{line: &a8}, nil, nil, errcode.UnexpectedComma},
		{"没有类型和有类型", args{line: &a9}, nil, nil, errcode.ParameDeclHasNoType},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, got1, got2 := check_func_Parame(nil, tt.args.line, errcode.FUNCErr, nil)
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("check_func_Parame() got = %v, want %v", got, tt.want)
			}
			if !reflect.DeepEqual(got1, tt.want1) {
				t.Errorf("check_func_Parame() got1 = %v, want %v", got1, tt.want1)
			}
			if !reflect.DeepEqual(got2, tt.want2) {
				t.Errorf("check_func_Parame() got2 = %v, want %v", got2, tt.want2)
			}
		})
	}
}

var TypeInt = NewObject(SymbolObj|TypeObj, "int")

func Test_check_from_list(t *testing.T) {
	type args struct {
		list string
	}
	a1 := args{"T"}
	a2 := args{"T,S"}
	a3 := args{"s[T,S]"}
	a4 := args{"s[T,S],T"}
	tests := []struct {
		name string
		args args
		want []string
	}{
		{"一般泛型", a1, []string{"T"}},
		{"多个类型参数的泛型", a2, []string{"T", "S"}},
		{"泛型实例化当类型参数", a3, []string{"s[T,S]"}},
		{"混合类型参数", a4, []string{"s[T,S]", "T"}},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := check_from_list(tt.args.list); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("check_from_list() = %v, want %v", got, tt.want)
			}
		})
	}
}

func init() {
	NdSotfOn = true
	go errcode.Handle(context.Background(), func(err errcode.ErrInfo) { panic(err.String()) }, func() {})
}
