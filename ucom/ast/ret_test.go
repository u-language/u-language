package ast_test

import (
	"testing"

	. "gitee.com/u-language/u-language/ucom/ast"
)

func TestRetType(t *testing.T) {
	type args struct {
		ptr *Object
		sbt *Sbt
	}
	a3 := args{ptr: NewObject(SymbolObj, "b")}
	a3.sbt = NewSbt(true)
	a3.sbt.AddVar("b", NewObject(TypeObj, "int"), false, 0, "")
	a4 := args{ptr: NewObject(SymbolObj, "b")}
	a4.sbt = NewSbt(true)
	a4.sbt.AddVar("b", NewObject(TypeObj, "float"), false, 0, "")
	tests := []struct {
		name string
		args args
		want string
	}{
		{"int类型", args{NewObject(INTOBJ, "1"), NewSbt(true)}, "int"},
		{"float类型", args{NewObject(FLOATOBJ, "1.9"), NewSbt(true)}, "float"},
		{"int型变量", a3, "int"},
		{"float型变量", a4, "float"},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := RetType(tt.args.ptr, tt.args.sbt); got != tt.want {
				t.Errorf("RetType() = %v, want %v", got, tt.want)
			}
		})
	}
}
