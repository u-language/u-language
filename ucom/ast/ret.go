package ast

import (
	"fmt"

	"gitee.com/u-language/u-language/ucom/enum"
	"gitee.com/u-language/u-language/ucom/internal/errutil"
)

// RetType获取Object的类型，对于未知的，将panic
//   - ptr是被获取类型的的Object,不能为nil
//   - sbt是Object所在区块的符号表,不能为nil
func RetType(ptr *Object, sbt *Sbt) string {
	switch ptr.Kind {
	case INTOBJ: //如果是int
		return enum.Int
	case FLOATOBJ: //如果是float
		return enum.Float
	case BoolObj: //如果是bool
		return enum.Bool
	case StringObj: //如果是字符串
		return enum.String
	case SymbolObj, StructPtr | SymbolObj: //如果是符号
		vv := sbt.Have(ptr.Name)
		return vv.Type()
	case NilObj: //如果是指针的零值
		return enum.Nil
	case LeaObj: //如果是取地址
		_, ok := TypeEnumStrMap[ptr.Name]
		if ok {
			return "&" + ptr.Name
		}
		vv := sbt.Have(ptr.Name)
		return "&" + vv.Type()
	case DerefObj: //如果是解引用
		vv := sbt.Have(ptr.Name)
		return vv.Type()[1:]
	}
	panic(errutil.NewErr2(errutil.CompileErr, fmt.Sprintf("未知的类型 ptr=%+v \n sbt=%+v", ptr, sbt)))
}
