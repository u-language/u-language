package ast

import (
	"gitee.com/u-language/u-language/ucom/lex"
)

// braceStack 用于检查左右大括号是否匹配
type braceStack struct {
	leftRightMatch
}

func newbraceStack(left int) braceStack {
	ret := braceStack{}
	ret.leftNum = left
	return ret
}

// AutoAdd 自动记录末尾的左大括号或右大括号
func (b *braceStack) AutoAdd(line []lex.Token) bool {
	if line[len(line)-1].TYPE == lex.LBRACE { //是左大括号
		b.leftNum++ //更新左大括号数量
	}
	if line[len(line)-1].TYPE == lex.RBRACE { //是右大括号
		b.rightNum++ //更新右大括号数量
	}
	return b.leftNum == b.rightNum //是否数量相等
}

// 缩写： Left and right matching 缩写成 leftRightMatch
// leftRightMatch 表示一组检查左右数量是否匹配
type leftRightMatch struct {
	// 左数量
	leftNum int
	// 右数量
	rightNum int
}

// Ok 返回左数量是否等于右数量
func (l *leftRightMatch) Ok() bool {
	return l.leftNum == l.rightNum
}
