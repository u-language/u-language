package ast_test

import (
	"context"
	"fmt"
	"testing"

	"gitee.com/u-language/u-language/ucom/errcode"
	"gitee.com/u-language/u-language/ucom/internal/parser"
	"gitee.com/u-language/u-language/ucom/internal/utils"
)

func TestCheckSyntaxErr(t *testing.T) {
	type args struct {
		str    []string
		errctx *errcode.ErrCtx
		cancel context.CancelFunc
	}
	newargs := func(str []string, errf func(errcode.ErrInfo), num int, name string) args {
		actx := errcode.NewErrCtx()
		astdctx, acancel := context.WithCancel(context.Background())
		i := 0
		s := make(chan struct{})
		go actx.Handle(astdctx, func(info errcode.ErrInfo) { //每收到一个错误，i++
			i++
			errf(info)
		}, func() { s <- struct{}{} })
		go func() {
			<-astdctx.Done()
			if num != 0 { //如果预期发生错误数量大于0，等待处理所有错误完毕
				<-s
			}
			if num != i { //如果预期发生错误数量不等于实际发生错误数量
				panic(fmt.Errorf("%s 应该有%d个错误，实际有%d个错误", name, num, i))
			}
		}()
		ret := args{str: str, errctx: actx, cancel: acancel}
		return ret
	}
	a1 := newargs([]string{"", "func main(){", "}"}, func(err errcode.ErrInfo) {
		if !err.IsErr("1.txt", 1, nil, errcode.PackageDeclMustFirstLine) {
			panic("a1\n" + err.String())
		}
	}, 1, "a1")
	a2 := newargs([]string{"package main", "func main(){", "var ", "var a", "}"}, func(err errcode.ErrInfo) {
		if !err.IsErr("1.txt", 3, nil, errcode.VARErr, errcode.NoName) && !err.IsErr("1.txt", 4, nil, errcode.VARErr, errcode.NoType) {
			panic("a2\n" + err.String())
		}
	}, 2, "a2")
	a3 := newargs([]string{"package main", "func main(){", "const ", "const a", "const a int", "}"}, func(err errcode.ErrInfo) {
		if !err.IsErr("1.txt", 3, nil, errcode.ConstErr, errcode.NoName) && !err.IsErr("1.txt", 4, nil, errcode.ConstErr, errcode.NoType) && !err.IsErr("1.txt", 5, nil, errcode.ConstErr, errcode.NoConstInit) {
			panic("a3\n" + err.String())
		}
	}, 3, "a3")
	a4 := newargs([]string{"package main", "func main(){", "1:", "t : t", "}"}, func(err errcode.ErrInfo) {
		if !err.IsErr("1.txt", 3, nil, errcode.LabelErr, errcode.NoName) && !err.IsErr("1.txt", 4, nil, errcode.LabelErr, errcode.OPbug) {
			panic("a4\n" + err.String())
		}
	}, 2, "a4")
	a5 := newargs([]string{"package main", "func main(){", "goto ", "goto 1", "goto e e", "}"}, func(err errcode.ErrInfo) {
		if !err.IsErr("1.txt", 3, nil, errcode.GotoErr, errcode.NoLabel) && !err.IsErr("1.txt", 4, nil, errcode.GotoErr, errcode.NoLabel) && !err.IsErr("1.txt", 5, nil, errcode.GotoErr, errcode.OPbug) {
			panic("a5\n" + err.String())
		}
	}, 3, "a5")
	a6 := newargs([]string{"package main", "func {", "}", "func a {", "}", "func a(o int)", "}", "func a o int){", "}", "func a(o int{", "}"}, func(err errcode.ErrInfo) {
		if !err.IsErr("1.txt", 2, nil, errcode.FUNCErr, errcode.NoName) && !err.IsErr("1.txt", 4, nil, errcode.FUNCErr, errcode.EmptyLPAREN) && !err.IsErr("1.txt", 6, nil, errcode.FUNCErr, errcode.LbraceEndOfLine) && !err.IsErr("1.txt", 8, nil, errcode.FUNCErr, errcode.EmptyLPAREN) && !err.IsErr("1.txt", 10, nil, errcode.FUNCErr, errcode.EmptyRPAREN) {
			panic("a6\n" + err.String())
		}
	}, 5, "a6")
	a7 := newargs([]string{"package main", "func s[i](){", "}", "func s[i 3](){", "}"}, func(err errcode.ErrInfo) {
		if !err.IsErr("1.txt", 2, nil, errcode.TypeParameSyntaxErr) && !err.IsErr("1.txt", 4, nil, errcode.FUNCErr, errcode.UnknownType) && !err.IsErr("1.txt", 4, nil, errcode.TypeParamrConstraintErr) {
			panic("a7\n" + err.String())
		}
	}, 3, "a7")
	a8 := newargs([]string{"func main(){", "}"}, func(err errcode.ErrInfo) {
		if !err.IsErr("1.txt", 1, nil, errcode.PackageDeclMustFirstLine) {
			panic("a8\n" + err.String())
		}
	}, 1, "a8")
	a9 := newargs([]string{"package main", "func main(){", "}", "enum {", "}", "enum j{", "}", "enum k{", "", "1", "q", "q", "f g", "}", "enum s{"}, func(err errcode.ErrInfo) {
		if !err.IsErr("1.txt", 4, nil, errcode.EnumErr, errcode.NoName) && !err.IsErr("1.txt", 7, nil, errcode.EnumErr, errcode.EmptyEnum) && !err.IsErr("1.txt", 10, nil, errcode.EnumErr, errcode.EnumValueShouldBeASymbol) && !err.IsErr("1.txt", 12, errcode.NewMsgSymbol("q"), errcode.EnumErr, errcode.EnumValueDup) && !err.IsErr("1.txt", 13, nil, errcode.EnumErr, errcode.EnumValueShouldHaveOneOnEachLine) && !err.IsErr("1.txt", 15, nil, errcode.EnumErr, errcode.EmptyRbrace) {
			panic("a9\n" + err.String())
		}
	}, 6, "a9")
	a10 := newargs([]string{"package main", "func main(){", "}", "struct {", "struct a{", "", "9 int", "s []p", "}"}, func(err errcode.ErrInfo) {
		if !err.IsErr("1.txt", 4, nil, errcode.StructErr, errcode.NoName) && !err.IsErr("1.txt", 5, nil, errcode.StructErr, errcode.LbraceEndOfLine) && !err.IsErr("1.txt", 7, nil, errcode.StructErr, errcode.FieldErr, errcode.NoIdent) && !err.IsErr("1.txt", 8, nil, errcode.StructErr, errcode.UnknownType) {
			panic("a10\n" + err.String())
		}
	}, 3, "a10")
	a11 := newargs([]string{"package main", "func main(){", "if ", "if {", "}", "if a.{", "}", "}"}, func(err errcode.ErrInfo) {
		if !err.IsErr("1.txt", 3, nil, errcode.IfErr, errcode.LbraceEndOfLine) && !err.IsErr("1.txt", 4, nil, errcode.IfErr, errcode.NoBoolExpr) && !err.IsErr("1.txt", 6, nil, errcode.LeftOrRightValueSelectorEmpty) {
			panic("a11\n" + err.String())
		}
	}, 3, "a11")
	a12 := newargs([]string{"package main", "func main(){", "else if ", "else if a.{", "}", "}"}, func(err errcode.ErrInfo) {
		if !err.IsErr("1.txt", 3, nil, errcode.ElseErr, errcode.LbraceEndOfLine) && !err.IsErr("1.txt", 4, nil, errcode.LeftOrRightValueSelectorEmpty) {
			panic("a12\n" + err.String())
		}
	}, 2, "a12")
	a13 := newargs([]string{"package main", "func main(){", "for ", "for {", "}", "for ;{", "}", "}"}, func(err errcode.ErrInfo) {
		if !err.IsErr("1.txt", 3, nil, errcode.ForErr, errcode.LbraceEndOfLine) && !err.IsErr("1.txt", 4, errcode.NewMsgNumberOfSemicolons(0), errcode.ForErr) && !err.IsErr("1.txt", 6, errcode.NewMsgNumberOfSemicolons(1), errcode.ForErr) {
			panic("a13\n" + err.String())
		}
	}, 3, "a13")
	a14 := newargs([]string{"package main", "func main(){", "main()", "init()", "}"}, func(err errcode.ErrInfo) {
		if !err.IsErr("1.txt", 3, nil, errcode.ProhibitCallMainFunc) && !err.IsErr("1.txt", 4, nil, errcode.ProhibitCallInitFunc) {
			panic("a14\n" + err.String())
		}
	}, 2, "a14")
	a15 := newargs([]string{"package main", "func main(){", "switch {", "}", "switch", "}"}, func(err errcode.ErrInfo) {
		if !err.IsErr("1.txt", 3, nil, errcode.SwitchErr, errcode.NoExpr) && !err.IsErr("1.txt", 5, nil, errcode.SwitchErr, errcode.LbraceEndOfLine) {
			panic("a15\n" + err.String())
		}
	}, 2, "a15")
	a16 := newargs([]string{"package main", "func main(){", "switch 1{", "case", "case:", "}", "}"}, func(err errcode.ErrInfo) {
		if !err.IsErr("1.txt", 4, nil, errcode.CaseErr, errcode.ExpectColonAtEnd) && !err.IsErr("1.txt", 5, nil, errcode.CaseErr, errcode.NoExpr) {
			panic("a16\n" + err.String())
		}
	}, 2, "a16")
	a17 := newargs([]string{"package main", "func main(){", "switch 1{", "default", "default 6:", "}", "}"}, func(err errcode.ErrInfo) {
		if !err.IsErr("1.txt", 4, nil, errcode.DefaultErr, errcode.ExpectColonAtEnd) && !err.IsErr("1.txt", 5, nil, errcode.DefaultErr, errcode.OPbug) {
			panic("a17\n" + err.String())
		}
	}, 2, "a17")
	a18 := newargs([]string{"package main", "func main(){", "}", "method", "method {", "method a {", "method a ({", "method a (){", "method a (i int){", "}"}, func(err errcode.ErrInfo) {
		if !err.IsErr("1.txt", 4, nil, errcode.MethodErr, errcode.LbraceEndOfLine) && !err.IsErr("1.txt", 5, nil, errcode.MethodErr, errcode.NoName) && !err.IsErr("1.txt", 6, nil, errcode.MethodErr, errcode.EmptyLPAREN) && !err.IsErr("1.txt", 7, nil, errcode.MethodErr, errcode.EmptyRPAREN) && !err.IsErr("1.txt", 8, nil, errcode.MethodErr, errcode.FirstParameTypeOfMethodACustomType) && !err.IsErr("1.txt", 9, nil, errcode.MethodErr, errcode.FirstParameTypeOfMethodACustomType) {
			panic("a18\n" + err.String())
		}
	}, 6, "a18")
	a19 := newargs([]string{"package main", "import {", "}", "func main(){", "import", "}", "import ", "import {", "1", "}"}, func(err errcode.ErrInfo) {
		if !err.IsErr("1.txt", 5, nil, errcode.ImportErr, errcode.ImportShouldBeOutSideOfTheFunc) && !err.IsErr("1.txt", 7, nil, errcode.ImportErr, errcode.LbraceEndOfLine) && !err.IsErr("1.txt", 9, nil, errcode.ImportErr, errcode.ImportPathMustString) {
			panic("a19\n" + err.String())
		}
	}, 3, "a19")
	a20 := newargs([]string{"package main", "import {"}, func(err errcode.ErrInfo) {
		if !err.IsErr("1.txt", 2, nil, errcode.ImportErr, errcode.EmptyRbrace) {
			panic("a20\n" + err.String())
		}
	}, 1, "a20")
	a21 := newargs([]string{"package main", "func main(){", "switch i {", "case 100 :", "if i > 50 {", "i++", "}", "if i > 90", " {", "i++", "printf(i)", "default :", "}", "}", "}"}, func(err errcode.ErrInfo) {
		if !err.IsErr("1.txt", 4, errcode.NewMsgBracesNoEqual(2, 1), errcode.CaseErr) {
			panic("a21\n" + err.String())
		}
	}, 1, "a21")
	a22 := newargs([]string{"package main", "func main(){", "switch i {", "default :", "if i > 50 {", "i++", "}", "if i > 90", " {", "i++", "printf(i)", "case 78 :", "}", "}", "}"}, func(err errcode.ErrInfo) {
		if !err.IsErr("1.txt", 4, errcode.NewMsgBracesNoEqual(2, 1), errcode.DefaultErr) {
			panic("a22\n" + err.String())
		}
	}, 1, "a22")
	a23 := newargs([]string{"package ", "package unsafe", "package f f", "package main"}, func(err errcode.ErrInfo) {
		if !err.IsErr("1.txt", 1, nil, errcode.PackageDeclMustName) && !err.IsErr("1.txt", 2, nil, errcode.PackageNameCannotBeEqualUnsafe) && !err.IsErr("1.txt", 3, nil, errcode.PackageDeclOnlyName) && !err.IsErr("1.txt", 4, nil, errcode.PackageDeclMustFirstLine) {
			panic("a23\n" + err.String())
		}
	}, 4, "a23")
	a24 := newargs([]string{"package main", "func main(){", "autofree {", "}", "}"}, func(err errcode.ErrInfo) {
		if !err.IsErr("1.txt", 3, nil, errcode.AutoFreeErr, errcode.NoExpr) {
			panic("a24\n" + err.String())
		}
	}, 1, "a24")
	a25 := newargs([]string{"package main", "struct s[T any]{", "}", "func main(){", "var b s[]", "var g s[1]", "var f s[int,int] ", "}"}, func(err errcode.ErrInfo) {
		if !err.IsErr("1.txt", 5, nil, errcode.VARErr, errcode.GenericInstantiationEmpty) && !err.IsErr("1.txt", 6, nil, errcode.VARErr, errcode.UnknownType) && !err.IsErr("1.txt", 7, nil, errcode.UnequlaNumOfGen) {
			panic("a25\n" + err.String())
		}
	}, 3, "a25")
	a26 := newargs([]string{"package main", "/*", "*/", "/*"}, func(err errcode.ErrInfo) {
		if !err.IsErr("1.txt", 4, nil, errcode.MLCNoEnd) {
			panic("a26\n" + err.String())
		}
	}, 1, "a26")
	a27 := newargs([]string{"package main", ",(0000"}, func(err errcode.ErrInfo) {
		if !err.IsErr("1.txt", 2, nil, errcode.OPbug) {
			panic("a27\n" + err.String())
		}
	}, 1, "a27")
	tests := []struct {
		name string
		args args
	}{
		{"第一行空，没有包声明", a1},
		{"变量错误", a2},
		{"常量错误", a3},
		{"标签错误", a4},
		{"goto错误", a5},
		{"函数错误", a6},
		{"泛型错误", a7},
		{"第一行非空，没有包声明", a8},
		{"枚举错误", a9},
		{"结构体错误", a10},
		{"if错误", a11},
		{"else if错误", a12},
		{"for错误", a13},
		{"禁止调用函数错误", a14},
		{"switch错误", a15},
		{"case错误", a16},
		{"default错误", a17},
		{"方法错误", a18},
		{"import错误", a19},
		{"import右大括号缺失", a20},
		{"case中的代码块错误", a21},
		{"default中的代码块错误", a22},
		{"package错误", a23},
		{"自动释放块错误", a24},
		{"泛型错误", a25},
		{"多行注释错误", a26},
		{"函数调用错误", a27},
	}
	for _, ttf := range tests {
		tt := ttf
		t.Run(tt.name, func(t *testing.T) {
			t.Parallel()
			parser.ParserStr(tt.args.str, tt.args.errctx)
			tt.args.cancel()
		})
	}
}

func init() {
	utils.Debug = true
	errcode.Debug = true
	go errcode.Handle(context.Background(), func(err errcode.ErrInfo) { panic(err.String()) }, func() {})
}
