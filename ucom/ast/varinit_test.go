package ast_test

import (
	"reflect"
	"testing"

	. "gitee.com/u-language/u-language/ucom/ast"
	"gitee.com/u-language/u-language/ucom/enum"
	"gitee.com/u-language/u-language/ucom/internal/parser"
)

func TestVarInitTable_InitOrder(t *testing.T) {
	tree := parser.ParserStr([]string{"package main", "var a int=10", "var b int=c+10", "var c int=8", "func main(){", "}"})
	want := []ASSIGNInfo{
		{NewASSIGNNode(NewObject(SymbolObj, "main__a"), NewObject(INTOBJ, "10")), 2},
		{NewASSIGNNode(NewObject(SymbolObj, "main__b"), NewOpExpr(enum.ADDOP, NewObject(SymbolObj, "main__c"), NewObject(INTOBJ, "10"))), 3},
		{NewASSIGNNode(NewObject(SymbolObj, "main__c"), NewObject(INTOBJ, "8")), 4},
	}
	if got := tree.VarInitTable_File.InitOrder(); !reflect.DeepEqual(got, want) {
		t.Errorf("VarInitTable.InitOrder() = %v, want %v", got, want)
	}
}
