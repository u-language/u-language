package ast

import (
	"sort"
	"sync"

	"gitee.com/u-language/u-language/ucom/data"
)

var _ sort.Interface = (*VarInitTable)(nil)

// VarInitTable 变量初始化表
// 实现了 [sort.Interface] 接口
type VarInitTable struct {
	data.Slice[ASSIGNInfo]
	once sync.Once
}

// NewVarInitTable 创建变量初始化表
func NewVarInitTable(Thread bool) VarInitTable {
	ret := VarInitTable{}
	ret.Thread = Thread
	return ret
}

func (t *VarInitTable) Len() int {
	return len(t.Data)
}

func (t *VarInitTable) Less(i, j int) bool {
	return t.Data[i].LineNum < t.Data[j].LineNum
}

func (t *VarInitTable) Swap(i, j int) {
	t.Data[i], t.Data[j] = t.Data[j], t.Data[i]
}

// InitOrder 返回按行升序排列的变量初始化信息
func (t *VarInitTable) InitOrder() []ASSIGNInfo {
	t.once.Do(func() {
		sort.Sort(t)
	})
	return t.Data
}

type ASSIGNInfo struct {
	Ptr     *ASSIGNNode
	LineNum int
}
