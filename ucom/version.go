package main

import (
	"runtime/debug"
	"time"

	"github.com/spf13/cobra"
)

// Version 表示当前的版本号
var Version = "v0.16.0"

// VersionTime 表示当前版本在git的提交时间
var VersionTime = time.Date(2023, 8, 15, 17, 39, 0, 0, loc)

var loc = time.FixedZone("北京时间", 0)

var (
	versionCmd = &cobra.Command{
		Use:   "version",
		Short: "打印版本信息",
		Long:  "打印版本信息",
		Run: func(cmd *cobra.Command, args []string) {
			info, ok := debug.ReadBuildInfo()
			if !ok { //如果从二进制文件获取信息失败
				cmd.Println("版本：", Version, "非模块支持构建")
				return
			}
			var goos, goarch, times string
			for _, v := range info.Settings {
				switch v.Key {
				case "GOOS":
					goos = v.Value
				case "GOARCH":
					goarch = v.Value
				case "vcs.time":
					times = v.Value
				}
			}
			if t, err := time.ParseInLocation(time.RFC3339, times, loc); err == nil { //获取UTC时间
				toff := time.Date(t.Year(), t.Month(), t.Day(), t.Hour()+8, t.Minute(), t.Second(), t.Nanosecond(), loc) //转换为北京时间
				if toff.After(VersionTime) {
					Version = "tip"
				}
			}
			cmd.Println("版本：", Version)
			cmd.Printf("GOOS/GOARCH：%s / %s\n", goos, goarch)
			cmd.Printf("当前进程用 %s 的go编译器版本构建\n", info.GoVersion)
		},
	}
)
