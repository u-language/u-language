module gitee.com/u-language/u-language/ucom

go 1.20

require (
	github.com/huandu/go-clone v1.7.2
	github.com/spf13/cobra v1.8.0
	golang.org/x/sys v0.15.0
)

require (
	github.com/inconshreveable/mousetrap v1.1.0 // indirect
	github.com/spf13/pflag v1.0.5 // indirect
)
