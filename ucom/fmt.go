package main

import (
	"errors"
	"os"

	"gitee.com/u-language/u-language/ucom/errcode"
	"gitee.com/u-language/u-language/ucom/format"
	"gitee.com/u-language/u-language/ucom/internal/utils"

	"github.com/spf13/cobra"
)

var (
	fmtCmd = &cobra.Command{
		Use:   "fmt",
		Short: "格式化文件\n调用者确保被格式化的文件符合语法，如果被格式化的文件不符合语法，进行格式化会发生的行为未定义",
		Long:  "格式化文件\n调用者确保被格式化的文件符合语法，如果被格式化的文件不符合语法，进行格式化会发生的行为未定义",
		Run: func(cmd *cobra.Command, args []string) {
			defer Recover()
			if len(args) < 1 {
				cmd.PrintErrln("格式化文件为空，退出")
				return
			}
			for _, v := range args {
				info, err := os.Stat(v)
				if err != nil {
					cmd.PrintErrf("获取 %s 的信息失败，错误=%s", v, err.Error())
					continue
				}
				var paths []string
				if info.IsDir() { //如果是目录，获取目录下所有.u文件
					paths, err = utils.FindU(v)
					if err != nil {
						cmd.PrintErrf("查找目录 %s 的.u文件出错,错误=%s", v, err.Error())
						return
					}
				} else {
					paths = append(paths, v)
				}
				for _, file := range paths {
					err = format.FormatFile(file, errcode.DefaultErrCtx) //格式化单个文件
					if err != nil {
						switch {
						case errors.Is(err, format.ErrorAfterLex):
							cmd.PrintErrf("文件 %s 有错误\n", file)
							return
						case errors.Is(err, os.ErrNotExist):
							cmd.PrintErrf("文件 %s 不存在\n", file)
							return
						case errors.Is(err, format.KeyNoMatchRightBrace):
							cmd.PrintErrf("%s\n", err.Error())
							return
						default:
							cmd.PrintErrf("有错误，错误=%v\n", err)
						}
					}
				}
			}
		},
	}
)
