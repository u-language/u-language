package check

import (
	"testing"
)

func TestIsExportSymbol(t *testing.T) {
	type args struct {
		s string
	}
	tests := []struct {
		name string
		args args
		want bool
	}{
		{"a", args{"a"}, false},
		{"A", args{"A"}, true},
		{"一", args{"一"}, true},
		{"_一", args{"_一"}, false},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := IsExportSymbol(tt.args.s); got != tt.want {
				t.Errorf("IsExportSymbol() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_index(t *testing.T) {
	type args struct {
		s []string
		v string
	}
	tests := []struct {
		name string
		args args
		want int
	}{
		{"无包分隔", args{[]string{"a_s", "a_g"}, "g"}, 1},
		{"没有", args{[]string{"a_s", "a_g"}, "2"}, -1},
		{"正常包枚举值", args{[]string{"main__s", "main__g"}, "g"}, 1},
		{"有两个下划线包枚举值", args{[]string{"main__s__s", "main__g__g"}, "g"}, 1},
		{"有下划线包枚举值", args{[]string{"main__s_s", "main__g_g"}, "g"}, 1},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := index(tt.args.s, tt.args.v); got != tt.want {
				t.Errorf("index() = %v, want %v", got, tt.want)
			}
		})
	}
}
