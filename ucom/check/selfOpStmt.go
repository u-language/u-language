package check

import (
	"gitee.com/u-language/u-language/ucom/ast"
	"gitee.com/u-language/u-language/ucom/enum"
	"gitee.com/u-language/u-language/ucom/errcode"
	"gitee.com/u-language/u-language/ucom/internal/utils"
)

// checkSelfOpStmtCtx 检查自操作语句节点类型是否匹配
//   - ptr是被检查的自操作语句节点，不能为nil
//   - sbt是被检查的自操作语句节点所属代码块的符号表，不能为nil
//   - t是被检查节点所属的抽象语法树
//   - InAutoFree指示是否在自动释放块内
func checkSelfOpStmtCtx(ptr *ast.SelfOpStmt, sbt *ast.Sbt, t *ast.Tree, InAutoFree bool) (code errcode.ErrCode, msg errcode.Msg) {
	defer check_recover(&code, &msg)
	var info ast.SymbolInfo
	var typ string
	code, msg, info, _, typ = ret_info(ptr.Dest, sbt, t, InAutoFree)
	if code != errcode.NoErr {
		return code, msg
	}
	if info.Kind == enum.SymbolConst { //如果对常量赋值
		return errcode.ASSIGNErr, errcode.NewMsgAssignToConst(info.Name())
	}
	if utils.IsPtr(typ) {
		return errcode.PtrOpOnlyCmp, nil
	}
	//自操作语句只有加或减
	switch typ {
	case enum.String: //如果操作数类型是字符串
		return errcode.StringNoSelfOpStmt, nil
	case enum.Bool: //如果操作数类型是布尔值
		return errcode.BoolOpOnlyCmpOrLogic, nil
	}
	return errcode.NoErr, nil
}
