package check_test

import (
	"context"
	"fmt"
	"testing"

	"gitee.com/u-language/u-language/ucom/ast"
	. "gitee.com/u-language/u-language/ucom/check"
	"gitee.com/u-language/u-language/ucom/errcode"
	"gitee.com/u-language/u-language/ucom/internal/parser"
	"gitee.com/u-language/u-language/ucom/internal/utils"
)

func TestCheckTree(t *testing.T) {
	type args struct {
		tree   *ast.Tree
		errctx *errcode.ErrCtx
		cancel context.CancelFunc
	}
	newargs := func(str []string, errf func(errcode.ErrInfo), num int, name string) args {
		actx := errcode.NewErrCtx()
		astdctx, acancel := context.WithCancel(context.Background())
		i := int(0)
		s := make(chan struct{})
		go actx.Handle(astdctx, func(info errcode.ErrInfo) { //每收到一个错误，i++
			i++
			errf(info)
		}, func() { s <- struct{}{} })
		go func() {
			<-astdctx.Done()
			if num != 0 { //如果预期发生错误数量大于0，等待处理所有错误完毕
				<-s
			}
			if num != i { //如果预期发生错误数量不等于实际发生错误数量
				panic(fmt.Errorf("%s 应该有%d个错误，实际有%d个错误", name, num, i))
			}
		}()
		ret := args{tree: parser.ParserStr(str, actx), errctx: actx, cancel: acancel}
		return ret
	}
	a1 := newargs([]string{"package main", "func main(){", "var b int=1", "var c int=1.0", "}"}, func(err errcode.ErrInfo) {
		if !err.IsErr("1.txt", 4, errcode.NewMsgAssignTypeIsNotEqual("int", "float"), errcode.TypeIsNotEqual) {
			panic("a1\n" + err.String())
		}
	}, 1, "a1")
	a3 := newargs([]string{"package main", "func main(){", "var b int=1+2+3", "}"}, func(err errcode.ErrInfo) { panic("a3\n" + err.String()) }, 0, "a3")
	a4 := newargs([]string{"package main", "func main(){", "var b int=1+2.0+3", "var c int=1+2*3.0+2", "}"}, func(err errcode.ErrInfo) {
		if !err.IsErr("1.txt", 3, errcode.NewMsgOpexprTypeIsNotEqual("int", "float"), errcode.TypeIsNotEqual) && !err.IsErr("1.txt", 4, errcode.NewMsgOpexprTypeIsNotEqual("int", "float"), errcode.TypeIsNotEqual) {
			panic("a4\n" + err.String())
		}
	}, 2, "a4")
	a5 := newargs([]string{"package main", "func main(){", "var b int=1.0+2.0+3.0", "b=1.9", "}"}, func(err errcode.ErrInfo) {
		if !err.IsErr("1.txt", 3, errcode.NewMsgAssignTypeIsNotEqual("int", "float"), errcode.TypeIsNotEqual) && !err.IsErr("1.txt", 4, errcode.NewMsgAssignTypeIsNotEqual("int", "float"), errcode.TypeIsNotEqual) {
			panic("a5\n" + err.String())
		}
	}, 2, "a5")
	a6 := newargs([]string{"package main", "func main(){", "b=1", "var b int", "}"}, func(err errcode.ErrInfo) {
		if !err.IsErr("1.txt", 3, errcode.NewMsgSymbol("b"), errcode.SymbolUseBeforeDeclaration) {
			panic("a6\n" + err.String())
		}
	}, 1, "a6")
	a7 := newargs([]string{"package main", "func main(){", "var c int=1", "c=c+b", "c=b+c", "var b int", "}"}, func(err errcode.ErrInfo) {
		if !err.IsErr("1.txt", 4, errcode.NewMsgSymbol("b"), errcode.SymbolUseBeforeDeclaration) && !err.IsErr("1.txt", 5, errcode.NewMsgSymbol("b"), errcode.SymbolUseBeforeDeclaration) {
			panic("a7\n" + err.String())
		}
	}, 2, "a7")
	a8 := newargs([]string{"package main", "func main(){", "var b int=1", "if b==1{", "}", "if b+b{", "}", "}"}, func(err errcode.ErrInfo) {
		if !err.IsErr("1.txt", 6, nil, errcode.ExprNoBool) {
			panic("a8\n" + err.String())
		}
	}, 1, "a8")
	a9 := newargs([]string{"package main", "func main(){", "var b int=1", "if b==1{", "}", "else if b+b{", "}", "}"}, func(err errcode.ErrInfo) {
		if !err.IsErr("1.txt", 6, nil, errcode.ExprNoBool) {
			panic("a9\n" + err.String())
		}
	}, 1, "a9")
	a10 := newargs([]string{"package main", "func main(){", "for var i int;i<1;i=i+1{", "}", "var i int", "for i=i+1;;{", "}", "for ;;var j int{", "}", "}"}, func(err errcode.ErrInfo) {
		if !err.IsErr("1.txt", 6, nil, errcode.ForInitNoDeclVarStmt) && !err.IsErr("1.txt", 8, nil, errcode.ForEndStmt) {
			panic("a10\n" + err.String())
		}
	}, 2, "a10")
	a11 := newargs([]string{"package main", "func main(){", "var i int =a", "}"}, func(err errcode.ErrInfo) {
		if !err.IsErr("1.txt", 3, errcode.NewMsgUnknownSymbol("a"), errcode.UnknownSymbol) {
			panic("a11\n" + err.String())
		}
	}, 1, "a11")
	a12 := newargs([]string{"package main", "func main(){", "var i bool =true+false", "}"}, func(err errcode.ErrInfo) {
		if !err.IsErr("1.txt", 3, nil, errcode.BoolOpOnlyCmpOrLogic) {
			panic("a12\n" + err.String())
		}
	}, 1, "a12")
	a13 := newargs([]string{"package main", "func add()int{", "return true", "}", "func main(){", "}"}, func(err errcode.ErrInfo) {
		if !err.IsErr("1.txt", 3, errcode.NewMsgReturnTypeIsNotEqual("int", "bool"), errcode.ReturnErr) {
			panic("a13\n" + err.String())
		}
	}, 1, "a13")
	a14 := newargs([]string{"package main", "func main(){", "r:", "goto re", "}"}, func(err errcode.ErrInfo) {
		if !err.IsErr("1.txt", 4, errcode.NewMsgLabelNoExist("re"), errcode.GotoErr) {
			panic("a14\n" + err.String())
		}
	}, 1, "a14")
	a15 := newargs([]string{"package main", "func main(){", "for ;;{", "if true==true{", "break", "}", "}", "break", "continue", "}"}, func(err errcode.ErrInfo) {
		if !err.IsErr("1.txt", 8, nil, errcode.BreakStmtInForOrSwitch) && !err.IsErr("1.txt", 9, nil, errcode.ContinueStmtInFor) {
			panic("a15\n" + err.String())
		}
	}, 2, "a15")
	a16 := newargs([]string{"package main", "func main(){", "const a int = i ", "a=1", "var i int=1", "}"}, func(err errcode.ErrInfo) {
		if !err.IsErr("1.txt", 4, errcode.NewMsgAssignToConst("a"), errcode.ASSIGNErr) && !err.IsErr("1.txt", 3, errcode.NewMsgSymbol("i"), errcode.SymbolUseBeforeDeclaration) {
			panic("a16\n" + err.String())
		}
	}, 2, "a16")
	a17 := newargs([]string{"package main", "func main(){", "var a float = float(10) ", "var b int=int(&a)", "var c int=int(a,1)", "var d int=int()", "var e int=int(f)", "}"}, func(err errcode.ErrInfo) {
		if !err.IsErr("1.txt", 4, nil, errcode.CanOnlyConvertPointerTypeToUnsafePointer) && !err.IsErr("1.txt", 5, errcode.NewMsgNumberOfParameNoMatch(2, 1, "int"), errcode.TManyParame) && !err.IsErr("1.txt", 6, errcode.NewMsgNumberOfParameNoMatch(0, 1, "int"), errcode.InsuParame) && !err.IsErr("1.txt", 7, errcode.NewMsgUnknownSymbol("f"), errcode.UnknownSymbol) {
			panic("a17\n" + err.String())
		}
	}, 4, "a17")
	a18 := newargs([]string{"package main", "func main(){", "printf()", "printf(1)", "}"}, func(err errcode.ErrInfo) {
		if !err.IsErr("1.txt", 3, nil, errcode.InsuParame) && !err.IsErr("1.txt", 4, nil, errcode.FirstParameOfPrinfASting) {
			panic("a18\n" + err.String())
		}
	}, 2, "a18")
	a19 := newargs([]string{"package main", "func main(){", "add(1,2,3)", "add(1)", "}", "func add(a int,b int){", "return a+b", "}"}, func(err errcode.ErrInfo) {
		if !err.IsErr("1.txt", 3, errcode.NewMsgNumberOfParameNoMatch(3, 2, "main__add"), errcode.TManyParame) && !err.IsErr("1.txt", 4, errcode.NewMsgNumberOfParameNoMatch(1, 2, "main__add"), errcode.InsuParame) {
			panic("a19\n" + err.String())
		}
	}, 2, "a19")
	a20 := newargs([]string{"package main", "func main(){", "var i int", "add(1.0,&i)", "add(1,&i)", "add(1,nil)", "malloc(1)", "}", "func add(a int,b &int){", "return a+@b", "}"}, func(err errcode.ErrInfo) {
		if !err.IsErr("1.txt", 4, errcode.NewMsgCallTypeIsNotEqual("int", "float", 1), errcode.TypeIsNotEqual) && !err.IsErr("1.txt", 7, errcode.NewMsgUnknownSymbol("1"), errcode.UnknownType) {
			panic("a20\n" + err.String())
		}
	}, 2, "a20")
	a21 := newargs([]string{"package main", "func main(){", "var s str", "}"}, func(err errcode.ErrInfo) {
		if !err.IsErr("1.txt", 3, errcode.NewMsgUnknownSymbol("str"), errcode.UnknownType) {
			panic("a21\n" + err.String())
		}
	}, 1, "a21")
	a22 := newargs([]string{"package main", "func main(){", "struct str{", "ptr &int", "len int", "next &str", "}", "var s str", "s.len=1", "s.ln=1", "var i int", "i.f=1", "var p1 &int=&s.len", "var p2 &float=&s.len", "s.next=&s", "s.len=s.next.len", "}"}, func(err errcode.ErrInfo) {
		if !err.IsErr("1.txt", 10, errcode.NewMsgSelectorLNoSelectorR("s", "ln"), errcode.SelectorLNoSelectorR) && !err.IsErr("1.txt", 12, errcode.NewMsgSymbol("i"), errcode.NoOSelectorL) && !err.IsErr("1.txt", 14, errcode.NewMsgAssignTypeIsNotEqual("&float", "&int"), errcode.TypeIsNotEqual) {
			panic("a22\n" + err.String())
		}
	}, 3, "a22")
	a23 := newargs([]string{"package main", "func main(){", "var s int", "s(1)", "s=value(s)", "}", "func value(i int){", "}"}, func(err errcode.ErrInfo) {
		if !err.IsErr("1.txt", 5, errcode.NewMsgSymbol("value"), errcode.FuncNoRetValue) && !err.IsErr("1.txt", 4, errcode.NewMsgSymbol("s"), errcode.CallNoFunc) {
			panic("a23\n" + err.String())
		}
	}, 2, "a23")
	a24 := newargs([]string{"package main", "func main(){", "var a int", "a++", "var b bool", "b++", "var s string", "s++", "const c int=1", "c--", "var e &int=&a", "e++", "for ;;e++{", "}", "}"}, func(err errcode.ErrInfo) {
		if !err.IsErr("1.txt", 6, nil, errcode.BoolOpOnlyCmpOrLogic) && !err.IsErr("1.txt", 8, nil, errcode.StringNoSelfOpStmt) && !err.IsErr("1.txt", 10, errcode.NewMsgAssignToConst("c"), errcode.ASSIGNErr) && !err.IsErr("1.txt", 12, nil, errcode.PtrOpOnlyCmp) && !err.IsErr("1.txt", 13, nil, errcode.PtrOpOnlyCmp) {
			panic("a24\n" + err.String())
		}
	}, 5, "a24")
	a25 := newargs([]string{"package main", "func init(){", "}", "func ginit(){", "}", "func gmain(){", "}", "func main(){", "init()", "main()", "ginit()", "gmain()", "}"}, func(err errcode.ErrInfo) {
		if !err.IsErr("1.txt", 9, nil, errcode.ProhibitCallInitFunc) && !err.IsErr("1.txt", 10, nil, errcode.ProhibitCallMainFunc) {
			panic("a25\n" + err.String())
		}
	}, 2, "a25")
	a26 := newargs([]string{"package main", "func main(){", "var i &int=malloc(int)", "free(i)", "printf(\"%d%d%d\",@i,1,2)", "mallocSize(i)", "unsafe__Convert(i,float)", "}"}, func(err errcode.ErrInfo) {
		if !err.IsErr("1.txt", 6, nil, errcode.ParameForMallocSizeMustInt) && !err.IsErr("1.txt", 7, nil, errcode.SecondParameOfUnsafeConvertAPtrType) {
			panic("a26\n" + err.String())
		}
	}, 2, "a26")
	a27 := newargs([]string{"package main", "var i int=2", "func main(){", "switch i{", "case 1:", "i=2", "case 2.0:", "}", "}"}, func(err errcode.ErrInfo) {
		if !err.IsErr("1.txt", 7, errcode.NewMsgSwitchAndCaseTypNoEqual("int", "float"), errcode.TypeIsNotEqual) {
			panic("a27\n" + err.String())
		}
	}, 1, "a27")
	a28 := newargs([]string{"package main", "struct s{", "len int", "}", "method setint(ptr &s ,l int){", "ptr.len=l", "}", "func a(a1 int){", "a1=1", "}", "func main(){", "}"}, func(err errcode.ErrInfo) {
		panic("a28\n" + err.String())
	}, 0, "a28")
	a29 := newargs([]string{"package main", "struct s{", "len int", "len int", "}", "}", "func main(){", "}"}, func(err errcode.ErrInfo) {
		if !err.IsErr("1.txt", 2, errcode.NewMsgSymbol("len"), errcode.FieldDupName) {
			panic("a29\n" + err.String())
		}
	}, 1, "a29")
	a30 := newargs([]string{"package main", "func main(){", "var a [2]int", "a[n]=9", "a[0]=a[n]", "var f int", "f[2]=1", "}"}, func(err errcode.ErrInfo) {
		if !err.IsErr("1.txt", 4, errcode.NewMsgUnknownSymbol("n"), errcode.UnknownSymbol) && !err.IsErr("1.txt", 5, errcode.NewMsgUnknownSymbol("n"), errcode.UnknownSymbol) && !err.IsErr("1.txt", 7, nil, errcode.IndexExprElemTypeErr) {
			panic("a30\n" + err.String())
		}
	}, 3, "a30")
	a31 := newargs([]string{"package main", "struct s{", "p int", "}", "var b s", "var c int", "func main(){", "var a s", "b=@a", "c=@(a.p)", "}"}, func(err errcode.ErrInfo) {
		if !err.IsErr("1.txt", 9, nil, errcode.OnlyTypePtrCanDeref) && !err.IsErr("1.txt", 10, nil, errcode.OnlyTypePtrCanDeref) {
			panic("a31\n" + err.String())
		}
	}, 2, "a31")
	a32 := newargs([]string{"package main"}, func(err errcode.ErrInfo) {
		if !err.IsErr("1.txt", -1, nil, errcode.MainPackageMustHaveMainFunc) {
			panic("a32\n" + err.String())
		}
	}, 1, "a32")
	a33 := newargs([]string{"package dep", "func main(){", "}"}, func(err errcode.ErrInfo) {
		if !err.IsErr("1.txt", 2, nil, errcode.NoMainPackageMustHaveNoMainFunc) && !err.IsErr("1.txt", 1, errcode.NewMsgPackageNameNoEqualDirName("dep", "1"), errcode.PackageNameNoEqualDirName) {
			panic("a33\n" + err.String())
		}
	}, 2, "a33")
	a34 := newargs([]string{"package main", "func main(){", "}", "func k()&int{", "return nil", "}"}, func(err errcode.ErrInfo) {
		panic("a34\n" + err.String())
	}, 0, "a34")
	tests := []struct {
		name string
		args args
	}{
		{"int <- int,int <- float 正常赋值和目的操作数与源操作数类型不相等", a1},
		{"int <- int 多运算符表达式正常赋值", a3},
		{"int <- float 多运算符表达式,左右源操作数类型不相等", a4},
		{"int <- float 赋值,目的操作数与源操作数类型不相等", a5},
		{"变量b使用(赋值)在声明之前", a6},
		{"变量b使用(参加运算)在声明之前", a7},
		{"if语句布尔表达式正确与错误", a8},
		{"else if语句布尔表达式正确与错误", a9},
		{"for语句正确与错误", a10},
		{"赋值中未知的符号", a11},
		{"对布尔类型进行非判等运算", a12},
		{"声明int返回bool", a13},
		{"不存在的标签", a14},
		{"break,continue语句不在for代码块中", a15},
		{"常量赋值及使用在声明之前", a16},
		{"类型转换错误", a17},
		{"参数不足和printf第一个参数不是字符串", a18},
		{"参数数量错误", a19},
		{"参数类型错误", a20},
		{"未知的类型", a21},
		{"选择器正确与错误", a22},
		{"调用错误", a23},
		{"自操作语句", a24},
		{"调用禁止调用的函数", a25},
		{"与内置函数有关", a26},
		{"switch语句相关", a27},
		{"函数参数视为局部变量", a28},
		{"字段重名", a29},
		{"索引表达式", a30},
		{"错误解引用", a31},
		{"main包没有main函数", a32},
		{"非main包有main函数", a33},
		{"返回nil", a34},
	}
	for _, ttf := range tests {
		tt := ttf
		t.Run(tt.name, func(t *testing.T) {
			t.Parallel()
			CheckTree(tt.args.tree, tt.args.errctx, false)
			tt.args.cancel()
		})
	}
}

func init() {
	utils.Debug = true
	errcode.Debug = true
	go errcode.Handle(context.Background(), func(err errcode.ErrInfo) { panic(err.String()) }, func() {})
}
