package check

import (
	"gitee.com/u-language/u-language/ucom/ast"
	"gitee.com/u-language/u-language/ucom/errcode"
)

// checkAllgoto 检查一个函数的所有goto语句
//   - f是被检查的函数
//   - ltable是记录被检查函数所有标签的表
func checkAllgoto(goto_range []gotoStmtInfo, ltable labelTable) []codeBlockErr {
	var ret = make([]codeBlockErr, 0)
	for _, value := range goto_range {
		ok := ltable.Have(value.Ptr.Label)
		if !ok { //如果函数没有goto跳转的便签
			ret = append(ret, codeBlockErr{Line: value.LineNum, Code: errcode.GotoErr, Msg: errcode.NewMsgLabelNoExist(value.Ptr.Label)})
		}
	}
	return ret
}

type gotoStmtInfo struct {
	Ptr     *ast.GotoStmt
	LineNum int
}
