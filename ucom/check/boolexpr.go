package check

import (
	"gitee.com/u-language/u-language/ucom/ast"
	"gitee.com/u-language/u-language/ucom/enum"
	"gitee.com/u-language/u-language/ucom/errcode"
)

// CheckBoolExprNode 一个表达式是不是表示bool
//   - ptr是被检查的表达式节点
//   - sbt是被检查的节点所属代码块的符号表，不能为nil
//   - t是被检查节点所属的抽象语法树
//   - InAutoFree指示是否在自动释放块内
func CheckBoolExprNode(ptr ast.Expr, sbt *ast.Sbt, t *ast.Tree, InAutoFree bool) (errcode.ErrCode, errcode.Msg) {
	if ptr == nil {
		return errcode.NoErr, nil
	}
	code, msg, typestr := ret_type_str(ptr, sbt, t, InAutoFree) //获取表达式类型
	if code != errcode.NoErr {                                  //有错误
		return code, msg
	}
	if typestr != enum.Bool { //不是表示bool
		return errcode.ExprNoBool, nil
	}
	return errcode.NoErr, nil
}
