package cast

type ObjKind int

const (
	NoOP ObjKind = 1 << iota
	INTOBJ
	FLOATOBJ
	BoolObj
	StringObj
	SymbolObj
	LeaObj
	DerefObj
	StructPtr
	NilObj
	TypeObj
	EnumObj
)

var (
	ObjKindSrtMap = [...]string{
		NoOP:      "noop (未定义的类型)",
		INTOBJ:    "intobj (整数)",
		FLOATOBJ:  "floatobj (浮点数)",
		BoolObj:   "boolobj (布尔值)",
		StringObj: "stringobj（字符串）",
		SymbolObj: "symbolobj (符号)",
		LeaObj:    "leaObj (取地址)",
		DerefObj:  "derefObj (解引用)",
		StructPtr: "StructPtr (结构体指针)",
		NilObj:    "NilObj (指针的零值)",
		EnumObj:   "EnumObj (枚举值)",
	}
)

func (o ObjKind) String() string {
	return ObjKindSrtMap[o]
}
