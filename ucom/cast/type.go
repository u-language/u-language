package cast

import (
	"fmt"
	"strings"

	"gitee.com/u-language/u-language/ucom/enum"
)

func typeToC(typ string) string {
	switch typ {
	case enum.String:
		return "char*"
	case enum.Int:
		return "int64_t"
	case enum.UnsafePointer:
		return "void*"
	}
	switch typ[0] {
	case '&':
		return typeToC(typ[1:]) + "*"
	}
	return typ
}

// include节点
type includeNode struct {
	file string
}

func (i *includeNode) String() string {
	var buf strings.Builder
	if i == nil {
		buf.WriteString("&cast.includeNode{")
		buf.WriteString("<nil>")
		buf.WriteString("}")
	} else {
		buf.WriteString("&cast.includeNode")
		buf.WriteString(fmt.Sprintf("%+v", *i))
	}
	return buf.String()
}

func (i *includeNode) C(buf *strings.Builder) {
	buf.WriteString("#include \"")
	buf.WriteString(i.file)
	buf.WriteString("\"")
}
