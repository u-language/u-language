package ir_test

import (
	"context"
	"reflect"
	"testing"

	"gitee.com/u-language/u-language/ucom/ast"
	"gitee.com/u-language/u-language/ucom/errcode"
	"gitee.com/u-language/u-language/ucom/internal/parser"
	. "gitee.com/u-language/u-language/ucom/ir"
)

func TestToState_ToASSIGN(t *testing.T) {
	type args struct {
		ptr *ast.ASSIGNNode
		sbt *ast.Sbt
	}
	a1tree := parser.ParserStr([]string{"var b int", "b=9+1"})
	a1 := args{a1tree.Nodes[1].(*ast.ASSIGNNode), a1tree.Sbt}
	a1want := []IrNode{newIrNode(MOVOP, "10", "", "b", ImmInt, NoArg, Var)}
	//a2
	a2tree := parser.ParserStr([]string{"var b int", "b=9+9+1"})
	a2 := args{a2tree.Nodes[1].(*ast.ASSIGNNode), a2tree.Sbt}
	a2want := []IrNode{newIrNode(MOVOP, "19", "", "b", ImmInt, NoArg, Var)}
	//a3
	a3tree := parser.ParserStr([]string{"var b int", "b=b+9-1"})
	a3 := args{a3tree.Nodes[1].(*ast.ASSIGNNode), a3tree.Sbt}
	a3want := []IrNode{newIrNode(ADDOP, "b", "8", "b", Var, ImmInt, Var)}
	//a4
	a4tree := parser.ParserStr([]string{"var b int", "b=b+b+1"})
	a4 := args{a4tree.Nodes[1].(*ast.ASSIGNNode), a4tree.Sbt}
	a4want := []IrNode{newIrNode(ADDOP, "b", "b", "tmp1", Var, Var, Tmp), newIrNode(ADDOP, "tmp1", "1", "b", Tmp, ImmInt, Var)}
	//a5
	a5tree := parser.ParserStr([]string{"var b int", "b=1+b"})
	a5 := args{a5tree.Nodes[1].(*ast.ASSIGNNode), a5tree.Sbt}
	a5want := []IrNode{newIrNode(ADDOP, "1", "b", "b", ImmInt, Var, Var)}
	//a6
	a6tree := parser.ParserStr([]string{"var b float", "b=b+9.1+1.8"})
	a6 := args{a6tree.Nodes[1].(*ast.ASSIGNNode), a6tree.Sbt}
	a6want := []IrNode{newIrNode(ADDOP, "b", "10.9", "b", Var, ImmFloat, Var)}
	//a7
	a7tree := parser.ParserStr([]string{"var b int", "b=b+9+1+b"})
	a7 := args{a7tree.Nodes[1].(*ast.ASSIGNNode), a7tree.Sbt}
	a7want := []IrNode{newIrNode(ADDOP, "b", "10", "tmp2", Var, ImmInt, Tmp), newIrNode(ADDOP, "tmp2", "b", "b", Tmp, Var, Var)}
	tests := []struct {
		name    string
		tr      *ToState
		args    args
		wantRet []IrNode
	}{
		{"b=9+1", NewToState(), a1, a1want},
		{"b=9+9+1", NewToState(), a2, a2want},
		{"b=b+9-1", NewToState(), a3, a3want},
		{"b=b+b+1", NewToState(), a4, a4want},
		{"b=1+b", NewToState(), a5, a5want},
		{"b=b+9.1+1.8", NewToState(), a6, a6want},
		{"b=b+9+1+b", NewToState(), a7, a7want},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if gotRet := tt.tr.ToASSIGN(tt.args.ptr, tt.args.sbt); !reflect.DeepEqual(gotRet, tt.wantRet) {
				t.Errorf("ToState.ToASSIGN() = %v, want %v", gotRet, tt.wantRet)
			}
		})
	}
}
func newIrNode(Op OPEnum, Arg1Obj, Arg2Obj, ResultObj string, Arg1Typ, Arg2Typ, ResultTyp ArgEnum) IrNode {
	return IrNode{
		Op:        Op,
		Arg1Obj:   Arg1Obj,
		Arg2Obj:   Arg2Obj,
		ResultObj: ResultObj,
		Arg1Typ:   Arg1Typ,
		Arg2Typ:   Arg2Typ,
		ResultTyp: ResultTyp,
	}
}

func init() {
	ast.NdSotfOn = true
	ast.FirstLineNoPackageDeclOn = true
	go errcode.Handle(context.Background(), func(err errcode.ErrInfo) { panic(err.String()) }, func() {})
}
