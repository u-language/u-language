# U语言中间代码设计文档

## 前置描述
本文档描述了U语言中间代码（以下简称ir），用VS Code + 扩展（Markdown Preview Enhanced）编写

## 目录
[定义](#1)
[操作数类型](#2)
[指令目录](#3)
- [运算类指令](#4)
  - [add](#5) 
  - [sub](#6)
- [传输类指令](#7) 
  - [mov](#8) 
- [附录](#9)

<h2 id=1> 定义 </h2>

> ir是四元式、线性的中间代码
> go语言代码表示为


```go
// OPEnum 指令枚举
type OPEnum int

// ArgEnum 操作数枚举
type ArgEnum int8

// IrNode 代表一条ir指令
type IrNode struct {
	//操作码
	Op OPEnum
	//第一运算操作数，第二运算操作数，运算结果的值及类型。
	Arg1Obj, Arg2Obj, ResultObj string
	Arg1Typ, Arg2Typ, ResultTyp ArgEnum
}
```

<h2 id=2> 操作数类型 </h2>

> ir目前定义了下列8种操作数类型

-	NoArg 未定义的操作数
-	StackVar 栈变量
-	Var 全局变量
-	Tmp 临时变量
-	TmpIntVar int型临时变量,必须编译期可求值
-	TmpFloatVar float型临时变量,必须编译期可求值
-	ImmInt int型立即数
-	ImmFloat float型立即数

<h2 id=3> 指令目录 </h2>

<h2 id=4> 运算类指令 </h2>

<h3 id=5> add </h3>

指令格式 add <src1,src2,result>
指令操作 将src1和src2相加，结果送入result

<h3 id=6> sub </h3>

指令格式 sub <src1,src2,result>
指令操作 将src1和src2相减，结果送入result

<h2 id=7> 传输类指令  </h2>

<h3 id=8> mov </h3>

指令格式 mov <src1,_,result>
指令操作 将src1的值送入result

<h2 id=9> 附录 </h2>

<h3 id=10> 官方生成ir指令的优化 </h3>

> 官方生成的ir指令有下列优化

>优化1：如果算术表达式的操作数都是立即数（也就是编译期可求值），直接计算该立即数，不等到运行时计算
>>例如
>>add <1,2,tmp1> 被优化为 add <noarg,noarg,tmp1> 同时 tmp1=3

>优化2：如果结果为立即数临时变量（也就是编译期可求值），直接使用该立即数，不等到运行时计算   
>> 例如
>> add <noarg,noarg,tmp1> mov <tmp1,noarg,b> 同时 tmp1因为优化1值为3 
>> 被优化为 mov < 3,noarg,b>

>优化3：如果赋值源操作数类型为临时变量，且这个临时变量为上一个的结果，将上一个的结果改写为赋值目的操作数
>>例如
>> add <b,2,tmp1> mov <tmp1,noarg,b>
>> 被优化为 add <b,2,b>

>优化4：如果其中任意两条中间代码形如(a op1 b) op2 c,且b和c为立即数，同时op1和op2优先级相同，改写为a op1 (b op2 c)，并编译期求值
>>例如
>> add <b,9,tmp1> add <tmp1,1,b>
>> 被优化为 add <b,10,b>