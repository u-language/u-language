package ir

// ArgEnum 操作数枚举
type ArgEnum int8

const (
	//未定义的操作数
	NoArg ArgEnum = iota
	//栈变量
	StackVar
	//全局变量
	Var
	//临时变量
	Tmp
	//int型临时变量,必须编译期可求值
	TmpIntVar
	//float型临时变量,必须编译期可求值
	TmpFloatVar
	//int型立即数
	ImmInt
	//float型立即数
	ImmFloat
)

var ArgEnumStr = [...]string{
	NoArg:       "noarg (未定义的操作数)",
	StackVar:    "stackvar （栈变量）",
	Var:         "var （全局变量）",
	Tmp:         "tmp (临时变量)",
	TmpIntVar:   "tmp_int_var (int型临时变量)",
	TmpFloatVar: "ttmp_float_var (float型临时变量)",
	ImmInt:      "imm_int (int型立即数)",
	ImmFloat:    "imm_float (float型立即数)",
}

func (a ArgEnum) String() string {
	return ArgEnumStr[a]
}

func (a ArgEnum) IsImm() bool {
	if a == ImmInt || a == TmpIntVar || a == ImmFloat {
		return true
	}
	return false
}

// OPEnum 指令枚举
type OPEnum int

const (
	NoOp OPEnum = iota //零值表示未定义的指令
	ADDOP
	SUBOP
	MOVOP
)

var OPEnumStr = [...]string{
	NoOp:  "no_op (未定义的指令)",
	ADDOP: "add （加法运算）",
	SUBOP: "sub (减法运算)",
	MOVOP: "mov （移动操作）",
}

func (o OPEnum) String() string {
	return OPEnumStr[o]
}
