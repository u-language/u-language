// Package ir实现了一种中间代码
//
// Deprecated:已弃用
//
// ir实现的中间代码，是四元式、线性的中间代码
//
// [ArgEnum] 是中间代码的操作数类型枚举
//
// [OPEnum] 是中间代码的指令枚举
//
// [IrNode] 给定了一条ir指令的结构体定义
package ir

import (
	"fmt"
	"strings"
)

// IrNode 代表一条ir指令
type IrNode struct {
	Arg1Obj   string  //第一操作数
	Arg2Obj   string  //第二操作数
	ResultObj string  //运算结果的值
	Op        OPEnum  //操作码
	Arg1Typ   ArgEnum //第一操作数类型
	Arg2Typ   ArgEnum //第二操作数类型
	ResultTyp ArgEnum //运算结果的值类型
}

func (n IrNode) String() string {
	var buf strings.Builder
	buf.WriteString("Op: ")
	buf.WriteString(n.Op.String())
	buf.WriteString(fmt.Sprintf("\nArg1Obj=%v Arg1Typ=%v\n", n.Arg1Obj, n.Arg1Typ.String()))
	buf.WriteString(fmt.Sprintf("Arg2Obj=%v Arg2Typ=%v\n", n.Arg2Obj, n.Arg2Typ.String()))
	buf.WriteString(fmt.Sprintf("ResultObj=%v ResultObj=%v\n", n.ResultObj, n.ResultTyp.String()))
	return buf.String()
}
