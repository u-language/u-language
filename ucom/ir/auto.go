package ir

import (
	"strconv"
	"strings"
	"sync/atomic"
)

// autoVar 临时变量分配器
type autoVar struct {
	i int32
}

func newautoVar() *autoVar {
	return &autoVar{}
}

// Get 获取一个临时变量
// 不同的调用返回不同的临时变量
func (a *autoVar) Get() string {
	n := atomic.AddInt32(&a.i, 1)
	var buf strings.Builder
	buf.WriteString("tmp")
	buf.WriteString(strconv.Itoa(int(n)))
	return buf.String()
}
