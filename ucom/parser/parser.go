// Package parser提供创建ast树的简便方法
//
// parser不对代码编译的报误进行处理
//
// 如果要进行错误处理请自行使用 [pkg/gitee.com/u-language/u-language/ucom/errcode.Handle]
package parser

import (
	"errors"
	"fmt"
	"os"
	"path/filepath"

	"gitee.com/u-language/u-language/ucom/ast"
	"gitee.com/u-language/u-language/ucom/ast2"
	"gitee.com/u-language/u-language/ucom/errcode"
)

var Debug = false

var test = false //判断是否在测试

// 缩写 File does not have an extension 缩写成 FileNoExt
var FileNoExt = errors.New("文件没有扩展名")

// ParserAuto 根据path是目录还是文件决定调用 [ParserPackage] 或 [ParserFile]
func ParserAuto(path string, thread bool, errctx *errcode.ErrCtx) (ast.GetImport, error) {
	fileinfo, err := os.Stat(path)
	if err != nil {
		return nil, err
	}
	if fileinfo.IsDir() { //如果是目录
		return ParserPackage(path, thread, errctx, ast.NewSbt(thread), new(map[string]*ast.Package))
	}
	if filepath.Ext(path) == "" { //如果文件没有扩展名
		return nil, FileNoExt
	}
	return ParserFile(path, errctx, true, new(map[string]*ast.Package), thread)
}

// ParserAutoBuildMode2 根据path是目录还是文件决定调用 [ParserPackageBuildMode2] 或 [ParserFileBuildMode2]
// 如果path带有后缀名，视为文件
func ParserAutoBuildMode2(path string, thread bool, errctx *errcode.ErrCtx) (fmt.Stringer, error) {
	if ext := filepath.Ext(path); ext != "" {
	} else {
		fileinfo, err := os.Stat(path)
		if err != nil {
			return nil, err
		}
		if fileinfo.IsDir() { //如果是目录
			return ParserPackageBuildMode2(path, thread, errctx, ast2.NewSbt(true), make(map[string]*ast2.Package))
		}
	}
	return ParserFileBuildMode2(path, errctx, true, false)
}
