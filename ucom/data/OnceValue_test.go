package data_test

import (
	"testing"

	. "gitee.com/u-language/u-language/ucom/data"
)

func TestOnceValue(t *testing.T) {
	v := OnceValue[int]{}
	for i := 0; i < 100; i++ {
		got, _ := v.Get()
		v.Set(got + 1)
	}
	if i, _ := v.Get(); i != 1 {
		t.Fatalf("!=1")
	}
}
