package data

import (
	"sync"
)

// Slice 是可以并发安全的切片（Thread=true）
type Slice[T any] struct {
	Data   []T
	lock   sync.Mutex
	Thread bool
}

func NewSilce[T any](Thread bool, capnum int) *Slice[T] {
	return &Slice[T]{Thread: Thread, Data: make([]T, 0, capnum)}
}

// Add 添加一个值到末尾，可以从多个goroutine同时调用（Thread=true）
func (s *Slice[T]) Add(value T) {
	if s.Thread { //并发加锁
		s.lock.Lock()
	}
	s.Data = append(s.Data, value)
	if s.Thread { //并发解锁
		s.lock.Unlock()
	}
}

// AddSlice 添加一个切片到末尾，可以从多个goroutine同时调用（Thread=true）
func (s *Slice[T]) AddSlice(value []T) {
	if s.Thread { //并发加锁
		s.lock.Lock()
	}
	s.Data = append(s.Data, value...)
	if s.Thread { //并发解锁
		s.lock.Unlock()
	}
}
