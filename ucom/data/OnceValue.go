package data

import (
	"sync"
)

// 缩写：Set value only once缩写成 OnceValue
//
//	只设置一次的值
//
// TODO:当最低需要go1.21时，改为标准库实现
type OnceValue[T any] struct {
	value T
	once  sync.Once
	lock  sync.Mutex
	IsSet bool
}

// Set 设置值，如果已经设置过，不会重复设置
func (o *OnceValue[T]) Set(value T) {
	o.once.Do(func() {
		o.lock.Lock()
		o.value = value
		o.IsSet = true
		o.lock.Unlock()
	})
}

// Get 获取值，已经是否设置过
func (o *OnceValue[T]) Get() (T, bool) {
	o.lock.Lock()
	defer o.lock.Unlock()
	return o.value, o.IsSet
}
