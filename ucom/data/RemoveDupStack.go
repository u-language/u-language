package data

// RemoveDupStrck 能去除重复元素的栈
type RemoveDupStrck[T comparable] struct {
	set map[T]struct{}
	Slice[T]
}

func NewRemoveDupStrck[T comparable](thread bool) RemoveDupStrck[T] {
	ret := RemoveDupStrck[T]{set: make(map[T]struct{})}
	ret.Thread = thread
	return ret
}

// Push 如果栈中没有值，则入栈
func (s *RemoveDupStrck[T]) Push(value T) {
	_, ok := s.set[value]
	if ok {
		return
	}
	s.set[value] = struct{}{}
	s.Add(value)
}

// Range 从栈底开始遍历到栈顶
func (s *RemoveDupStrck[T]) Range(f func(T)) {
	for i := 0; i < len(s.Data); i++ {
		f(s.Data[i])
	}
}

// Len 返回栈中有多少数据
func (s *RemoveDupStrck[T]) Len() int {
	return len(s.Data)
}
