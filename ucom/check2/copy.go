package check2

import (
	"strconv"

	"gitee.com/u-language/u-language/ucom/enum"
	"gitee.com/u-language/u-language/ucom/errcode"
	"gitee.com/u-language/u-language/ucom/internal/errutil"
	"gitee.com/u-language/u-language/ucom/internal/utils"
	"gitee.com/u-language/u-language/ucom/ir2"
)

// predefined_func 检查预定义的函数或视为函数调用的类型转换
func predefined_func(f *ir2.File, v *ir2.IrNode, i *int, ir []ir2.IrNode) (ok bool) {
	pn, err := strconv.Atoi(v.ResultObj)
	if err != nil {
		panic(errutil.NewErr2(errutil.CompileErr, err.Error()))
	}
	switch v.Arg1Obj {
	case enum.Printf:
		if pn < 1 {
			f.Errctx.Panic(f.FileName, v.LineNum, nil, errcode.InsuParame)
			return true
		}
	f:
		for *i++; *i < len(ir); *i++ {
			switch ir[*i].Op {
			case ir2.EndCallOp:
				break f
			case ir2.PaPaOp:
				p := ir[*i]
				tmp := findTmpTyp(p.Arg2Obj, i, ir)
				pn, err = strconv.Atoi(p.Arg1Obj)
				utils.MustErr(err)
				if pn == 0 && tmp != enum.String { //如果第一个参数不是字符串
					f.Errctx.Panic(f.FileName, v.LineNum, nil, errcode.FirstParameOfPrinfASting)
					return true
				}
			default:
				checkIr(f, &ir[*i], ir, i)
			}
		}
		return true
	}
	return false
}
