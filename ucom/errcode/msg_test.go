package errcode

import "testing"

func Test_remove_package_name(t *testing.T) {
	type args struct {
		s string
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		{"内置函数", args{"malloc"}, "malloc"},
		{"包函数", args{"main__str"}, "str"},
		{"指针类型", args{"&main__str"}, "&str"},
		{"二级指针", args{"&&main__str"}, "&&str"},
		{"不安全函数", args{"unsafe__Sizeof"}, "unsafe__Sizeof"},
		{"包名为空函数", args{"__Sizeof"}, "Sizeof"},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := remove_package_name(tt.args.s); got != tt.want {
				t.Errorf("remove_package_name() = %v, want %v", got, tt.want)
			}
		})
	}
}
