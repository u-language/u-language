package errcode

import (
	"context"
)

var (
	DefaultErrCtx = NewErrCtx()
)

// Panic 调用 DefaultErrCtx.Panic
func Panic(file string, line int, addInfo Msg, err ...ErrCode) {
	DefaultErrCtx.Panic(file, line, addInfo, err...)
}

// Errbol 调用 DefaultErrCtx.Errbol
func Errbol() bool {
	return DefaultErrCtx.Errbol()
}

// Handle 调用 DefaultErrCtx.Handle
func Handle(ctx context.Context, errfn func(ErrInfo), retfn func()) { //错误处理
	DefaultErrCtx.Handle(ctx, errfn, retfn)
}
