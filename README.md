# U语言

### 介绍
U语言是开发者对编译技术的实践，更多的是开发者对编译技术的实验,由于个人时间有限，**目前只开发和机器无关的（词法分析、构建抽象语法树、语义检查）等部分**。
一些主要的设计目标是

- 支持面向过程
- 静态类型
- 无STW

- - -

### 文档
[词法分析器设计文档](./pkg/lex/README.md)

[语言规范](./语言规范.md)

[U语言包管理器文档](./cmd/upam/README.md)

[其他重要文档目录](./doc/docfile/)

- - -

### 重大事件记录
2023/1/23 无限期暂停开发mode=fasm模式：自2022年8月实现一个实验性的将ast转换为fasm汇编代码功能以来，fasm/amd64及其配套的包，消耗了最多的时间，写出的代码可维护性最差，可读性最差，实现的功能最差，严重拖慢了开发进度，现在决定无限期暂停开发mode=fasm模式。

- - -

### 安装教程

#### 推荐安装方式：包管理器安装

参见 [U语言包管理器文档](./cmd/upam/README.md)

##### 不推荐安装方式：源码安装

1. 源码编译依赖
 go 版本 >=1.20

- - -

2. 下载源代码
3. cd pkg
4. go build 

可以使用下列命令安装
git clone https://gitee.com/u-language/u-language

cd u-language/pkg

go build

- - -

### 编译器工作流程

```mermaid
graph TD
err(报错退出 errcode)
lex(词法分析器 lex)
ast(抽象语法树 ast)
check(语义检查器 check)
cast(C代码转换器 cast)
amd64(汇编转换器 fasm/amd64 ,已暂停开发)
ir(中间表示 ir,只被fasm/amd64依赖, 已暂停开发)
fd(结果输出)
cToolChain(调用C工具链)
main(入口) -->parser(解析器 parser)
parser --调用--> lex --发现代码错误-->err
lex --结果转换ast--> ast --发现代码错误-->err
ast --类型检查--> check --发现代码错误-->err
check -.mode==c.-> cast
check -.mode==fasm.-> amd64 
cast --> cToolChain --> fd
amd64 --调用了--> ir
amd64 --> fd
```

- - -

### 参与贡献

1. 新建issue，并在issue的文本中加上@qiulaidongfeng
2. Fork 本仓库
3. 新建 Fork_xxx 分支
4. 提交代码
5. 新建 Pull Request
