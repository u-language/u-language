package cmd

import (
	"fmt"
	"os"
	"path/filepath"

	"github.com/spf13/cobra"
)

// listCmd represents the list command
var listCmd = &cobra.Command{
	Use:   "list",
	Short: "输出所有安装的编译器版本",
	Long:  `输出所有安装的编译器版本`,
	Run: func(cmd *cobra.Command, args []string) {
		info := GetComplierDirInfo(u_env_var.U_Complier_Sdk)
		file_name, err := os.Readlink(u_env_var.U_Complier_Link)
		if err != nil {
			cmd.PrintErr(fmt.Errorf("读取符号链接 %s 失败： 错误信息： %w", u_env_var.U_Complier_Link, err))
			return
		}
		file_name = filepath.Base(file_name)
		//打印所有安装了的版本，如果正在使用加后缀 in use
		for _, v := range info {
			if file_name == filepath.Base(v.Path) {
				cmd.Printf("%s in use\n", v.Version[2:])
				continue
			}
			cmd.Printf("%s\n", v.Version[2:])
		}
	},
}

func init() {
	rootCmd.AddCommand(listCmd)
}
