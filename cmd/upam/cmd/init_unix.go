//go:build unix

package cmd

import (
	"fmt"
	"os/exec"
)

var Ext = ""

// InitLink 初始化链接目录
func InitLink() error {
	cmd := exec.Command("ln", "-s", "-n", u_env_var.U_Complier_Link, u_env_var.U_Complier_Sdk)
	buf, err := cmd.CombinedOutput()
	fmt.Println(string(buf))
	if err != nil {
		return fmt.Errorf("创建目录符号链接失败 错误信息：%w", err)
	}
	return nil
}
