package cmd

import (
	"fmt"
	"os"
	"path/filepath"

	"github.com/spf13/cobra"
)

// rmCmd represents the rm command
var rmCmd = &cobra.Command{
	Use: "rm",
	Short: `rm 删除指定版本编译器，默认选择开发分支，后可有一个可选的版本号
	示例：
	upam rm v0.4.3`,
	Long: `rm 删除指定版本编译器，默认选择开发分支，后可有一个可选的版本号
	示例：
	upam rm v0.4.3`,
	Run: func(cmd *cobra.Command, args []string) {
		typ := istip(args)
		err := RmComplier(typ)
		if err != nil {
			cmd.PrintErrf("%s\n", err.Error())
		}
	},
}

// RmComplier 删除指定版本的编译器
func RmComplier(typ string) error {
	err := checkVersion(typ) //检查版本号是否正确
	if err != nil {
		return err
	}
	if !IsVersion(typ) { //检查指定版本是否安装
		return fmt.Errorf("版本 %s 没有下载并安装了", typ)
	}
	dir := filepath.Join(u_env_var.U_Complier_Sdk, `u@`+typ)
	fmt.Printf("删除 %s", dir)
	err = os.RemoveAll(dir) //删除编译器所在目录
	if err != nil {
		return fmt.Errorf("删除目录 %s 失败 错误信息： %w", dir, err)
	}
	return nil
}

func init() {
	rootCmd.AddCommand(rmCmd)
}
