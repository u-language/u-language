package cmd

import (
	"os"

	"github.com/spf13/cobra"
)

// upam == U Language Pack Manager
var rootCmd = &cobra.Command{
	Use:   "upam",
	Short: "upam U语言包管理器，附带多版本编译器管理",
	Long:  "upam U语言包管理器，附带多版本编译器管理",
}

// Execute adds all child commands to the root command and sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func Execute() {
	err := rootCmd.Execute()
	if err != nil {
		os.Exit(1)
	}
}
