package cmd

import (
	"fmt"
	"syscall"
)

var Ext = ".exe"

// InitLink 初始化链接目录
func InitLink() error {
	linkptr, err := syscall.UTF16PtrFromString(u_env_var.U_Complier_Link)
	if err != nil {
		return fmt.Errorf("创建目录符号链接失败 错误信息：%w", err)
	}
	targetptr, err := syscall.UTF16PtrFromString(`C:\`)
	if err != nil {
		return fmt.Errorf("创建目录符号链接失败 错误信息：%w", err)
	}
	// 创建目录符号链接
	err = syscall.CreateSymbolicLink(linkptr, targetptr, syscall.SYMBOLIC_LINK_FLAG_DIRECTORY)
	if err != nil {
		return fmt.Errorf("创建目录符号链接失败 错误信息：%w", err)
	}
	return nil
}
