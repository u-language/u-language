package cmd

import (
	"fmt"

	"github.com/spf13/cobra"
)

// setCmd represents the set command
var setCmd = &cobra.Command{
	Use: "set",
	Short: `set设置使用的编译器版本
	示例：
	upam set v0.4.3`,
	Long: `set设置使用的编译器版本
	示例：
	upam set v0.4.3`,
	Run: func(cmd *cobra.Command, args []string) {
		typ := istip(args)
		err := SetComplier(typ)
		if err != nil {
			cmd.PrintErrf("%s\n", err.Error())
		}
	},
}

func init() {
	rootCmd.AddCommand(setCmd)
}

func SetComplier(typ string) error {
	err := checkVersion(typ) //检查版本号是否正确
	if err != nil {
		return err
	}
	if !IsVersion(typ) { //检查指定版本是否安装
		return fmt.Errorf("版本 %s 没有下载并安装", typ)
	}
	err = Link(typ) //将 $U_HOME\sdk\link 设置为指定编译器版本的目录符号链接
	return err
}
