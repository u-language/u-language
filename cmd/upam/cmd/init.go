package cmd

import (
	"fmt"
	"os"
	"path/filepath"
	"runtime"

	"github.com/spf13/cobra"
)

// initCmd represents the init command
var initCmd = &cobra.Command{
	Use: "init",
	Short: `init 初始化
	如果没有 $U_HOME+sdk 的文件夹则创建 
	如果没有 $U_HOME+sdk+link 的目录符号链接则创建 
	`,
	Long: `init 初始化
	如果没有 $U_HOME+sdk 的文件夹则创建 
	如果没有 $U_HOME+sdk+link 的目录符号链接则创建 
	`,
	Run: func(cmd *cobra.Command, args []string) {
		err := os.MkdirAll(u_env_var.U_Complier_Sdk, 0777)
		if err != nil {
			cmd.PrintErrln(fmt.Errorf("初始化创建 %s 失败 错误信息: %w", u_env_var.U_Complier_Link, err))
			return
		}
		err = InitLink()
		if err != nil {
			cmd.PrintErrln(err.Error())
			return
		}
		if runtime.GOOS == "windows" { //windows自动设置环境变量
			err = Exec(cmd, "SETX", "U_HOME", u_env_var.U_HOME)
			if err != nil {
				cmd.PrintErrln(fmt.Errorf("设置环境变量U_HOME的值为 %s 失败 错误信息: %w", u_env_var.U_HOME, err))
				return
			}
			path := filepath.Join(u_env_var.U_Complier_Link, "pkg")
			err = Exec(cmd, "SETX", "PATH", "%PATH%;"+path)
			if err != nil {
				cmd.PrintErrln(fmt.Errorf("设置环境变量PATH的值为 %s 失败 错误信息: %w", "%PATH%;"+path, err))
				return
			}
		}
	},
}

func init() {
	rootCmd.AddCommand(initCmd)
}
