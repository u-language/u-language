# U语言包管理器文档

## 前置描述
本文档描述了U语言包管理器（英文简称upam），用VS Code + 扩展（Markdown Preview Enhanced）编写

#### 最后更新时间 ： 2023/8/15

## 功能
U语言包管理器有两个功能

- 包管理（还没开发）
- 多版本编译器管理

## 多版本编译器管理 
### 1. 安装U语言包管理器文档
#### 1.1 前提条件
- 安装有go编译器（go版本大于等于1.20）
- 安装有git

#### 1.2 执行命令
go install gitee.com/u-language/u-language/cmd/upam@master

### 2. 初始化
#### 2.1 U_HOME 环境变量
**U_HOME环境变量的值是U语言包管理器存放下载的数据的根目录，所有下载的数据都在U_HOME环境变量的值指定的目录**

U_HOME的默认值如下

- windows系统默认值是C:\\Users\\Administrator\\u
- 其他系统的默认值是$HOME/u

每次U_HOME的值被更改都需要重新初始化，并且只需要也只能初始化一次

#### 2.2 执行命令
upam init

如果命令行窗口报找不到文件请运行go env获取GOPATH环境变量的值，将GOPATH环境变量的值和bin,windows用\\ 其他系统用/连接起来，添加到PATH环境变量里

windows系统会自动设置环境变量U_HOME（默认为默认值）,如果已经设置过，值不变；会自动将U_HOME的值+\\sdk\\link\\pkg追加到环境变量PATH的末尾，请重启命令行窗口（比如使用cmd就重新打开cmd）确保生效

其他系统请自行设置环境变量U_HOME（可以不设置，那样会使用默认值）；请自行设置将U_HOME的值+/sdk/link/pkg追加到环境变量PATH

### 3. 安装指定版本
#### 3.1 安装最新开发版本
执行命令 upam download 

#### 3.2 安装指定版本
执行命令 upam download 版本号

举例 安装v0.4.2版本 执行命令 upam download v0.4.2

### 4. 删除指定版本
删除的版本必须已经下载并安装

#### 4.1 删除最新开发版本
执行命令 upam rm 

#### 4.2 删除指定版本
执行命令 upam rm 版本号

举例 删除v0.4.2版本 执行命令 upam rm v0.4.2

### 5. 设置使用指定版本
设置使用的版本必须已经下载并安装

#### 5.1 设置使用最新开发版本
执行命令 upam set 

#### 5.2 设置使用指定版本
执行命令 upam set 版本号

举例 设置使用v0.4.2版本 执行命令 upam set v0.4.2

### 6. 列出已安装版本

执行命令 upam list

将输出已安装版本号

正在使用的版本号后面将加 in use
