package main

import (
	"os"
	"os/exec"
	"path/filepath"
	"runtime"
	"testing"
)

var Ext string

var Sep string

func init() {
	if runtime.GOOS == "windows" {
		Ext = ".exe"
		Sep = ";"
	} else {
		Sep = ":"
	}
}

func TestAll(t *testing.T) {
	tmp, err := os.MkdirTemp("", "upam")
	if err != nil {
		t.Fatal(err)
	}
	t.Cleanup(func() {
		if !t.Failed() {
			if err := os.RemoveAll(tmp); err != nil {
				t.Fatal(err)
			}
		}
	})
	t.Setenv("U_HOME", tmp)
	path := filepath.Join(tmp, "upam"+Ext)
	Exec(t, "go", "build", "-o", path, "./")
	t.Setenv("PATH", filepath.Dir(path)+Sep+os.Getenv("PATH"))
	Exec(t, path, "init")
	Exec(t, path, "download")
	t.Setenv("PATH", filepath.Join(tmp, "sdk", "link", "ucom")+Sep+os.Getenv("PATH"))
	Exec(t, "ucom"+Ext, "version")
}

func Exec(t *testing.T, path string, args ...string) {
	t.Logf("path=%s args=%v", path, args)
	c := exec.Command(path, args...)
	b, err := c.CombinedOutput()
	t.Logf("%s\n", string(b))
	if err != nil {
		t.Logf("%s\n", os.Getenv("PATH"))
		t.Fatalf("err=%+v", err)
	}
}
